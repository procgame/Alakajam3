#include <stdlib.h>
#include <limits.h>
#include "procgl/procgl.h"

#include "../core.h"

static int hello_click(struct pg_ui_context* ctx, pg_ui_t elem,
                       struct pg_ui_event* event)
{
    struct game_core* core = pg_ui_variable(ctx, ctx->root, "game_core")->ptr[0];
    if(!core) return 1;
    struct pg_type* elem_var = pg_ui_variable(ctx, elem, "foo");
    if(elem_var->i[0]) {
        pg_ui_set_text(ctx, elem, L"%C(0.5,1,0.5,1)Hello, %C(0.5,0.5,1,1)World!", 64);
        pg_ui_simple_anim(ctx, elem, PG_UI_TEXT_POS, &PG_SIMPLE_ANIM(
            .start = vec4(0,-1), .duration = 0.5, .ease = pg_ease_out_bounce ));
    } else {
        pg_ui_set_text(ctx, elem, L"%C(0.5,0.5,1,1)Hello, %C(0.5,1,0.5,1)World!", 64);
        pg_ui_simple_anim(ctx, elem, PG_UI_TEXT_POS, &PG_SIMPLE_ANIM(
            .end = vec4(0,-1), .duration = 0.5, .ease = pg_ease_out_bounce ));
    }
    pg_audio_play_ch(&core->snd0, 0.5, 0);
    elem_var->i[0] = !elem_var->i[0];
    return 1;
}

void game_core_build_ui(struct game_core* core)
{
    struct pg_ui_context* ctx = &core->ui;
    pg_ui_variable(ctx, ctx->root, "game_core")->ptr[0] = core;
    /*  Build UI stuff  */
    pg_ui_t hello = pg_ui_add_elem(ctx, ctx->root, "hello_world",
        PG_UI_PROPERTIES( .draw = PG_UI_IMG_THEN_TEXT,
            .text = L"%C(0.5,1,0.5,1)Hello, %C(0.5,0.5,1,1)World!",
            .text_scale = vec2(0.2,0.2), .text_pos = vec2(0,0.4),
            .text_formatter = &PG_TEXT_FORMATTER_MOD(ctx->default_text_formatter,
                .align = PG_TEXT_CENTER),
            .img_scale = vec2(1.5,1.5),
            .img_color_mod = vec4(0), .img_color_add = vec4(1,1,1,0.25),
            .action_item = PG_UI_ACTION_TEXT,
            .cb_click = hello_click ));
}

void game_core_update_ui(struct game_core* core)
{
    struct pg_ui_context* ctx = &core->ui;
    /*  Special UI update logic */
}
