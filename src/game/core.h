struct game_core {
    /*  Assets  */
    struct pg_texture sgtex;
    struct pg_font sgfont[3];
    struct pg_audio_chunk snd0;
    /*  RENDERING   */
    struct pg_renderer rend;
    vec2 halfres;
    vec2 res;
    float ar;
    /*  Rendertargets   */
    struct pg_texture pptex[2];
    struct pg_texture ppdepth;
    struct pg_renderbuffer ppbuf[2];
    struct pg_rendertarget target;
    /*  Renderpasses    */
    struct pg_quadbatch quadbatch;
    struct pg_boxbatch boxbatch;
    struct pg_renderpass quads_pass;
    struct pg_renderpass box_pass;
    struct pg_renderpass overlay_pass;
    struct pg_renderpass copy_pass;
    /*  Viewers */
    struct pg_viewer worldview;
    struct pg_viewer screenview;
    /*  UI  */
    struct pg_ui_context ui;
    pg_ui_t overworld_ui;
    /*  Input   */
    vec2 mouse_motion;
    vec2 mouse_pos;
};

void game_core_start(struct pg_game_state* state);

void game_core_build_ui(struct game_core* core);
void game_core_update_ui(struct game_core* core);
