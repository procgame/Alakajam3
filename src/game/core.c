#include <stdlib.h>
#include <limits.h>
#include "procgl/procgl.h"

#include "core.h"

static void game_core_update(struct pg_game_state*);
static void game_core_tick(struct pg_game_state*);
static void game_core_draw(struct pg_game_state*);
static void game_core_deinit(void*);

/*  Create a game state for procgame    */
void game_core_start(struct pg_game_state* state)
{
    pg_game_state_init(state, pg_time(), 30, 3);
    struct game_core* d = malloc(sizeof(*d));
    *d = (struct game_core){};

    /****************************/
    /*  Setup a render target   */
    int w, h;
    pg_screen_size(&w, &h);
    d->res = vec2(w, h);
    d->halfres = vec2_scale(d->res, 0.5);
    d->ar = (float)w / h;
    /*  Make the render textures    */
    pg_texture_init(&d->pptex[0], PG_UBVEC4, w, h, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_init(&d->pptex[1], PG_UBVEC4, w, h, 1, PG_TEXTURE_OPTS(
        .filter_mag = GL_NEAREST,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    pg_texture_init_depth_stencil(&d->ppdepth, w, h, 1, PG_TEXTURE_OPTS());
    pg_renderbuffer_init(&d->ppbuf[0]);
    pg_renderbuffer_init(&d->ppbuf[1]);
    pg_renderbuffer_attach(&d->ppbuf[0], &d->pptex[0], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_attach(&d->ppbuf[1], &d->pptex[1], 0, 0, GL_COLOR_ATTACHMENT0);
    pg_renderbuffer_attach(&d->ppbuf[0], &d->ppdepth, 0, 1, GL_DEPTH_STENCIL_ATTACHMENT);
    pg_renderbuffer_attach(&d->ppbuf[1], &d->ppdepth, 0, 1, GL_DEPTH_STENCIL_ATTACHMENT);
    pg_renderbuffer_build(&d->ppbuf[0]);
    pg_renderbuffer_build(&d->ppbuf[1]);
    pg_rendertarget_init(&d->target, &d->ppbuf[0], &d->ppbuf[1]);
    
    /****************************/
    /*  Allocate texture space  */
    char font_tex_size[16];
    snprintf(font_tex_size, 16, ":%d,%d", h, h);
    const char* tex_files[4] = { ":64,64",
        font_tex_size, font_tex_size, font_tex_size };
    pg_texture_from_files(&d->sgtex, 4, tex_files,
        PG_TEXTURE_OPTS(.filter_min = GL_NEAREST, .filter_mag = GL_NEAREST,
                        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE));
    /*  Load fonts at (screensize * 0.1) pixel height   */
    int font_pixels = floor((float)h * 0.1);
    pg_font_init(&d->sgfont[0], "res/Orbitron-Bold.ttf", &d->sgtex, 1, font_pixels);
    pg_font_init(&d->sgfont[1], "res/NASDAQER.ttf", &d->sgtex, 2, font_pixels*1.25);
    pg_font_init(&d->sgfont[2], "res/Caveat-Bold.ttf", &d->sgtex, 3, font_pixels*1.25);
    d->sgfont[1].line_move *= 0.75;
    /*  Buffer the texture  */
    pg_texture_buffer(&d->sgtex);

    /************************/
    /*  Load audio assets   */
    pg_audio_load_wav(&d->snd0, "res/audio/snd0.wav");

    /************************/
    /*  Make a UI context   */
    pg_ui_init(&d->ui, &d->target, &d->sgtex, &PG_TEXT_FORMATTER(
            .fonts = PG_FONTS(3, &d->sgfont[0], &d->sgfont[1], &d->sgfont[2]),
            .scale = vec2(5,5), .htab = 4 ));
    *pg_ui_variable(&d->ui, d->ui.root, "akj") = PG_TYPE_POINTER(d);
    pg_ui_enabled(&d->ui, d->ui.root, 1);
    game_core_build_ui(d);

    /************************/
    /*  Set up the renderer */
    pg_renderer_init(&d->rend);
    /*  Allocate space for boxes and quads  */
    pg_boxbatch_init(&d->boxbatch, 16384, &d->sgtex);
    pg_quadbatch_init(&d->quadbatch, 16384, &d->sgtex);
    /*  Box-batch renderpass    */
    pg_boxbatch_init_pass(&d->boxbatch, &d->box_pass, &d->target, vec2(d->sgtex.w, d->sgtex.h));
    pg_renderpass_viewer(&d->box_pass, &d->worldview);
    /*  3D quads renderpass    */
    pg_quadbatch_init_pass(&d->quadbatch, &d->quads_pass, &d->target);
    pg_renderpass_viewer(&d->quads_pass, &d->worldview);
    pg_renderpass_depth_test(&d->quads_pass, 1, GL_LEQUAL);
    pg_renderpass_uniform(&d->quads_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(0));
    /*  2D quads renderpass (overlay)   */
    pg_quadbatch_init_pass(&d->quadbatch, &d->overlay_pass, &d->target);
    pg_renderpass_viewer(&d->overlay_pass, &d->screenview);
    pg_renderpass_texture(&d->overlay_pass, 1, &d->sgtex, PG_TEXTURE_OPTS(
        .filter_min = GL_LINEAR, .filter_mag = GL_LINEAR,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE ));
    pg_renderpass_uniform(&d->overlay_pass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(-1));
    /*  Final copy-to-screen render stage   */
    pg_ezpass_screen(&d->copy_pass, &d->target);
    /*  Add the renderpasses to the renderer    */
    ARR_PUSH(d->rend.passes, &d->quads_pass);
    ARR_PUSH(d->rend.passes, &d->box_pass);
    ARR_PUSH(d->rend.passes, &d->overlay_pass);
    ARR_PUSH(d->rend.passes, &d->ui.rendpass);
    ARR_PUSH(d->rend.passes, &d->copy_pass);

    /****************************/
    /*  Fill in pg_game_state   */
    state->data = d;
    state->update = game_core_update;
    state->tick = game_core_tick;
    state->draw = game_core_draw;
    state->deinit = game_core_deinit;
}

/*  Fixed timestep  */
static void game_core_tick(struct pg_game_state* state)
{
    struct game_core* d = state->data;
    if(pg_user_exit()) state->running = 0;
    /*  Tick the UI   */
    pg_ui_update(&d->ui, (float)state->ticks / 30.0f);
    game_core_update_ui(d);
    /*  Tick game stuff */

    /*  Flush all the input */
    pg_flush_input();
}

/*  Per-frame update (variable timing)  */
static void game_core_update(struct pg_game_state* state)
{
    struct game_core* d = state->data;
    pg_poll_input();
}

/*  Draw frame  */
static void game_core_draw(struct pg_game_state* state)
{
    struct game_core* d = state->data;
    float dt = state->tick_over;
    /*  Clear everything    */
    pg_rendertarget_clear(&d->target);
    pg_renderpass_clear(&d->box_pass);
    pg_renderpass_clear(&d->quads_pass);
    pg_renderpass_clear(&d->overlay_pass);
    /*  UI  */
    pg_ui_draw(&d->ui, dt / 30.0f);
    /*  Draw game stuff */

    /*  Actually render everything  */
    pg_renderer_draw_frame(&d->rend);
}

static void game_core_deinit(void* data)
{
    struct game_core* d = data;

    pg_texture_deinit(&d->sgtex);
    pg_texture_deinit(&d->pptex[0]);
    pg_texture_deinit(&d->pptex[1]);
    pg_texture_deinit(&d->ppdepth);

    pg_font_deinit(&d->sgfont[0]);
    pg_font_deinit(&d->sgfont[1]);
    pg_font_deinit(&d->sgfont[2]);

    pg_quadbatch_deinit(&d->quadbatch);
    pg_boxbatch_deinit(&d->boxbatch);

    pg_renderbuffer_deinit(&d->ppbuf[0]);
    pg_renderbuffer_deinit(&d->ppbuf[1]);
    pg_renderpass_deinit(&d->quads_pass);
    pg_renderpass_deinit(&d->overlay_pass);
    pg_renderpass_deinit(&d->copy_pass);
    pg_renderpass_deinit(&d->box_pass);
    pg_renderer_deinit(&d->rend);

    pg_ui_deinit(&d->ui);

    free(d);
}
