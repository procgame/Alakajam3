
struct pg_model;
struct pg_viewer;

/*
enum pg_shader_attribute {
    PG_ATTRIB_POSITION,
    PG_ATTRIB_NORMAL,
    PG_ATTRIB_TANGENT,
    PG_ATTRIB_BITANGENT,
    PG_ATTRIB_COLOR,
    PG_ATTRIB_TEXCOORD,
    PG_ATTRIB_HEIGHT,
    PG_NUM_ATTRIBS
};

static const struct pg_shader_attribute_info {
    char name[32];
    enum pg_data_type type;
    GLenum gltype;
    GLboolean normalized;
    GLint elements;
    int as_integer;
    int size;
} PG_SHADER_ATTRIBUTE_INFO[PG_NUM_ATTRIBS] = {
    [PG_ATTRIB_POSITION] = {
        .name = "v_position", .type = PG_VEC3,
        .gltype = GL_FLOAT, .elements = 3,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(vec3), },
    [PG_ATTRIB_NORMAL] = {
        .name = "v_normal", .type = PG_VEC3,
        .gltype = GL_FLOAT, .elements = 3,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(vec3), },
    [PG_ATTRIB_TANGENT] = {
        .name = "v_tangent", .type = PG_VEC3,
        .gltype = GL_FLOAT, .elements = 3,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(vec3), },
    [PG_ATTRIB_BITANGENT] = {
        .name = "v_bitangent", .type = PG_VEC3,
        .gltype = GL_FLOAT, .elements = 3,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(vec3), },
    [PG_ATTRIB_COLOR] = {
        .name = "v_color", .type = PG_UBVEC4,
        .gltype = GL_UNSIGNED_BYTE, .elements = 4,
        .normalized = 1, .as_integer = 0,
        .size = sizeof(ubvec4), },
    [PG_ATTRIB_TEXCOORD] = {
        .name = "v_tex_coord", .type = PG_VEC2,
        .gltype = GL_FLOAT, .elements = 2,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(vec2), },
    [PG_ATTRIB_HEIGHT] = {
        .name = "v_height", .type = PG_FLOAT,
        .gltype = GL_FLOAT, .elements = 1,
        .normalized = 0, .as_integer = 0,
        .size = sizeof(float), },
};
*/

struct pg_shader_attrib {
    char name[32];
    GLenum type;
    int elements;
    int index;
};

struct pg_shader_uniform {
    enum pg_data_type type;
    struct pg_type data;
    int array_len;
    GLint idx;
};

typedef HTABLE_T(struct pg_shader_uniform) pg_shader_uniform_table_t;
typedef HTABLE_ENTRY pg_shader_uniform_t;

enum pg_matrix {
    PG_MODEL_MATRIX,
    PG_NORMAL_MATRIX,
    PG_VIEW_MATRIX,
    PG_PROJECTION_MATRIX,
    PG_MODELVIEW_MATRIX,
    PG_PROJECTIONVIEW_MATRIX,
    PG_MVP_MATRIX,
    PG_COMMON_MATRICES,
};

struct pg_shader {
    char name[32];
    /*  Handles for the shader program  */
    GLuint vert, frag, prog;
    /*  Set if shader has static vertices; should not use a model   */
    int static_verts;
    /*  Uniforms: matrices, textures, and a generic string->index table */
    GLint uni_mat[PG_COMMON_MATRICES];
    GLint uni_tex[8], uni_tex_frame[8], uni_tex_layer[8];
    pg_shader_uniform_table_t uniforms;
    struct pg_shader_attrib attribs[8];
    int num_attribs;
};

typedef HTABLE_ENTRY pg_shader_ref_t;
typedef HTABLE_T(struct pg_shader) pg_shader_table_t;

/*  GLSL loading/compiling  */
int pg_compile_glsl(GLuint* vert, GLuint* frag, GLuint* prog,
                    const char* vert_filename, const char* frag_filename);
int pg_compile_glsl_static(GLuint* vert, GLuint* frag, GLuint* prog,
                           const char* vert_src, int vert_len,
                           const char* frag_src, int frag_len);
int pg_shader_load(struct pg_shader* shader, const char* name,
                   const char* vert_filename, const char* frag_filename);
int pg_shader_load_static(struct pg_shader* shader,
                          const char* vert, int vert_len,
                          const char* frag, int frag_len);

/*  Check if a shader is currently active   */
int pg_shader_is_active(struct pg_shader* shader);

/*  Per-shader interface    */
void pg_shader_deinit(struct pg_shader* shader);
void pg_shader_begin(struct pg_shader* shader);

pg_shader_uniform_t pg_shader_get_uniform(struct pg_shader* shader, char* name);
void pg_shader_uniform_by_name(struct pg_shader* shader,
                               char* name, struct pg_type* uni);
void pg_shader_uniforms_from_table(struct pg_shader* shader,
                                   pg_shader_uniform_table_t* table);
void pg_shader_uniform(struct pg_shader* shader, pg_shader_uniform_t sh_uni,
                       struct pg_type* uni);

#if 0
/*  Matrix/component handling */
void pg_shader_rebuild_matrices(struct pg_shader* shader);

#endif
