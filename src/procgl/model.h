
struct pg_model_attrib {
    char name[32];
    GLenum type;
    int as_integer, normalized, elements;
    size_t size, offset;
};

struct pg_model_face { uint32_t t[3]; };

struct pg_model_segment {
    uint32_t ebo_offset;
    uint32_t faces;
};

struct pg_model {
    struct pg_model_attrib attribs[8];
    int num_attribs;
    void* vertex_data;
    size_t vertex_stride;
    size_t vertex_len;
    size_t vertex_cap;
    ARR_T(struct pg_model_face) faces;
    HTABLE_T(GLuint) vao_table;
    GLuint vbo;
    GLuint ebo;
};

void pg_model_init(struct pg_model* model, int num_attribs,
                   const struct pg_model_attrib* attribs);
void pg_model_deinit(struct pg_model* model);
void pg_model_spec(struct pg_model* model, struct pg_shader* shader);
void pg_model_begin(struct pg_model* model, struct pg_shader* shader);
void pg_model_buffer(struct pg_model* model);
void pg_model_reserve(struct pg_model* model, int n);
void pg_model_push_vertex(struct pg_model* model, void* vertex);
void pg_model_set_vertex(struct pg_model* model, int i, void* vertex);

void pg_model_load_obj(struct pg_model* model, const char* filename);
