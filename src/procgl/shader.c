#include "procgl.h"

/*  Shadow state for the currently used OpenGL shader   */
static struct pg_shader* pg_active_shader = NULL;

static inline void pg_shader_clear(struct pg_shader* shader)
{
    *shader = (struct pg_shader){};
    int i;
    for(i = 0; i < PG_COMMON_MATRICES; ++i) shader->uni_mat[i] = -1;
    for(i = 0; i < 8; ++i) {
        shader->uni_tex[i] = -1;
        shader->uni_tex_frame[i] = -1;
        shader->uni_tex_layer[i] = -1;
    }
    HTABLE_INIT(shader->uniforms, 8);
}

static GLint shader_uni_idx(struct pg_shader* shader, const char* name)
{
    struct pg_shader_uniform* uni;
    HTABLE_GET(shader->uniforms, name, uni);
    return uni ? uni->idx : -1;
}

static void pg_shader_gather_uniforms(struct pg_shader* shader)
{
    GLint i, count, size;
    GLenum type; // type of the variable (float, vec3 or mat4, etc)
    const GLsizei bufSize = 32; // maximum name length
    GLchar name[bufSize]; // variable name in GLSL
    GLsizei length; // name length
    glGetProgramiv(shader->prog, GL_ACTIVE_UNIFORMS, &count);
    for (i = 0; i < count; i++)
    {
        glGetActiveUniform(shader->prog, (GLuint)i, bufSize, &length, &size, &type, name);
        GLint location = glGetUniformLocation(shader->prog, name);
        if(name[0] == '_') continue;
        enum pg_data_type pg_type = PG_NULL;
        switch(type) {
            case GL_INT: pg_type = PG_INT; break;
            case GL_INT_VEC2: pg_type = PG_IVEC2; break;
            case GL_INT_VEC3: pg_type = PG_IVEC3; break;
            case GL_INT_VEC4: pg_type = PG_IVEC4; break;
            case GL_UNSIGNED_INT: pg_type = PG_UINT; break;
            case GL_UNSIGNED_INT_VEC2: pg_type = PG_UVEC2; break;
            case GL_UNSIGNED_INT_VEC3: pg_type = PG_UVEC3; break;
            case GL_UNSIGNED_INT_VEC4: pg_type = PG_UVEC4; break;
            case GL_FLOAT: pg_type = PG_FLOAT; break;
            case GL_FLOAT_VEC2: pg_type = PG_VEC2; break;
            case GL_FLOAT_VEC3: pg_type = PG_VEC3; break;
            case GL_FLOAT_VEC4: pg_type = PG_VEC4; break;
            case GL_FLOAT_MAT4: pg_type = PG_MATRIX; break;
            case GL_SAMPLER_2D: pg_type = PG_TEXTURE; break;
            case GL_SAMPLER_2D_ARRAY: pg_type = PG_TEXTURE; break;
            case GL_SAMPLER_BUFFER: pg_type = PG_TEXTURE; break;
            default: break;
        }
        if(size > 1) {
            char* arr_def = strchr(name, '[');
            if(arr_def) arr_def[0] = '\0';
        }
        HTABLE_SET(shader->uniforms, name,
            (struct pg_shader_uniform){
                .type = pg_type, .idx = location, .array_len = size });
    }
    /*  Get fast-lookups for all the regular uniforms.
        If a shader doesn't use the standard names, these uniforms will
        have to be set by the user! */
    static const char* mat_names[] = {
        [PG_MODEL_MATRIX] = "pg_matrix_model",
        [PG_NORMAL_MATRIX] = "pg_matrix_normal",
        [PG_VIEW_MATRIX] = "pg_matrix_view",
        [PG_PROJECTION_MATRIX] = "pg_matrix_projection",
        [PG_MODELVIEW_MATRIX] = "pg_matrix_modelview",
        [PG_PROJECTIONVIEW_MATRIX] = "pg_matrix_projview",
        [PG_MVP_MATRIX] = "pg_matrix_mvp" };
    for(i = 0; i < PG_COMMON_MATRICES; ++i)
        shader->uni_mat[i] = shader_uni_idx(shader, mat_names[i]);
    char tex_name[32] =         "pg_texture_0";
    char tex_frame_name[32] =   "pg_tex_frame_0";
    char tex_layer_name[32] =   "pg_tex_layer_0";
    for(i = 0; i < 8; ++i) {
        shader->uni_tex[i] = shader_uni_idx(shader, tex_name);
        shader->uni_tex_frame[i] = shader_uni_idx(shader, tex_frame_name);
        shader->uni_tex_layer[i] = shader_uni_idx(shader, tex_layer_name);
        /*  '0' - '7'   */
        ++tex_name[11];
        ++tex_frame_name[13];
        ++tex_layer_name[13];
    }
}

static void pg_shader_gather_attribs(struct pg_shader* shader)
{
    int active_attrs = 0;
    glGetProgramiv(shader->prog, GL_ACTIVE_ATTRIBUTES, &active_attrs);
    GLchar name[32];
    GLsizei name_len;
    GLenum type;
    GLint type_size;
    int attribs_used = 0;
    int i;
    for(i = 0; i < active_attrs; ++i) {
        glGetActiveAttrib(shader->prog, i, 32, &name_len, &type_size, &type, name);
        GLint actual_index = glGetAttribLocation(shader->prog, name);
        if(actual_index == -1) continue;
        shader->attribs[attribs_used].index = actual_index;
        strncpy(shader->attribs[attribs_used].name, name, 32);
        shader->attribs[attribs_used].type = type;
        shader->attribs[attribs_used].elements = type_size;
        ++attribs_used;
    }
    shader->num_attribs = attribs_used;
}

/*  Code for loading shaders dumped to headers  */
static GLuint compile_glsl_static(const char* src, int len, GLenum type)
{
    /*  Create a shader and give it the source  */
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &src, &len);
    glCompileShader(shader);
    /*  Print the info log if there were any errors */
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE) {
        GLint log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
        char buffer[log_len + 1];
        glGetShaderInfoLog(shader, log_len + 1, NULL, buffer);
        printf("Error loading shader: (static)\nShader compilation log:\n%s",
               buffer);
        return 0;
    }
    return shader;
}


int pg_compile_glsl_static(GLuint* vert, GLuint* frag, GLuint* prog,
                           const char* vert_src, int vert_len,
                           const char* frag_src, int frag_len)
{
    *vert = compile_glsl_static(vert_src, vert_len, GL_VERTEX_SHADER);
    *frag = compile_glsl_static(frag_src, frag_len, GL_FRAGMENT_SHADER);
    if(!(*vert) || !(*frag)) return 0;
    *prog = glCreateProgram();
    glAttachShader(*prog, *vert);
    glAttachShader(*prog, *frag);
    glLinkProgram(*prog);
    /*  Print the info log if there were any errors */
    GLint link_status;
    glGetProgramiv(*prog, GL_LINK_STATUS, &link_status);
    if(!link_status) {
        GLint log_length;
        glGetProgramiv(*prog, GL_INFO_LOG_LENGTH, &log_length);
        GLchar log[log_length];
        glGetProgramInfoLog(*prog, log_length, &log_length, log);
        printf("Shader program info log:\n%s\n", log);
        return 0;
    }
    return 1;
}

int pg_shader_load_static(struct pg_shader* shader,
                          const char* vert, int vert_len,
                          const char* frag, int frag_len)
{
    pg_shader_clear(shader);
    return pg_compile_glsl_static(&shader->vert, &shader->frag, &shader->prog,
                                  vert, vert_len, frag, frag_len);
}

/*  Code for loading shaders from files dynamically */
static GLuint compile_glsl(const char* filename, GLenum type)
{
    /*  Read the file into a buffer */
    FILE* f = fopen(filename, "r");
    if(!f) {
        printf("Could not read shader source file: %s\n", filename);
        return 0;
    }
    int len;
    fseek(f, 0, SEEK_END);
    len = ftell(f);
    GLchar source[len+1];
    const GLchar* double_ptr = source; /*  GL requires this   */
    fseek(f, 0, SEEK_SET);
    int r = fread(source, 1, len, f);
    source[r] = '\0';
    /*  Create a shader and give it the source  */
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &double_ptr, NULL);
    glCompileShader(shader);
    /*  Print the info log if there were any errors */
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE) {
        GLint log_len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_len);
        char buffer[log_len + 1];
        glGetShaderInfoLog(shader, log_len + 1, NULL, buffer);
        printf("Error loading shader: %s\nShader compilation log:\n%s",
               filename, buffer);
        fclose(f);
        return 0;
    }
    fclose(f);
    return shader;
}

int pg_compile_glsl(GLuint* vert, GLuint* frag, GLuint* prog,
                   const char* vert_filename, const char* frag_filename)
{
    *vert = compile_glsl(vert_filename, GL_VERTEX_SHADER);
    *frag = compile_glsl(frag_filename, GL_FRAGMENT_SHADER);
    if(!(*vert) || !(*frag)) return 0;
    *prog = glCreateProgram();
    glAttachShader(*prog, *vert);
    glAttachShader(*prog, *frag);
    glLinkProgram(*prog);
    /*  Print the info log if there were any errors */
    GLint link_status;
    glGetProgramiv(*prog, GL_LINK_STATUS, &link_status);
    if(!link_status) {
        GLint log_length;
        glGetProgramiv(*prog, GL_INFO_LOG_LENGTH, &log_length);
        GLchar log[log_length];
        glGetProgramInfoLog(*prog, log_length, &log_length, log);
        printf("Shader program info log:\n%s\n", log);
        return 0;
    }
    return 1;
}

int pg_shader_load(struct pg_shader* shader, const char* name,
                   const char* vert_filename, const char* frag_filename)
{
    pg_shader_clear(shader);
    strncpy(shader->name, name, 32);
    int ret = pg_compile_glsl(&shader->vert, &shader->frag, &shader->prog,
                           vert_filename, frag_filename);
    if(ret == 1) {
        pg_shader_gather_uniforms(shader);
        pg_shader_gather_attribs(shader);
    }
    return ret;
}

static void set_uniform(struct pg_shader_uniform* dst, struct pg_type* src)
{
    if(!dst || dst->idx == -1) return;
    switch(dst->type) {
        case PG_INT:
            glUniform1i(dst->idx, src->i[0]); break;
        case PG_IVEC2:
            glUniform2i(dst->idx, src->i[0], src->i[1]); break;
        case PG_IVEC3:
            glUniform3i(dst->idx, src->i[0], src->i[1], src->i[2]); break;
        case PG_IVEC4:
            glUniform4i(dst->idx, src->i[0], src->i[1], src->i[2], src->i[3]); break;
        case PG_UINT:
            glUniform1ui(dst->idx, src->u[0]); break;
        case PG_UVEC2:
            glUniform2ui(dst->idx, src->u[0], src->u[1]); break;
        case PG_UVEC3:
            glUniform3ui(dst->idx, src->u[0], src->u[1], src->u[2]); break;
        case PG_UVEC4:
            glUniform4ui(dst->idx, src->u[0], src->u[1], src->u[2], src->u[3]); break;
        case PG_FLOAT:
            glUniform1f(dst->idx, src->f[0]); break;
        case PG_VEC2:
            glUniform2f(dst->idx, src->f[0], src->f[1]); break;
        case PG_VEC3:
            glUniform3f(dst->idx, src->f[0], src->f[1], src->f[2]); break;
        case PG_VEC4:
            glUniform4f(dst->idx, src->f[0], src->f[1], src->f[2], src->f[3]); break;
        case PG_MATRIX:
            glUniformMatrix4fv(dst->idx, 1, GL_FALSE, src->m.v); break;
        case PG_TEXTURE:
            glUniform1i(dst->idx, src->tex[0]->handle); break;
        default: return;
    }
    dst->data = *src;
}

void pg_shader_uniform_by_name(struct pg_shader* shader,
                               char* name, struct pg_type* uni)
{
    struct pg_shader_uniform* sh_uni;
    HTABLE_GET(shader->uniforms, name, sh_uni);
    set_uniform(sh_uni, uni);
}

pg_shader_uniform_t pg_shader_get_uniform(struct pg_shader* shader, char* name)
{
    pg_shader_uniform_t uni;
    HTABLE_GET_ENTRY(shader->uniforms, name, uni);
    return uni;
}

void pg_shader_uniforms_from_table(struct pg_shader* shader,
                                   pg_shader_uniform_table_t* table)
{
    HTABLE_ITER sh_iter;
    struct pg_shader_uniform* sh_uni;
    struct pg_shader_uniform* t_uni;
    char* key;
    HTABLE_ITER_BEGIN(shader->uniforms, sh_iter);
    while(!HTABLE_ITER_END(shader->uniforms, sh_iter)) {
        HTABLE_ITER_NEXT(shader->uniforms, sh_iter, key, sh_uni);
        if(sh_uni->idx == -1) continue;
        HTABLE_GET(*table, key, t_uni);
        if(!t_uni || sh_uni->type != t_uni->type) continue;
        set_uniform(sh_uni, &t_uni->data);
    }
}

void pg_shader_uniform(struct pg_shader* shader, pg_shader_uniform_t idx,
                       struct pg_type* uni)
{
    struct pg_shader_uniform* sh_uni = NULL;
    HTABLE_ENTRY_PTR(shader->uniforms, idx, sh_uni);
    if(!sh_uni) return;
    set_uniform(sh_uni, uni);
}

void pg_shader_deinit(struct pg_shader* shader)
{
    glDeleteShader(shader->vert);
    glDeleteShader(shader->frag);
    glDeleteProgram(shader->prog);
    HTABLE_DEINIT(shader->uniforms);
}

void pg_shader_begin(struct pg_shader* shader)
{
    pg_active_shader = shader;
    glUseProgram(shader->prog);
}
