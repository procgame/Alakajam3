/*  Linear math library originally by github user datenwolf, under the
    Do What The Fuck You Want To Public License.

    This version is very heavily modified, to support vectors of various
    types. All types in this version are held in structs rather than typedef
    arrays, to allow using all the regular C copy/assignment syntax, and
    so pointers and passing to functions is more intuitive. All the base
    functions now take and return types *by value*, and are declared by
    default with the "always_inline" attribute to eliminate the performance
    penalty for doing so.   */


#ifndef LINMATH_H
#define LINMATH_H

#include <tgmath.h>
#include <stdint.h>
#include <stdlib.h>

#ifndef LM_FUNC
/*  By-value functions are always_inline by default */
#define LM_FUNC __attribute__((always_inline)) static inline
#endif

#ifndef LM_SCALAR
#define LM_SCALAR   double
#endif

/*  Basic constants */
#define LM_PI           (3.14159265359)
#define LM_PI_2         (1.57079632679)
#define LM_2_PI         (6.28318530718)
#define LM_TAU          (LM_2_PI)

/****************************/
/* Single value stuff       */
/****************************/

#define LM_MAX(a, b)        ((a) > (b) ? (a) : (b))
#define LM_MIN(a, b)        ((a) < (b) ? (a) : (b))
#define LM_MOD(a, b)        ((a) % (b) < 0 ? ((a) % (b)) + b : (a) % (b))
#define LM_FMOD(a, b)       (fmod(a, b) < 0 ? (fmod(a, b) + b) : (fmod(a, b)))
#define LM_FFLOOR(a, b)     ((a) - LM_FMOD((a), (b)))
#define LM_FCEIL(a, b)      ((a) + (b) - LM_FMOD((a), (b)))
#define LM_FTRUNC(a, b)     ((a) - fmod((a), (b)))
#define LM_SGN(a)           (a < 0 ? -1 : 1)
#define LM_SGN3(a)          (a < 0 ? -1 : (a > 0 ? 1 : 0))
#define LM_CLAMP(x, a, b)   (LM_MIN(LM_MAX(x, a), b))
#define LM_SATURATE(x)      (LM_CLAMP(x, 0, 1))
#define LM_SATURATE2(x)     (LM_CLAMP(x, -1, 1))
#define LM_LERP(a, b, t)    ((1.0f - (t)) * (a) + (t) * (b))

LM_FUNC float smin(float a, float b, float k)
{
    float h = LM_CLAMP(0.5 + 0.5 * (b - a) / k, 0.0, 1.0);
    return LM_LERP(b, a, h) - k * h * (1.0 - h);
}
/****************************/
/* Vectors of various types */
/****************************/

#define LINMATH_DEFINE_VEC4(V, T, abs_fn, mod_fn) \
typedef union { T v[4]; struct { T x, y, z, w; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0, 0, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1, 0, 0 ); } \
LM_FUNC V V##_Z(void) { return V( 0, 0, 1, 0 ); } \
LM_FUNC V V##_W(void) { return V( 0, 0, 0, 1 ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y, a.z+b.z, a.w+b.w ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y, a.z-b.z, a.w-b.w ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y, a.z*b.z, a.w*b.w ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y, a.z/b.z, a.w/b.w ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y, 1/a.z, 1/a.w ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k, a.z*k, a.w*k ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k, a.z*k, a.w*k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0 && a.z==0 && a.w==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y && a.z==b.z && a.w==b.w); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c && a.z==c && a.w==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y && a.z<b.z && a.w<b.w); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c && a.z<c && a.w<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y && a.z<=b.z && a.w<=b.w); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c && a.z<=c && a.w<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y && a.z>b.z && a.w>b.w); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c && a.z>c && a.w>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y && a.z>=b.z && a.w>=b.w); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c && a.z>=c && a.w>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y), \
                LM_MAX(a.z,b.z), LM_MAX(a.w,b.w) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y), \
                LM_MIN(a.z,b.z), LM_MIN(a.w,b.w) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(LM_MAX(LM_MAX(a.x, a.y), a.z), a.w); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(LM_MIN(LM_MIN(a.x, a.y), a.z), a.w); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_add(V##_scale(V##_sub(a,b), 0.5), a); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y), LM_SGN(a.z), LM_SGN(a.w) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y), LM_SGN3(a.z), LM_SGN3(a.w) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y), ceil(a.z), ceil(a.w) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y), floor(a.z), floor(a.w) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y), \
                LM_CLAMP(a.z,b.z,c.z), LM_CLAMP(a.w,b.w,c.w) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c), \
                LM_CLAMP(a.z,b,c), LM_CLAMP(a.w,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y), \
                LM_SATURATE(a.z), LM_SATURATE(a.w) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y), \
                LM_SATURATE2(a.z), LM_SATURATE2(a.w) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y), abs_fn(a.z), abs_fn(a.w) ); } \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y), \
                mod_fn(a.z, b.z), mod_fn(a.w, b.w) ); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b), \
                mod_fn(a.z, b), mod_fn(a.w, b) ); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k), \
                LM_LERP(a.z, b.z, k), LM_LERP(a.w, b.w, k) ); } \

#define dvec4(...)  ((dvec4){ .v = { __VA_ARGS__ } })
#define vec4(...)   ((vec4){ .v = { __VA_ARGS__ } })
#define ivec4(...)  ((ivec4){ .v = { __VA_ARGS__ } })
#define uvec4(...)  ((uvec4){ .v = { __VA_ARGS__ } })
#define bvec4(...)  ((bvec4){ .v = { __VA_ARGS__ } })
#define ubvec4(...) ((ubvec4){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC4(dvec4, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC4(vec4, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC4(ivec4, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(uvec4, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(bvec4, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC4(ubvec4, uint8_t, abs, LM_MOD)

#define vec_cast4(V)    (V).x, (V).y, (V).z, (V).w

#define LINMATH_DEFINE_VEC3(V, T, abs_fn, mod_fn) \
typedef union { T v[3]; struct { T x, y, z; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1, 0 ); } \
LM_FUNC V V##_Z(void) { return V( 0, 0, 1 ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y, a.z+b.z ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y, a.z-b.z ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y, a.z*b.z ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y, a.z/b.z ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y, 1/a.z ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k, a.z*k ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k, a.z*k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0 && a.z==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y && a.z==b.z); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c && a.z==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y && a.z<b.z); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c && a.z<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y && a.z<=b.z); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c && a.z<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y && a.z>b.z); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c && a.z>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y && a.z>=b.z); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c && a.z>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y), \
                  LM_MAX(a.z,b.z) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y), \
                  LM_MIN(a.z,b.z) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(LM_MAX(a.x, a.y), a.z); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(LM_MIN(a.x, a.y), a.z); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y + a.z*b.z); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_add(V##_scale(V##_sub(a,b), 0.5), a); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y), LM_SGN(a.z) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y), LM_SGN3(a.z) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y), ceil(a.z) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y), floor(a.z) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y), \
                LM_CLAMP(a.z,b.z,c.z) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c), \
                LM_CLAMP(a.z,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y), \
                LM_SATURATE(a.z) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y), \
                LM_SATURATE2(a.z) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y), abs_fn(a.z) ); } \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y), mod_fn(a.z, b.z) ); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b), mod_fn(a.z, b) ); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k), \
                LM_LERP(a.z, b.z, k) ); } \
LM_FUNC V V##_cross(V a, V b) \
    { return V( a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x ); }

#define dvec3(...)  ((dvec3){ .v = { __VA_ARGS__ } })
#define vec3(...)   ((vec3){ .v = { __VA_ARGS__ } })
#define ivec3(...)  ((ivec3){ .v = { __VA_ARGS__ } })
#define uvec3(...)  ((uvec3){ .v = { __VA_ARGS__ } })
#define bvec3(...)  ((bvec3){ .v = { __VA_ARGS__ } })
#define ubvec3(...) ((ubvec3){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC3(dvec3, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC3(vec3, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC3(ivec3, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(uvec3, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(bvec3, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC3(ubvec3, uint8_t, abs, LM_MOD)


#define vec_cast3(V)    (V).x, (V).y, (V).z

#define LINMATH_DEFINE_VEC2(V, T, abs_fn, mod_fn) \
typedef union { T v[2]; struct { T x, y; }; } V; \
/*  Creation                        */ \
LM_FUNC V V##_X(void) { return V( 1, 0 ); } \
LM_FUNC V V##_Y(void) { return V( 0, 1 ); } \
\
/*  Basic ops                       */ \
LM_FUNC V V##_add(V a, V b) \
    { return V( a.x+b.x, a.y+b.y ); } \
LM_FUNC V V##_sub(V a, V b) \
    { return V( a.x-b.x, a.y-b.y ); } \
LM_FUNC V V##_mul(V a, V b) \
    { return V( a.x*b.x, a.y*b.y ); } \
LM_FUNC V V##_div(V a, V b) \
    { return V( a.x/b.x, a.y/b.y ); } \
LM_FUNC V V##_recip(V a) \
    { return V( 1/a.x, 1/a.y ); } \
LM_FUNC V V##_scale(V a, LM_SCALAR k) \
    { return V( a.x*k, a.y*k ); } \
LM_FUNC V V##_tscale(V a, T k) \
    { return V( a.x*k, a.y*k ); } \
\
/*  Basic comparisons               */ \
LM_FUNC int V##_is_zero(V a) \
    { return (a.x==0 && a.y==0); } \
LM_FUNC int V##_cmp_eq(V a, V b) \
    { return (a.x==b.x && a.y==b.y); } \
LM_FUNC int V##_vcmp_eq(V a, T c) \
    { return (a.x==c && a.y==c); } \
LM_FUNC int V##_cmp_lt(V a, V b) \
    { return (a.x<b.x && a.y<b.y); } \
LM_FUNC int V##_vcmp_lt(V a, T c) \
    { return (a.x<c && a.y<c); } \
LM_FUNC int V##_cmp_le(V a, V b) \
    { return (a.x<=b.x && a.y<=b.y); } \
LM_FUNC int V##_vcmp_le(V a, T c) \
    { return (a.x<=c && a.y<=c); } \
LM_FUNC int V##_cmp_gt(V a, V b) \
    { return (a.x>b.x && a.y>b.y); } \
LM_FUNC int V##_vcmp_gt(V a, T c) \
    { return (a.x>c && a.y>c); } \
LM_FUNC int V##_cmp_ge(V a, V b) \
    { return (a.x>=b.x && a.y>=b.y); } \
LM_FUNC int V##_vcmp_ge(V a, T c) \
    { return (a.x>=c && a.y>=c); } \
\
/*  Special comparisons             */ \
LM_FUNC V V##_max(V a, V b) \
    { return V( LM_MAX(a.x,b.x), LM_MAX(a.y,b.y) ); } \
LM_FUNC V V##_min(V a, V b) \
    { return V( LM_MIN(a.x,b.x), LM_MIN(a.y,b.y) ); } \
LM_FUNC T V##_vmax(V a) \
    { return LM_MAX(a.x, a.y); } \
LM_FUNC T V##_vmin(V a) \
    { return LM_MIN(a.x, a.y); } \
LM_FUNC T V##_dot(V a, V b) \
    { return (a.x*b.x + a.y*b.y); } \
\
/*  Length and distance             */ \
LM_FUNC T V##_len2(V a) \
    { return V##_dot(a,a); } \
LM_FUNC T V##_len(V a) \
    { return (T)sqrt(V##_dot(a,a)); } \
LM_FUNC T V##_dist2(V a, V b) \
    { return V##_len2(V##_sub(a,b)); } \
LM_FUNC T V##_dist(V a, V b) \
    { return V##_len(V##_sub(a,b)); } \
LM_FUNC V V##_tolen(V a, T k) \
    { T len = V##_len(a); return V##_scale(a, k/len); } \
LM_FUNC V V##_norm(V a) \
    { T len = V##_len(a); return V##_scale(a, 1/len); } \
LM_FUNC T V##_angle_diff(V a, V b) \
    { return acos(V##_dot(a,b) / (V##_len(a) * V##_len(b))); } \
LM_FUNC V V##_midpoint(V a, V b) \
    { return V##_add(V##_scale(V##_sub(a,b), 0.5), a); } \
\
/*  Signs, rounding, clamping           */ \
LM_FUNC V V##_sgn(V a) \
    { return V( LM_SGN(a.x), LM_SGN(a.y) ); } \
LM_FUNC V V##_sgn3(V a) \
    { return V( LM_SGN3(a.x), LM_SGN3(a.y) ); } \
LM_FUNC V V##_ceil(V a) \
    { return V( ceil(a.x), ceil(a.y) ); } \
LM_FUNC V V##_floor(V a) \
    { return V( floor(a.x), floor(a.y) ); } \
LM_FUNC V V##_clamp(V a, V b, V c) \
    { return V( LM_CLAMP(a.x,b.x,c.x), LM_CLAMP(a.y,b.y,c.y) ); } \
LM_FUNC V V##_vclamp(V a, T b, T c) \
    { return V( LM_CLAMP(a.x,b,c), LM_CLAMP(a.y,b,c) ); } \
LM_FUNC V V##_saturate(V a) \
    { return V( LM_SATURATE(a.x), LM_SATURATE(a.y) ); } \
LM_FUNC V V##_saturate2(V a) \
    { return V( LM_SATURATE2(a.x), LM_SATURATE2(a.y) ); } \
LM_FUNC V V##_abs(V a) \
    { return V( abs_fn(a.x), abs_fn(a.y) ); } \
LM_FUNC V V##_mod(V a, V b) \
    { return V( mod_fn(a.x, b.x), mod_fn(a.y, b.y) ); } \
LM_FUNC V V##_vmod(V a, T b) \
    { return V( mod_fn(a.x, b), mod_fn(a.y, b) ); } \
\
/*  Special transformations             */ \
LM_FUNC V V##_lerp(V a, V b, LM_SCALAR k) \
    { return V( LM_LERP(a.x, b.x, k), LM_LERP(a.y, b.y, k) ); } \
LM_FUNC V V##_rotate(V a, LM_SCALAR k) \
    { LM_SCALAR c = cos(k), s = sin(k); return V( a.x*c - a.y*s, a.x*s + a.y*c ); }

#define dvec2(...)  ((dvec2){ .v = { __VA_ARGS__ } })
#define vec2(...)   ((vec2){ .v = { __VA_ARGS__ } })
#define ivec2(...)  ((ivec2){ .v = { __VA_ARGS__ } })
#define uvec2(...)  ((uvec2){ .v = { __VA_ARGS__ } })
#define bvec2(...)  ((bvec2){ .v = { __VA_ARGS__ } })
#define ubvec2(...) ((ubvec2){ .v = { __VA_ARGS__ } })

LINMATH_DEFINE_VEC2(dvec2, double, fabs, LM_FMOD)
LINMATH_DEFINE_VEC2(vec2, float, fabs, LM_FMOD)
LINMATH_DEFINE_VEC2(ivec2, int32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(uvec2, uint32_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(bvec2, int8_t, abs, LM_MOD)
LINMATH_DEFINE_VEC2(ubvec2, uint8_t, abs, LM_MOD)

#define vec_cast2(V)    (V).x, (V).y

/****************************/
/* Matrix & Quaternion      */
/****************************/

typedef union { float v[4]; struct { float x,y,z,w; }; } quat;
typedef union { float v[16]; vec4 col[4]; } mat4;

#define mat4(...)   ((mat4){ .col = { __VA_ARGS__ } })
#define quat(...)   ((quat){ .v = { __VA_ARGS__ } })
#define quat_cast(q)    (q).x, (q).y, (q).z, (q).w

#define MAT(M, C, R)    (M.col[R].v[C])
#define MATP(M, C, R)   (M->col[R].v[C])

LM_FUNC quat quat_identity(void)
{
    return quat( 0, 0, 0, 1.0f );
}
LM_FUNC quat quat_norm(quat a)
{
    vec4 qv = vec4_norm(vec4(quat_cast(a)));
    return quat( vec_cast4(qv) );
}
LM_FUNC quat quat_lerp(quat a, quat b, float k)
{
    vec4 qv = vec4_lerp(vec4(quat_cast(a)), vec4(quat_cast(b)), k);
    return quat( vec_cast4(qv) );
}
LM_FUNC quat quat_mul(quat a, quat b)
{
    return quat(
        a.w*b.x + a.y*b.z - a.z*b.y + a.x*b.w,
        a.w*b.y + a.y*b.w + a.z*b.x - a.x*b.z,
        a.w*b.z - a.y*b.x + a.z*b.w + a.x*b.y,
        a.w*b.w - a.y*b.y - a.z*b.z - a.x*b.x );
}
LM_FUNC quat quat_scale(quat a, float k)
{
    return quat( a.x*k, a.y*k, a.z* k, a.w*k );
}
LM_FUNC float quat_dot(quat a, quat b)
{
    return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}
LM_FUNC quat quat_conj(quat a)
{
    return quat( -a.x, -a.y, -a.z, a.w );
}
LM_FUNC quat quat_rotation(vec3 axis, float angle)
{
    vec3 v = vec3_scale(axis, sin(angle * 0.5));
    return quat( vec_cast3(v), cos(angle * 0.5) );
}
LM_FUNC quat quat_from_to(vec3 from, vec3 to)
{
    float r = vec3_dot(from, to) + 1.0f;
    vec3 w;
    if (r > 0) w = vec3_cross(from, to);
    else {
        r = 0.0f;
        if(fabs(from.x) > fabs(from.z)) w = vec3(-from.y, from.x, 0);
        else w = vec3(0, -from.z, from.y);
    }
    return quat_norm(quat( w.x, w.y, w.z, r ));
}
LM_FUNC vec3 quat_mul_vec3(quat q, vec3 v)
{
    vec3 q_xyz = vec3( q.x, q.y, q.z );
    vec3 t = vec3_scale(vec3_cross(q_xyz, v), 2);
    vec3 u = vec3_cross(q_xyz, t);
    t = vec3_scale(t, q.w);
    return vec3_add(vec3_add(v, t), u);
}
LM_FUNC vec4 quat_mul_vec4(quat q, vec4 v)
{
    vec3 q_xyz = vec3( q.x, q.y, q.z );
    vec3 v3 = vec3( v.x, v.y, v.z );
    vec3 t = vec3_scale(vec3_cross(q_xyz, v3), 2);
    vec3 u = vec3_cross(q_xyz, t);
    t = vec3_scale(t, q.w);
    vec3 p = vec3_add(vec3_add(v3, t), u);
    return vec4(vec_cast3(p), v.w);
}
LM_FUNC quat quat_from_mat4(mat4 m)
{
    float r = 0;
    int i;
    int perm[] = { 0, 1, 2, 0, 1 };
    int* p = perm;
    for(i = 0; i < 3; ++i) {
        float m_ = m.col[i].v[i];
        if(m_ < r) continue;
        m_ = r;
        p = &perm[i];
    }
    r = sqrt(1.0f + MAT(m, p[0], p[0]) - MAT(m, p[1], p[1]) - MAT(m, p[2], p[2]));
    if(r < 1e-6) return quat_identity();
    return quat(
        (MAT(m, 0, 1) - MAT(m, 1, 0)) / (2.0f * r),
        (MAT(m, 2, 0) - MAT(m, 0, 2)) / (2.0f * r),
        (MAT(m, 2, 1) - MAT(m, 1, 2)) / (2.0f * r),
        (r / 2.0f) );
}
LM_FUNC mat4 mat4_from_quat(quat q)
{
    float a = q.w;
    float b = q.x;
    float c = q.y;
    float d = q.z;
    float a2 = a*a;
    float b2 = b*b;
    float c2 = c*c;
    float d2 = d*d;
    return mat4(
        vec4(    a2+b2-c2-d2, 2.0f*(b*c+a*d), 2.0f*(b*d-a*c), 0.0f ),
        vec4( 2.0f*(b*c-a*d),    a2-b2+c2-d2, 2.0f*(c*d+a*b), 0.0f ),
        vec4( 2.0f*(b*d+a*c), 2.0f*(c*d-a*b),    a2-b2-c2+d2, 0.0f ),
        vec4(              0,              0,              0, 1.0f ) );
}
LM_FUNC mat4 mat4_mul_quat(mat4 m, quat q)
{
    return mat4( quat_mul_vec4(q, m.col[0]),
                 quat_mul_vec4(q, m.col[1]),
                 quat_mul_vec4(q, m.col[2]),
                 m.col[3] );
}

/*  Pointer functions   */


/*  4x4 Matrix  */

LM_FUNC mat4 mat4_zero(void)
{
    return mat4( vec4(0), vec4(0), vec4(0), vec4(0) );
}
LM_FUNC mat4 mat4_identity(void)
{
    return mat4( vec4( 1, 0, 0, 0 ),
                 vec4( 0, 1, 0, 0 ),
                 vec4( 0, 0, 1, 0 ),
                 vec4( 0, 0, 0, 1 ) );
}
LM_FUNC vec4 mat4_col(mat4 m, int r)
{
    return m.col[r];
}
LM_FUNC vec4 mat4_row(mat4 m, int c)
{
    return vec4( m.col[0].v[c], m.col[1].v[c], m.col[2].v[c], m.col[3].v[c] );
}
LM_FUNC mat4 mat4_add(mat4 a, mat4 b)
{
    return mat4( vec4_add(a.col[0], b.col[0]), vec4_add(a.col[1], b.col[1]),
                 vec4_add(a.col[2], b.col[2]), vec4_add(a.col[3], b.col[3]) );
}
LM_FUNC mat4 mat4_sub(mat4 a, mat4 b)
{
    return mat4( vec4_sub(a.col[0], b.col[0]), vec4_sub(a.col[1], b.col[1]),
                 vec4_sub(a.col[2], b.col[2]), vec4_sub(a.col[3], b.col[3]) );
}
LM_FUNC mat4 mat4_mul(mat4 a, mat4 b)
{
    mat4 tmp = mat4_zero();
    int k, r, c;
    for(c=0; c<4; ++c) for(r=0; r<4; ++r) for(k=0; k<4; ++k)
        MAT(tmp, c, r) += MAT(a, c, k) * MAT(b, k, r);
    return tmp;
}
LM_FUNC vec4 mat4_mul_vec4(mat4 m, vec4 v)
{
    vec4 tmp = vec4(0,0,0,0);
    int i, j;
    for(j=0; j<4; ++j) for(i=0; i<4; ++i)
        tmp.v[j] += MAT(m, j, i) * v.v[i];
    return tmp;
}
LM_FUNC mat4 mat4_translation(vec3 t)
{
    return mat4( vec4( 1, 0, 0, 0 ),
                 vec4( 0, 1, 0, 0 ),
                 vec4( 0, 0, 1, 0 ),
                 vec4( t.x, t.y, t.z, 1 ) );
}
LM_FUNC mat4 mat4_rotation(vec3 axis, float angle)
{
    quat r = quat_rotation(axis, angle);
    return mat4_mul_quat(mat4_identity(), r);
}
LM_FUNC mat4 mat4_scaling(vec3 scale)
{
    return mat4( vec4( scale.x, 0, 0, 0 ),
                 vec4( 0, scale.y, 0, 0 ),
                 vec4( 0, 0, scale.z, 0 ),
                 vec4( 0, 0, 0, 1 ) );
}
LM_FUNC mat4 mat4_translate(vec3 t, mat4 m)
{
    vec4 t_ = vec4(t.x, t.y, t.z, 0);
    return mat4( m.col[0], m.col[1], m.col[2],
        vec4( MAT(m, 3, 0) + vec4_dot(mat4_row(m, 0), t_),
              MAT(m, 3, 1) + vec4_dot(mat4_row(m, 1), t_),
              MAT(m, 3, 2) + vec4_dot(mat4_row(m, 2), t_),
              MAT(m, 3, 3) + vec4_dot(mat4_row(m, 3), t_) ) );
}
LM_FUNC mat4 mat4_rotate(vec3 axis, float angle, mat4 m)
{
    quat r = quat_rotation(axis, angle);
    return mat4_mul_quat(m, r);
}
LM_FUNC mat4 mat4_scale(float k, mat4 m)
{
    return mat4( vec4_scale(m.col[0], k), vec4_scale(m.col[1], k),
                 vec4_scale(m.col[2], k), m.col[3] );
}
LM_FUNC mat4 mat4_scale_aniso(vec3 s, mat4 m)
{
    return mat4( vec4_scale(m.col[0], s.x), vec4_scale(m.col[1], s.y),
                 vec4_scale(m.col[2], s.z), m.col[3] );
}
LM_FUNC mat4 mat4_frustum(float l, float r, float b, float t, float n, float f)
{
    return mat4(
        vec4( 2.0f*n/(r-l),             0,                  0,     0 ),
        vec4(            0,  2.0f*n/(t-b),                  0,     0 ),
        vec4(  (r+l)/(r-l),   (t+b)/(t-b),       -(f+n)/(f-n), -1.0f ),
        vec4(            0,             0,  -2.0f*(f*n)/(f-n),  0.0f ) );
}
LM_FUNC mat4 mat4_ortho(float l, float r, float b, float t, float n, float f)
{
    return mat4(
        vec4(   2.0f/(r-l),             0,             0,  0 ),
        vec4(            0,    2.0f/(t-b),             0,  0 ),
        vec4(            0,             0,   -2.0f/(f-n),  0 ),
        vec4( -(r+l)/(r-l),  -(t+b)/(t-b),  -(f+n)/(f-n),  1.0f ) );
}
LM_FUNC mat4 mat4_perspective(float y_fov, float aspect, float n, float f)
{
    float a = 1.0f / tan(y_fov / 2.0f);
    return mat4(
        vec4(  a/aspect,  0,                  0,      0 ),
        vec4(         0,  a,                  0,      0 ),
        vec4(         0,  0,       -(f+n)/(f-n),  -1.0f ),
        vec4(         0,  0,  -(2.0f*f*n)/(f-n),      0 ) );
}
LM_FUNC mat4 mat4_look_at(vec3 eye, vec3 center, vec3 up)
{
    vec3 f = vec3_norm(vec3_sub(center, eye));
    vec3 s = vec3_norm(vec3_cross(f, up));
    vec3 t = vec3_cross(s, f);
    mat4 look = mat4( vec4( s.x,  t.x,  -f.x,  0.0f ),
                      vec4( s.y,  t.y,  -f.y,  0.0f ),
                      vec4( s.z,  t.z,  -f.z,  0.0f ),
                      vec4(   0,    0,     0,  1.0f ) );
    return mat4_translate(vec3_scale(eye, -1), look);
}
LM_FUNC mat4 mat4_transpose(mat4 m)
{
    mat4 tmp;
    int i, j;
    for(j=0; j<4; ++j) for(i=0; i<4; ++i)
        tmp.col[i].v[j] = m.col[j].v[i];
    return tmp;
}
LM_FUNC mat4 mat4_inverse(mat4 m)
{
    float s[6];
    float c[6];
    s[0] = MAT(m, 0, 0)*MAT(m, 1, 1) - MAT(m, 1, 0)*MAT(m, 0, 1);
    s[1] = MAT(m, 0, 0)*MAT(m, 1, 2) - MAT(m, 1, 0)*MAT(m, 0, 2);
    s[2] = MAT(m, 0, 0)*MAT(m, 1, 3) - MAT(m, 1, 0)*MAT(m, 0, 3);
    s[3] = MAT(m, 0, 1)*MAT(m, 1, 2) - MAT(m, 1, 1)*MAT(m, 0, 2);
    s[4] = MAT(m, 0, 1)*MAT(m, 1, 3) - MAT(m, 1, 1)*MAT(m, 0, 3);
    s[5] = MAT(m, 0, 2)*MAT(m, 1, 3) - MAT(m, 1, 2)*MAT(m, 0, 3);
    c[0] = MAT(m, 2, 0)*MAT(m, 3, 1) - MAT(m, 3, 0)*MAT(m, 2, 1);
    c[1] = MAT(m, 2, 0)*MAT(m, 3, 2) - MAT(m, 3, 0)*MAT(m, 2, 2);
    c[2] = MAT(m, 2, 0)*MAT(m, 3, 3) - MAT(m, 3, 0)*MAT(m, 2, 3);
    c[3] = MAT(m, 2, 1)*MAT(m, 3, 2) - MAT(m, 3, 1)*MAT(m, 2, 2);
    c[4] = MAT(m, 2, 1)*MAT(m, 3, 3) - MAT(m, 3, 1)*MAT(m, 2, 3);
    c[5] = MAT(m, 2, 2)*MAT(m, 3, 3) - MAT(m, 3, 2)*MAT(m, 2, 3);
    /* Assumes it is invertible */
    float idet = 1.0f/( s[0]*c[5]-s[1]*c[4]+s[2]*c[3]+s[3]*c[2]-s[4]*c[1]+s[5]*c[0] );
    mat4 ret;
    MAT(ret, 0, 0) = ( MAT(m, 1, 1) * c[5] - MAT(m, 1, 2) * c[4] + MAT(m, 1, 3) * c[3]) * idet;
    MAT(ret, 0, 1) = (-MAT(m, 0, 1) * c[5] + MAT(m, 0, 2) * c[4] - MAT(m, 0, 3) * c[3]) * idet;
    MAT(ret, 0, 2) = ( MAT(m, 3, 1) * s[5] - MAT(m, 3, 2) * s[4] + MAT(m, 3, 3) * s[3]) * idet;
    MAT(ret, 0, 3) = (-MAT(m, 2, 1) * s[5] + MAT(m, 2, 2) * s[4] - MAT(m, 2, 3) * s[3]) * idet;
    MAT(ret, 1, 0) = (-MAT(m, 1, 0) * c[5] + MAT(m, 1, 2) * c[2] - MAT(m, 1, 3) * c[1]) * idet;
    MAT(ret, 1, 1) = ( MAT(m, 0, 0) * c[5] - MAT(m, 0, 2) * c[2] + MAT(m, 0, 3) * c[1]) * idet;
    MAT(ret, 1, 2) = (-MAT(m, 3, 0) * s[5] + MAT(m, 3, 2) * s[2] - MAT(m, 3, 3) * s[1]) * idet;
    MAT(ret, 1, 3) = ( MAT(m, 2, 0) * s[5] - MAT(m, 2, 2) * s[2] + MAT(m, 2, 3) * s[1]) * idet;
    MAT(ret, 2, 0) = ( MAT(m, 1, 0) * c[4] - MAT(m, 1, 1) * c[2] + MAT(m, 1, 3) * c[0]) * idet;
    MAT(ret, 2, 1) = (-MAT(m, 0, 0) * c[4] + MAT(m, 0, 1) * c[2] - MAT(m, 0, 3) * c[0]) * idet;
    MAT(ret, 2, 2) = ( MAT(m, 3, 0) * s[4] - MAT(m, 3, 1) * s[2] + MAT(m, 3, 3) * s[0]) * idet;
    MAT(ret, 2, 3) = (-MAT(m, 2, 0) * s[4] + MAT(m, 2, 1) * s[2] - MAT(m, 2, 3) * s[0]) * idet;
    MAT(ret, 3, 0) = (-MAT(m, 1, 0) * c[3] + MAT(m, 1, 1) * c[1] - MAT(m, 1, 2) * c[0]) * idet;
    MAT(ret, 3, 1) = ( MAT(m, 0, 0) * c[3] - MAT(m, 0, 1) * c[1] + MAT(m, 0, 2) * c[0]) * idet;
    MAT(ret, 3, 2) = (-MAT(m, 3, 0) * s[3] + MAT(m, 3, 1) * s[1] - MAT(m, 3, 2) * s[0]) * idet;
    MAT(ret, 3, 3) = ( MAT(m, 2, 0) * s[3] - MAT(m, 2, 1) * s[1] + MAT(m, 2, 2) * s[0]) * idet;
    return ret;
}
LM_FUNC mat4 mat4_orthonormalize(mat4 m)
{
    float s = 1;
    vec3 r[3] = { vec3(vec_cast3(m.col[0])),
                  vec3(vec_cast3(m.col[1])),
                  vec3(vec_cast3(m.col[2])) };
    vec3 h = vec3_norm(r[2]);
    r[2] = vec3_norm(r[2]);

    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[2] = vec3_norm(r[2]);

    s = vec3_dot(r[1], r[2]);
    h = vec3_scale(r[2], s);
    r[1] = vec3_sub(r[1], h);
    r[1] = vec3_norm(r[1]);

    s = vec3_dot(r[0], r[1]);
    h = vec3_scale(r[1], s);
    r[0] = vec3_sub(r[0], h);
    r[0] = vec3_norm(r[0]);

    return mat4( vec4(vec_cast3(r[0]), m.col[0].v[3]),
                 vec4(vec_cast3(r[1]), m.col[1].v[3]),
                 vec4(vec_cast3(r[2]), m.col[2].v[3]),
                 m.col[3] );
}

#undef MAT
#undef LM_FUNC
#undef LM_SCALAR

/****************************/
/*  SWIZZLE                 */
/****************************/

#define VEC_XXXX(V)    (V).x, (V).x, (V).x, (V).x
#define VEC_XXXY(V)    (V).x, (V).x, (V).x, (V).y
#define VEC_XXXZ(V)    (V).x, (V).x, (V).x, (V).z
#define VEC_XXXW(V)    (V).x, (V).x, (V).x, (V).w
#define VEC_XXYX(V)    (V).x, (V).x, (V).y, (V).x
#define VEC_XXYY(V)    (V).x, (V).x, (V).y, (V).y
#define VEC_XXYZ(V)    (V).x, (V).x, (V).y, (V).z
#define VEC_XXYW(V)    (V).x, (V).x, (V).y, (V).w
#define VEC_XXZX(V)    (V).x, (V).x, (V).z, (V).x
#define VEC_XXZY(V)    (V).x, (V).x, (V).z, (V).y
#define VEC_XXZZ(V)    (V).x, (V).x, (V).z, (V).z
#define VEC_XXZW(V)    (V).x, (V).x, (V).z, (V).w
#define VEC_XXWX(V)    (V).x, (V).x, (V).w, (V).x
#define VEC_XXWY(V)    (V).x, (V).x, (V).w, (V).y
#define VEC_XXWZ(V)    (V).x, (V).x, (V).w, (V).z
#define VEC_XXWW(V)    (V).x, (V).x, (V).w, (V).w
#define VEC_XYXX(V)    (V).x, (V).y, (V).x, (V).x
#define VEC_XYXY(V)    (V).x, (V).y, (V).x, (V).y
#define VEC_XYXZ(V)    (V).x, (V).y, (V).x, (V).z
#define VEC_XYXW(V)    (V).x, (V).y, (V).x, (V).w
#define VEC_XYYX(V)    (V).x, (V).y, (V).y, (V).x
#define VEC_XYYY(V)    (V).x, (V).y, (V).y, (V).y
#define VEC_XYYZ(V)    (V).x, (V).y, (V).y, (V).z
#define VEC_XYYW(V)    (V).x, (V).y, (V).y, (V).w
#define VEC_XYZX(V)    (V).x, (V).y, (V).z, (V).x
#define VEC_XYZY(V)    (V).x, (V).y, (V).z, (V).y
#define VEC_XYZZ(V)    (V).x, (V).y, (V).z, (V).z
#define VEC_XYZW(V)    (V).x, (V).y, (V).z, (V).w
#define VEC_XYWX(V)    (V).x, (V).y, (V).w, (V).x
#define VEC_XYWY(V)    (V).x, (V).y, (V).w, (V).y
#define VEC_XYWZ(V)    (V).x, (V).y, (V).w, (V).z
#define VEC_XYWW(V)    (V).x, (V).y, (V).w, (V).w
#define VEC_XZXX(V)    (V).x, (V).z, (V).x, (V).x
#define VEC_XZXY(V)    (V).x, (V).z, (V).x, (V).y
#define VEC_XZXZ(V)    (V).x, (V).z, (V).x, (V).z
#define VEC_XZXW(V)    (V).x, (V).z, (V).x, (V).w
#define VEC_XZYX(V)    (V).x, (V).z, (V).y, (V).x
#define VEC_XZYY(V)    (V).x, (V).z, (V).y, (V).y
#define VEC_XZYZ(V)    (V).x, (V).z, (V).y, (V).z
#define VEC_XZYW(V)    (V).x, (V).z, (V).y, (V).w
#define VEC_XZZX(V)    (V).x, (V).z, (V).z, (V).x
#define VEC_XZZY(V)    (V).x, (V).z, (V).z, (V).y
#define VEC_XZZZ(V)    (V).x, (V).z, (V).z, (V).z
#define VEC_XZZW(V)    (V).x, (V).z, (V).z, (V).w
#define VEC_XZWX(V)    (V).x, (V).z, (V).w, (V).x
#define VEC_XZWY(V)    (V).x, (V).z, (V).w, (V).y
#define VEC_XZWZ(V)    (V).x, (V).z, (V).w, (V).z
#define VEC_XZWW(V)    (V).x, (V).z, (V).w, (V).w
#define VEC_XWXX(V)    (V).x, (V).w, (V).x, (V).x
#define VEC_XWXY(V)    (V).x, (V).w, (V).x, (V).y
#define VEC_XWXZ(V)    (V).x, (V).w, (V).x, (V).z
#define VEC_XWXW(V)    (V).x, (V).w, (V).x, (V).w
#define VEC_XWYX(V)    (V).x, (V).w, (V).y, (V).x
#define VEC_XWYY(V)    (V).x, (V).w, (V).y, (V).y
#define VEC_XWYZ(V)    (V).x, (V).w, (V).y, (V).z
#define VEC_XWYW(V)    (V).x, (V).w, (V).y, (V).w
#define VEC_XWZX(V)    (V).x, (V).w, (V).z, (V).x
#define VEC_XWZY(V)    (V).x, (V).w, (V).z, (V).y
#define VEC_XWZZ(V)    (V).x, (V).w, (V).z, (V).z
#define VEC_XWZW(V)    (V).x, (V).w, (V).z, (V).w
#define VEC_XWWX(V)    (V).x, (V).w, (V).w, (V).x
#define VEC_XWWY(V)    (V).x, (V).w, (V).w, (V).y
#define VEC_XWWZ(V)    (V).x, (V).w, (V).w, (V).z
#define VEC_XWWW(V)    (V).x, (V).w, (V).w, (V).w
#define VEC_YXXX(V)    (V).y, (V).x, (V).x, (V).x
#define VEC_YXXY(V)    (V).y, (V).x, (V).x, (V).y
#define VEC_YXXZ(V)    (V).y, (V).x, (V).x, (V).z
#define VEC_YXXW(V)    (V).y, (V).x, (V).x, (V).w
#define VEC_YXYX(V)    (V).y, (V).x, (V).y, (V).x
#define VEC_YXYY(V)    (V).y, (V).x, (V).y, (V).y
#define VEC_YXYZ(V)    (V).y, (V).x, (V).y, (V).z
#define VEC_YXYW(V)    (V).y, (V).x, (V).y, (V).w
#define VEC_YXZX(V)    (V).y, (V).x, (V).z, (V).x
#define VEC_YXZY(V)    (V).y, (V).x, (V).z, (V).y
#define VEC_YXZZ(V)    (V).y, (V).x, (V).z, (V).z
#define VEC_YXZW(V)    (V).y, (V).x, (V).z, (V).w
#define VEC_YXWX(V)    (V).y, (V).x, (V).w, (V).x
#define VEC_YXWY(V)    (V).y, (V).x, (V).w, (V).y
#define VEC_YXWZ(V)    (V).y, (V).x, (V).w, (V).z
#define VEC_YXWW(V)    (V).y, (V).x, (V).w, (V).w
#define VEC_YYXX(V)    (V).y, (V).y, (V).x, (V).x
#define VEC_YYXY(V)    (V).y, (V).y, (V).x, (V).y
#define VEC_YYXZ(V)    (V).y, (V).y, (V).x, (V).z
#define VEC_YYXW(V)    (V).y, (V).y, (V).x, (V).w
#define VEC_YYYX(V)    (V).y, (V).y, (V).y, (V).x
#define VEC_YYYY(V)    (V).y, (V).y, (V).y, (V).y
#define VEC_YYYZ(V)    (V).y, (V).y, (V).y, (V).z
#define VEC_YYYW(V)    (V).y, (V).y, (V).y, (V).w
#define VEC_YYZX(V)    (V).y, (V).y, (V).z, (V).x
#define VEC_YYZY(V)    (V).y, (V).y, (V).z, (V).y
#define VEC_YYZZ(V)    (V).y, (V).y, (V).z, (V).z
#define VEC_YYZW(V)    (V).y, (V).y, (V).z, (V).w
#define VEC_YYWX(V)    (V).y, (V).y, (V).w, (V).x
#define VEC_YYWY(V)    (V).y, (V).y, (V).w, (V).y
#define VEC_YYWZ(V)    (V).y, (V).y, (V).w, (V).z
#define VEC_YYWW(V)    (V).y, (V).y, (V).w, (V).w
#define VEC_YZXX(V)    (V).y, (V).z, (V).x, (V).x
#define VEC_YZXY(V)    (V).y, (V).z, (V).x, (V).y
#define VEC_YZXZ(V)    (V).y, (V).z, (V).x, (V).z
#define VEC_YZXW(V)    (V).y, (V).z, (V).x, (V).w
#define VEC_YZYX(V)    (V).y, (V).z, (V).y, (V).x
#define VEC_YZYY(V)    (V).y, (V).z, (V).y, (V).y
#define VEC_YZYZ(V)    (V).y, (V).z, (V).y, (V).z
#define VEC_YZYW(V)    (V).y, (V).z, (V).y, (V).w
#define VEC_YZZX(V)    (V).y, (V).z, (V).z, (V).x
#define VEC_YZZY(V)    (V).y, (V).z, (V).z, (V).y
#define VEC_YZZZ(V)    (V).y, (V).z, (V).z, (V).z
#define VEC_YZZW(V)    (V).y, (V).z, (V).z, (V).w
#define VEC_YZWX(V)    (V).y, (V).z, (V).w, (V).x
#define VEC_YZWY(V)    (V).y, (V).z, (V).w, (V).y
#define VEC_YZWZ(V)    (V).y, (V).z, (V).w, (V).z
#define VEC_YZWW(V)    (V).y, (V).z, (V).w, (V).w
#define VEC_YWXX(V)    (V).y, (V).w, (V).x, (V).x
#define VEC_YWXY(V)    (V).y, (V).w, (V).x, (V).y
#define VEC_YWXZ(V)    (V).y, (V).w, (V).x, (V).z
#define VEC_YWXW(V)    (V).y, (V).w, (V).x, (V).w
#define VEC_YWYX(V)    (V).y, (V).w, (V).y, (V).x
#define VEC_YWYY(V)    (V).y, (V).w, (V).y, (V).y
#define VEC_YWYZ(V)    (V).y, (V).w, (V).y, (V).z
#define VEC_YWYW(V)    (V).y, (V).w, (V).y, (V).w
#define VEC_YWZX(V)    (V).y, (V).w, (V).z, (V).x
#define VEC_YWZY(V)    (V).y, (V).w, (V).z, (V).y
#define VEC_YWZZ(V)    (V).y, (V).w, (V).z, (V).z
#define VEC_YWZW(V)    (V).y, (V).w, (V).z, (V).w
#define VEC_YWWX(V)    (V).y, (V).w, (V).w, (V).x
#define VEC_YWWY(V)    (V).y, (V).w, (V).w, (V).y
#define VEC_YWWZ(V)    (V).y, (V).w, (V).w, (V).z
#define VEC_YWWW(V)    (V).y, (V).w, (V).w, (V).w
#define VEC_ZXXX(V)    (V).z, (V).x, (V).x, (V).x
#define VEC_ZXXY(V)    (V).z, (V).x, (V).x, (V).y
#define VEC_ZXXZ(V)    (V).z, (V).x, (V).x, (V).z
#define VEC_ZXXW(V)    (V).z, (V).x, (V).x, (V).w
#define VEC_ZXYX(V)    (V).z, (V).x, (V).y, (V).x
#define VEC_ZXYY(V)    (V).z, (V).x, (V).y, (V).y
#define VEC_ZXYZ(V)    (V).z, (V).x, (V).y, (V).z
#define VEC_ZXYW(V)    (V).z, (V).x, (V).y, (V).w
#define VEC_ZXZX(V)    (V).z, (V).x, (V).z, (V).x
#define VEC_ZXZY(V)    (V).z, (V).x, (V).z, (V).y
#define VEC_ZXZZ(V)    (V).z, (V).x, (V).z, (V).z
#define VEC_ZXZW(V)    (V).z, (V).x, (V).z, (V).w
#define VEC_ZXWX(V)    (V).z, (V).x, (V).w, (V).x
#define VEC_ZXWY(V)    (V).z, (V).x, (V).w, (V).y
#define VEC_ZXWZ(V)    (V).z, (V).x, (V).w, (V).z
#define VEC_ZXWW(V)    (V).z, (V).x, (V).w, (V).w
#define VEC_ZYXX(V)    (V).z, (V).y, (V).x, (V).x
#define VEC_ZYXY(V)    (V).z, (V).y, (V).x, (V).y
#define VEC_ZYXZ(V)    (V).z, (V).y, (V).x, (V).z
#define VEC_ZYXW(V)    (V).z, (V).y, (V).x, (V).w
#define VEC_ZYYX(V)    (V).z, (V).y, (V).y, (V).x
#define VEC_ZYYY(V)    (V).z, (V).y, (V).y, (V).y
#define VEC_ZYYZ(V)    (V).z, (V).y, (V).y, (V).z
#define VEC_ZYYW(V)    (V).z, (V).y, (V).y, (V).w
#define VEC_ZYZX(V)    (V).z, (V).y, (V).z, (V).x
#define VEC_ZYZY(V)    (V).z, (V).y, (V).z, (V).y
#define VEC_ZYZZ(V)    (V).z, (V).y, (V).z, (V).z
#define VEC_ZYZW(V)    (V).z, (V).y, (V).z, (V).w
#define VEC_ZYWX(V)    (V).z, (V).y, (V).w, (V).x
#define VEC_ZYWY(V)    (V).z, (V).y, (V).w, (V).y
#define VEC_ZYWZ(V)    (V).z, (V).y, (V).w, (V).z
#define VEC_ZYWW(V)    (V).z, (V).y, (V).w, (V).w
#define VEC_ZZXX(V)    (V).z, (V).z, (V).x, (V).x
#define VEC_ZZXY(V)    (V).z, (V).z, (V).x, (V).y
#define VEC_ZZXZ(V)    (V).z, (V).z, (V).x, (V).z
#define VEC_ZZXW(V)    (V).z, (V).z, (V).x, (V).w
#define VEC_ZZYX(V)    (V).z, (V).z, (V).y, (V).x
#define VEC_ZZYY(V)    (V).z, (V).z, (V).y, (V).y
#define VEC_ZZYZ(V)    (V).z, (V).z, (V).y, (V).z
#define VEC_ZZYW(V)    (V).z, (V).z, (V).y, (V).w
#define VEC_ZZZX(V)    (V).z, (V).z, (V).z, (V).x
#define VEC_ZZZY(V)    (V).z, (V).z, (V).z, (V).y
#define VEC_ZZZZ(V)    (V).z, (V).z, (V).z, (V).z
#define VEC_ZZZW(V)    (V).z, (V).z, (V).z, (V).w
#define VEC_ZZWX(V)    (V).z, (V).z, (V).w, (V).x
#define VEC_ZZWY(V)    (V).z, (V).z, (V).w, (V).y
#define VEC_ZZWZ(V)    (V).z, (V).z, (V).w, (V).z
#define VEC_ZZWW(V)    (V).z, (V).z, (V).w, (V).w
#define VEC_ZWXX(V)    (V).z, (V).w, (V).x, (V).x
#define VEC_ZWXY(V)    (V).z, (V).w, (V).x, (V).y
#define VEC_ZWXZ(V)    (V).z, (V).w, (V).x, (V).z
#define VEC_ZWXW(V)    (V).z, (V).w, (V).x, (V).w
#define VEC_ZWYX(V)    (V).z, (V).w, (V).y, (V).x
#define VEC_ZWYY(V)    (V).z, (V).w, (V).y, (V).y
#define VEC_ZWYZ(V)    (V).z, (V).w, (V).y, (V).z
#define VEC_ZWYW(V)    (V).z, (V).w, (V).y, (V).w
#define VEC_ZWZX(V)    (V).z, (V).w, (V).z, (V).x
#define VEC_ZWZY(V)    (V).z, (V).w, (V).z, (V).y
#define VEC_ZWZZ(V)    (V).z, (V).w, (V).z, (V).z
#define VEC_ZWZW(V)    (V).z, (V).w, (V).z, (V).w
#define VEC_ZWWX(V)    (V).z, (V).w, (V).w, (V).x
#define VEC_ZWWY(V)    (V).z, (V).w, (V).w, (V).y
#define VEC_ZWWZ(V)    (V).z, (V).w, (V).w, (V).z
#define VEC_ZWWW(V)    (V).z, (V).w, (V).w, (V).w
#define VEC_WXXX(V)    (V).w, (V).x, (V).x, (V).x
#define VEC_WXXY(V)    (V).w, (V).x, (V).x, (V).y
#define VEC_WXXZ(V)    (V).w, (V).x, (V).x, (V).z
#define VEC_WXXW(V)    (V).w, (V).x, (V).x, (V).w
#define VEC_WXYX(V)    (V).w, (V).x, (V).y, (V).x
#define VEC_WXYY(V)    (V).w, (V).x, (V).y, (V).y
#define VEC_WXYZ(V)    (V).w, (V).x, (V).y, (V).z
#define VEC_WXYW(V)    (V).w, (V).x, (V).y, (V).w
#define VEC_WXZX(V)    (V).w, (V).x, (V).z, (V).x
#define VEC_WXZY(V)    (V).w, (V).x, (V).z, (V).y
#define VEC_WXZZ(V)    (V).w, (V).x, (V).z, (V).z
#define VEC_WXZW(V)    (V).w, (V).x, (V).z, (V).w
#define VEC_WXWX(V)    (V).w, (V).x, (V).w, (V).x
#define VEC_WXWY(V)    (V).w, (V).x, (V).w, (V).y
#define VEC_WXWZ(V)    (V).w, (V).x, (V).w, (V).z
#define VEC_WXWW(V)    (V).w, (V).x, (V).w, (V).w
#define VEC_WYXX(V)    (V).w, (V).y, (V).x, (V).x
#define VEC_WYXY(V)    (V).w, (V).y, (V).x, (V).y
#define VEC_WYXZ(V)    (V).w, (V).y, (V).x, (V).z
#define VEC_WYXW(V)    (V).w, (V).y, (V).x, (V).w
#define VEC_WYYX(V)    (V).w, (V).y, (V).y, (V).x
#define VEC_WYYY(V)    (V).w, (V).y, (V).y, (V).y
#define VEC_WYYZ(V)    (V).w, (V).y, (V).y, (V).z
#define VEC_WYYW(V)    (V).w, (V).y, (V).y, (V).w
#define VEC_WYZX(V)    (V).w, (V).y, (V).z, (V).x
#define VEC_WYZY(V)    (V).w, (V).y, (V).z, (V).y
#define VEC_WYZZ(V)    (V).w, (V).y, (V).z, (V).z
#define VEC_WYZW(V)    (V).w, (V).y, (V).z, (V).w
#define VEC_WYWX(V)    (V).w, (V).y, (V).w, (V).x
#define VEC_WYWY(V)    (V).w, (V).y, (V).w, (V).y
#define VEC_WYWZ(V)    (V).w, (V).y, (V).w, (V).z
#define VEC_WYWW(V)    (V).w, (V).y, (V).w, (V).w
#define VEC_WZXX(V)    (V).w, (V).z, (V).x, (V).x
#define VEC_WZXY(V)    (V).w, (V).z, (V).x, (V).y
#define VEC_WZXZ(V)    (V).w, (V).z, (V).x, (V).z
#define VEC_WZXW(V)    (V).w, (V).z, (V).x, (V).w
#define VEC_WZYX(V)    (V).w, (V).z, (V).y, (V).x
#define VEC_WZYY(V)    (V).w, (V).z, (V).y, (V).y
#define VEC_WZYZ(V)    (V).w, (V).z, (V).y, (V).z
#define VEC_WZYW(V)    (V).w, (V).z, (V).y, (V).w
#define VEC_WZZX(V)    (V).w, (V).z, (V).z, (V).x
#define VEC_WZZY(V)    (V).w, (V).z, (V).z, (V).y
#define VEC_WZZZ(V)    (V).w, (V).z, (V).z, (V).z
#define VEC_WZZW(V)    (V).w, (V).z, (V).z, (V).w
#define VEC_WZWX(V)    (V).w, (V).z, (V).w, (V).x
#define VEC_WZWY(V)    (V).w, (V).z, (V).w, (V).y
#define VEC_WZWZ(V)    (V).w, (V).z, (V).w, (V).z
#define VEC_WZWW(V)    (V).w, (V).z, (V).w, (V).w
#define VEC_WWXX(V)    (V).w, (V).w, (V).x, (V).x
#define VEC_WWXY(V)    (V).w, (V).w, (V).x, (V).y
#define VEC_WWXZ(V)    (V).w, (V).w, (V).x, (V).z
#define VEC_WWXW(V)    (V).w, (V).w, (V).x, (V).w
#define VEC_WWYX(V)    (V).w, (V).w, (V).y, (V).x
#define VEC_WWYY(V)    (V).w, (V).w, (V).y, (V).y
#define VEC_WWYZ(V)    (V).w, (V).w, (V).y, (V).z
#define VEC_WWYW(V)    (V).w, (V).w, (V).y, (V).w
#define VEC_WWZX(V)    (V).w, (V).w, (V).z, (V).x
#define VEC_WWZY(V)    (V).w, (V).w, (V).z, (V).y
#define VEC_WWZZ(V)    (V).w, (V).w, (V).z, (V).z
#define VEC_WWZW(V)    (V).w, (V).w, (V).z, (V).w
#define VEC_WWWX(V)    (V).w, (V).w, (V).w, (V).x
#define VEC_WWWY(V)    (V).w, (V).w, (V).w, (V).y
#define VEC_WWWZ(V)    (V).w, (V).w, (V).w, (V).z
#define VEC_WWWW(V)    (V).w, (V).w, (V).w, (V).w
#define VEC_XXX(V)    (V).x, (V).x, (V).x
#define VEC_XXY(V)    (V).x, (V).x, (V).y
#define VEC_XXZ(V)    (V).x, (V).x, (V).z
#define VEC_XXW(V)    (V).x, (V).x, (V).w
#define VEC_XYX(V)    (V).x, (V).y, (V).x
#define VEC_XYY(V)    (V).x, (V).y, (V).y
#define VEC_XYZ(V)    (V).x, (V).y, (V).z
#define VEC_XYW(V)    (V).x, (V).y, (V).w
#define VEC_XZX(V)    (V).x, (V).z, (V).x
#define VEC_XZY(V)    (V).x, (V).z, (V).y
#define VEC_XZZ(V)    (V).x, (V).z, (V).z
#define VEC_XZW(V)    (V).x, (V).z, (V).w
#define VEC_XWX(V)    (V).x, (V).w, (V).x
#define VEC_XWY(V)    (V).x, (V).w, (V).y
#define VEC_XWZ(V)    (V).x, (V).w, (V).z
#define VEC_XWW(V)    (V).x, (V).w, (V).w
#define VEC_YXX(V)    (V).y, (V).x, (V).x
#define VEC_YXY(V)    (V).y, (V).x, (V).y
#define VEC_YXZ(V)    (V).y, (V).x, (V).z
#define VEC_YXW(V)    (V).y, (V).x, (V).w
#define VEC_YYX(V)    (V).y, (V).y, (V).x
#define VEC_YYY(V)    (V).y, (V).y, (V).y
#define VEC_YYZ(V)    (V).y, (V).y, (V).z
#define VEC_YYW(V)    (V).y, (V).y, (V).w
#define VEC_YZX(V)    (V).y, (V).z, (V).x
#define VEC_YZY(V)    (V).y, (V).z, (V).y
#define VEC_YZZ(V)    (V).y, (V).z, (V).z
#define VEC_YZW(V)    (V).y, (V).z, (V).w
#define VEC_YWX(V)    (V).y, (V).w, (V).x
#define VEC_YWY(V)    (V).y, (V).w, (V).y
#define VEC_YWZ(V)    (V).y, (V).w, (V).z
#define VEC_YWW(V)    (V).y, (V).w, (V).w
#define VEC_ZXX(V)    (V).z, (V).x, (V).x
#define VEC_ZXY(V)    (V).z, (V).x, (V).y
#define VEC_ZXZ(V)    (V).z, (V).x, (V).z
#define VEC_ZXW(V)    (V).z, (V).x, (V).w
#define VEC_ZYX(V)    (V).z, (V).y, (V).x
#define VEC_ZYY(V)    (V).z, (V).y, (V).y
#define VEC_ZYZ(V)    (V).z, (V).y, (V).z
#define VEC_ZYW(V)    (V).z, (V).y, (V).w
#define VEC_ZZX(V)    (V).z, (V).z, (V).x
#define VEC_ZZY(V)    (V).z, (V).z, (V).y
#define VEC_ZZZ(V)    (V).z, (V).z, (V).z
#define VEC_ZZW(V)    (V).z, (V).z, (V).w
#define VEC_ZWX(V)    (V).z, (V).w, (V).x
#define VEC_ZWY(V)    (V).z, (V).w, (V).y
#define VEC_ZWZ(V)    (V).z, (V).w, (V).z
#define VEC_ZWW(V)    (V).z, (V).w, (V).w
#define VEC_WXX(V)    (V).w, (V).x, (V).x
#define VEC_WXY(V)    (V).w, (V).x, (V).y
#define VEC_WXZ(V)    (V).w, (V).x, (V).z
#define VEC_WXW(V)    (V).w, (V).x, (V).w
#define VEC_WYX(V)    (V).w, (V).y, (V).x
#define VEC_WYY(V)    (V).w, (V).y, (V).y
#define VEC_WYZ(V)    (V).w, (V).y, (V).z
#define VEC_WYW(V)    (V).w, (V).y, (V).w
#define VEC_WZX(V)    (V).w, (V).z, (V).x
#define VEC_WZY(V)    (V).w, (V).z, (V).y
#define VEC_WZZ(V)    (V).w, (V).z, (V).z
#define VEC_WZW(V)    (V).w, (V).z, (V).w
#define VEC_WWX(V)    (V).w, (V).w, (V).x
#define VEC_WWY(V)    (V).w, (V).w, (V).y
#define VEC_WWZ(V)    (V).w, (V).w, (V).z
#define VEC_WWW(V)    (V).w, (V).w, (V).w
#define VEC_XX(V)    (V).x, (V).x
#define VEC_XY(V)    (V).x, (V).y
#define VEC_XZ(V)    (V).x, (V).z
#define VEC_XW(V)    (V).x, (V).w
#define VEC_YX(V)    (V).y, (V).x
#define VEC_YY(V)    (V).y, (V).y
#define VEC_YZ(V)    (V).y, (V).z
#define VEC_YW(V)    (V).y, (V).w
#define VEC_ZX(V)    (V).z, (V).x
#define VEC_ZY(V)    (V).z, (V).y
#define VEC_ZZ(V)    (V).z, (V).z
#define VEC_ZW(V)    (V).z, (V).w
#define VEC_WX(V)    (V).w, (V).x
#define VEC_WY(V)    (V).w, (V).y
#define VEC_WZ(V)    (V).w, (V).z
#define VEC_WW(V)    (V).w, (V).w
#define VEC_X(V)    (V).x
#define VEC_Y(V)    (V).y
#define VEC_Z(V)    (V).z
#define VEC_W(V)    (V).w

#endif

