#include "procgl.h"

void pg_renderer_init(struct pg_renderer* rend)
{
    /*  Load the base shaders and populate the shader table with them   */
    struct pg_shader shader;
    glGenVertexArrays(1, &rend->dummy_vao);
    ARR_INIT(rend->passes);
    HTABLE_INIT(rend->common_uniforms, 8);
    HTABLE_INIT(rend->shaders, 8);
    /*  Spritebatch shader  */
    int load = pg_shader_load(&shader, "spritebatch",
        "src/procgl/shaders/spritebatch_vert.glsl", "src/procgl/shaders/spritebatch_frag.glsl");
    if(!load) printf("Failed to load sprite-batch shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "spritebatch", shader);
    }
    /*  Boxbatch shader */
    load = pg_shader_load(&shader, "spritebatch",
        "src/procgl/shaders/boxbatch_vert.glsl", "src/procgl/shaders/spritebatch_frag.glsl");
    if(!load) printf("Failed to load box-batch shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "boxbatch", shader);
    }
    /*  3D model shader  */
    load = pg_shader_load(&shader, "model",
        "src/procgl/shaders/model_vert.glsl", "src/procgl/shaders/spritebatch_frag.glsl");
    if(!load) printf("Failed to load model shader.\n");
    else {
        HTABLE_SET(rend->shaders, "model", shader);
    }
    /*  Sine-wave distortion post-effect    */
    load = pg_shader_load(&shader, "post_sine",
        "src/procgl/shaders/screen_vert.glsl", "src/procgl/shaders/post_sine_frag.glsl");
    if(!load) printf("Failed to load sine-wave shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "post_sine", shader);
    }
    /*  Blur post-effect    */
    load = pg_shader_load(&shader, "post_blur",
        "src/procgl/shaders/screen_vert.glsl", "src/procgl/shaders/post_blur_frag.glsl");
    if(!load) printf("Failed to load blur shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "post_blur", shader);
    }
    /*  Blur post-effect    */
    load = pg_shader_load(&shader, "post_fog",
        "src/procgl/shaders/screen_vert.glsl", "src/procgl/shaders/post_fog.glsl");
    if(!load) printf("Failed to load blur shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "post_fog", shader);
    }
    /*  Pass-thru shader    */
    load = pg_shader_load(&shader, "passthru",
        "src/procgl/shaders/screen_vert.glsl", "src/procgl/shaders/post_screen_frag.glsl");
    if(!load) printf("Failed to load pass-thru shader.\n");
    else {
        shader.static_verts = 1;
        HTABLE_SET(rend->shaders, "passthru", shader);
    }
}

void pg_renderer_deinit(struct pg_renderer* rend)
{
    HTABLE_DEINIT(rend->common_uniforms);
    ARR_DEINIT(rend->passes);
    /*  Deinit shaders  */
    struct pg_shader* shader;
    HTABLE_ITER iter;
    HTABLE_ITER_BEGIN(rend->shaders, iter);
    while(!HTABLE_ITER_END(rend->shaders, iter)) {
        HTABLE_ITER_NEXT_PTR(rend->shaders, iter, shader);
        pg_shader_deinit(shader);
    }
    HTABLE_DEINIT(rend->shaders);
    /*  Zero the renderer structure */
    *rend = (struct pg_renderer){};
}

void pg_renderer_model_spec(struct pg_renderer* rend, char* shader_name,
                            struct pg_model* model)
{
    struct pg_shader* shader;
    HTABLE_GET(rend->shaders, shader_name, shader);
    pg_model_spec(model, shader);
}

void pg_renderpass_init(struct pg_renderpass* pass, char* shader_name,
                        struct pg_renderpass_opts* opts)
{
    *pass = (struct pg_renderpass) { .enabled = 1,
        .opts = (opts ? *opts : *PG_RENDERPASS_OPTS()) };
    strncpy(pass->shader, shader_name, 31);
    int i;
    for(i = 0; i < PG_COMMON_MATRICES; ++i) pass->mats[i] = mat4_identity();
    HTABLE_INIT(pass->uniforms, 8);
}

void pg_renderpass_deinit(struct pg_renderpass* pass)
{
    HTABLE_DEINIT(pass->uniforms);
    ARR_DEINIT(pass->draws);
}

void pg_renderpass_clear(struct pg_renderpass* pass)
{
    ARR_TRUNCATE_CLEAR(pass->draws, 0);
}

void pg_renderpass_uniform(struct pg_renderpass* pass, char* name,
                            enum pg_data_type type, struct pg_type* data)
{
    HTABLE_SET(pass->uniforms, name,
        (struct pg_shader_uniform){ .type = type, .data = *data });
}

void pg_renderpass_model(struct pg_renderpass* pass, struct pg_model* model)
{
    pass->model = model;
}

void pg_renderpass_texture(struct pg_renderpass* pass, int idx,
                           struct pg_texture* tex, struct pg_texture_opts* opts)
{
    pass->tex[idx].type = PG_PASS_TEX_IMAGE;
    pass->tex[idx].tex = tex;
    if(opts) {
        pass->tex[idx].tex_opts = *opts;
        pass->tex[idx].use_opts = 1;
    } else {
        pass->tex[idx].use_opts = 0;
    }
}

void pg_renderpass_fbtexture(struct pg_renderpass* pass, int idx,
                             struct pg_rendertarget* src, int src_idx)
{
    pass->tex[idx].type = PG_PASS_TEX_RENDERTARGET;
    pass->tex[idx].fb_tex = src;
    pass->tex[idx].idx = src_idx;
}

void pg_renderpass_buftexture(struct pg_renderpass* pass, int idx,
                              struct pg_buffertex* tex)
{
    pass->tex[idx].type = PG_PASS_TEX_BUFFER;
    pass->tex[idx].buf = tex;
}

void pg_renderpass_drawformat(struct pg_renderpass* pass,
                               pg_render_drawfunc_t per_draw, int n, ...)
{
    pass->per_draw = per_draw;
    va_list args;
    va_start(args, n);
    int i;
    for(i = 0; i < 8 && i < n; ++i) {
        char* u = va_arg(args, char*);
        strncpy(pass->draw_uniform[i], u, 32);
    }
    va_end(args);
}

void pg_renderpass_add_draw(struct pg_renderpass* pass, int n,
                             struct pg_type* draw_unis)
{
    ARR_RESERVE(pass->draws, pass->draws.len + n);
    memcpy(pass->draws.data + pass->draws.len, draw_unis, n * sizeof(*draw_unis));
    pass->draws.len += n;
}

void pg_renderer_reset(struct pg_renderer* rend)
{
    ARR_TRUNCATE_CLEAR(rend->passes, 0);
}

static inline void shader_base_mats(struct pg_shader* shader,
                                    struct pg_renderpass* pass)
{
    int i;
    for(i = 0; i < PG_COMMON_MATRICES; ++i) {
        if(shader->uni_mat[i] == -1) continue;
        glUniformMatrix4fv(shader->uni_mat[i], 1, GL_FALSE, pass->mats[i].v);
    }
}

static inline void shader_textures(struct pg_shader* shader, 
                                   struct pg_renderpass* pass)
{
    int i;
    for(i = 0; i < 8; ++i) {
        if(shader->uni_tex[i] == -1) continue;
        glActiveTexture(GL_TEXTURE0 + i);
        if(pass->tex[i].type == PG_PASS_TEX_IMAGE) {
            glBindTexture(GL_TEXTURE_2D_ARRAY, pass->tex[i].tex->handle);
            if(pass->tex[i].use_opts) pg_texture_buffer_opts(&pass->tex[i].tex_opts);
            else pg_texture_buffer_opts(&pass->tex[i].tex->opts);
            glUniform1i(shader->uni_tex[i], i);
        } else if(pass->tex[i].type == PG_PASS_TEX_RENDERTARGET
               && pass->tex[i].fb_tex != pass->target) {
            struct pg_renderbuffer* buf = pass->tex[i].fb_tex->buf[0];
            if(!buf || !buf->outputs[pass->tex[i].idx]) continue;
            glBindTexture(GL_TEXTURE_2D_ARRAY, buf->outputs[pass->tex[i].idx]->handle);
            glUniform1i(shader->uni_tex[i], i);
            if(shader->uni_tex_layer[i] != -1)
                glUniform1i(shader->uni_tex_layer[i], buf->layer[i]);
        } else if(pass->tex[i].type == PG_PASS_TEX_BUFFER) {
            glBindTexture(GL_TEXTURE_BUFFER, pass->tex[i].buf->tex_handle);
            glUniform1i(shader->uni_tex[i], i);
        }
    }
}

static inline void frame_swap_target(struct pg_shader* shader,
                                     struct pg_renderpass* pass)
{
    if(!pass->target) return;
    pg_rendertarget_swap(pass->target);
    pg_rendertarget_dst(pass->target);
    glClear(pass->opts.clear_buffers);
    int i;
    for(i = 0; i < 8; ++i) {
        if(shader->uni_tex[i] == -1 || pass->tex[i].type != PG_PASS_TEX_RENDERTARGET
        || pass->tex[i].fb_tex != pass->target) continue;
        struct pg_renderbuffer* buf = pass->tex[i].fb_tex->buf[1];
        if(!buf || !buf->outputs[pass->tex[i].idx]) continue;
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D_ARRAY, buf->outputs[pass->tex[i].idx]->handle);
        glUniform1i(shader->uni_tex[i], i);
        if(shader->uni_tex_layer[i] != -1)
            glUniform1i(shader->uni_tex_layer[i], buf->layer[i]);
    }

}

static void pass_opts(struct pg_renderpass* pass, struct pg_shader* shader)
{
    /*  Blending    */
    if(pass->opts.flags & PG_RENDERPASS_BLENDING) {
        glEnable(GL_BLEND);
        glBlendFunc(pass->opts.blend_func[0], pass->opts.blend_func[1]);
    } else glDisable(GL_BLEND);
    /*  Depth testing   */
    if(pass->opts.flags & PG_RENDERPASS_DEPTH_TEST) {
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(pass->opts.depth_func);
    } else glDisable(GL_DEPTH_TEST);
    /*  Face culling    */
    if(pass->opts.flags & PG_RENDERPASS_CULL_FACES) glEnable(GL_CULL_FACE);
    else glDisable(GL_CULL_FACE);
    if(!(pass->opts.flags & PG_RENDERPASS_BUFFER_SWAP)) {
        if(pass->target) pg_rendertarget_dst(pass->target);
        else pg_screen_dst();
        glClear(pass->opts.clear_buffers);
    } else if(pass->opts.flags & PG_RENDERPASS_SWAP_BEFORE) {
        frame_swap_target(shader, pass);
    }
    /*  Viewport    */
    if(pass->opts.flags & PG_RENDERPASS_VIEWPORT) {
        glViewport( pass->resolution.x * pass->opts.viewport.x,
                    pass->resolution.y * pass->opts.viewport.y,
                    pass->resolution.x * pass->opts.viewport.z,
                    pass->resolution.y * pass->opts.viewport.w);
    }
    /*  Scissor */
    if(pass->opts.flags & PG_RENDERPASS_SCISSOR) {
        glScissor( pass->resolution.x * pass->opts.scissor.x,
                   pass->resolution.y * pass->opts.scissor.y,
                   pass->resolution.x * pass->opts.scissor.z,
                   pass->resolution.y * pass->opts.scissor.w );
        glEnable(GL_SCISSOR_TEST);
    } else glDisable(GL_SCISSOR_TEST);
}

void pg_renderer_draw_frame(struct pg_renderer* rend)
{
    struct pg_shader* lastshader = NULL;
    struct pg_shader* shader;
    struct pg_renderpass* pass = NULL;
    struct pg_model* model;
    int i, j;
    ARR_FOREACH(rend->passes, pass, i) {
        if(!pass->enabled) continue;
        HTABLE_GET(rend->shaders, pass->shader, shader);
        if(!shader) continue;
        if(shader != lastshader) pg_shader_begin(shader);
        /*  Setup the model to draw, if there is one    */
        if(pass->model && pass->model != model) pg_model_begin(pass->model, shader);
        else if(shader->static_verts) glBindVertexArray(rend->dummy_vao);
        model = pass->model;
        /*  Get the viewer matrices */
        if(pass->viewer) {
            pass->mats[PG_PROJECTION_MATRIX] = pass->viewer->proj_matrix;
            pass->mats[PG_VIEW_MATRIX] = pass->viewer->view_matrix;
            pass->mats[PG_PROJECTIONVIEW_MATRIX] = pass->viewer->projview_matrix;
        }
        /*  Set the regular uniforms    */
        shader_base_mats(shader, pass);
        shader_textures(shader, pass);
        pg_shader_uniforms_from_table(shader, &pass->uniforms);
        pass_opts(pass, shader);
        /*  Set up the uniform indices which are passed to per_draw */
        GLint uni_idx[8];
        for(j = 0; j < 8; ++j) {
            struct pg_shader_uniform* sh_uni;
            HTABLE_GET(shader->uniforms, pass->draw_uniform[j], sh_uni);
            if(!sh_uni) {
                uni_idx[j] = -1;
                continue;
            } else uni_idx[j] = sh_uni->idx;
        }
        /*  The inner draw loop */
        j = 0;
        while(j < pass->draws.len) {
            /*  If this shader DOES swap targets with every draw,
                swap and set target here, and clear it on each draw,
                and also switch around the fbtextures uniforms  */
            if(pass->opts.flags & PG_RENDERPASS_SWAP_PER_DRAW)
                frame_swap_target(shader, pass);
            j += pass->per_draw(pass->draws.data + j, shader,
                                (const mat4*)pass->mats, uni_idx);
            /*  Only make a draw call if the shader doesn't have static
                vertices. If it does, the draw call is done in per_draw */
            if(!shader->static_verts)
                glDrawElements(GL_TRIANGLES, model->faces.len * 3, GL_UNSIGNED_INT, 0);
        }
        if(pass->opts.flags & PG_RENDERPASS_SWAP_AFTER) {
            frame_swap_target(shader, pass);
            glClear(pass->opts.clear_buffers);
        }
    }
}

void pg_renderpass_target(struct pg_renderpass* pass,
                          struct pg_rendertarget* target)
{
    pass->target = target;
    int w, h;
    if(target) {
        w = target->buf[0]->w;
        h = target->buf[0]->h;
    } else {
        pg_screen_size(&w, &h);
    }
    float ar = (float)w / h;
    pg_renderpass_uniform(pass, "pg_resolution", PG_VEC2, &PG_TYPE_FLOAT(w, h));
    pg_renderpass_uniform(pass, "pg_aspect_ratio", PG_FLOAT, &PG_TYPE_FLOAT(ar));
    pass->resolution = vec2(w, h);
    pass->aspect_ratio = ar;
}

void pg_renderpass_viewer(struct pg_renderpass* pass, struct pg_viewer* view)
{
    pass->viewer = view;
    if(!view) {
        pass->mats[PG_VIEW_MATRIX] = mat4_identity();
        pass->mats[PG_PROJECTION_MATRIX] = mat4_identity();
        pass->mats[PG_PROJECTIONVIEW_MATRIX] = mat4_identity();
    }
}

void pg_renderpass_enable(struct pg_renderpass* pass, int enabled)
{
    pass->enabled = enabled;
}

void pg_renderpass_blending(struct pg_renderpass* pass, int use,
                            GLenum left, GLenum right)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_BLENDING;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_BLENDING;
    pass->opts.blend_func[0] = left;
    pass->opts.blend_func[1] = right;
}

void pg_renderpass_depth_write(struct pg_renderpass* pass, int use)
{
    if(use) pass->opts.flags |= PG_RENDERPASS_DEPTH_WRITE;
    else pass->opts.flags &= ~PG_RENDERPASS_DEPTH_WRITE;
}

void pg_renderpass_depth_test(struct pg_renderpass* pass, int use, GLenum func)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_DEPTH_TEST;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_DEPTH_TEST;
    pass->opts.depth_func = func;
}

void pg_renderpass_clear_bits(struct pg_renderpass* pass, GLbitfield buffers)
{
    pass->opts.clear_buffers = buffers;
}

void pg_renderpass_viewport(struct pg_renderpass* pass, int use, struct pg_viewport* vp)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_VIEWPORT;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_VIEWPORT;
    pass->opts.viewport = vec4( vp->pos.x * 0.5 + 0.5, vp->pos.y * 0.5 + 0.5,
                                vp->size.x * 0.5, vp->size.y * 0.5 );
}

void pg_renderpass_scissor(struct pg_renderpass* pass, int use, struct pg_viewport* vp)
{
    if(!use) {
        pass->opts.flags &= ~PG_RENDERPASS_SCISSOR;
        return;
    }
    pass->opts.flags |= PG_RENDERPASS_SCISSOR;
    pass->opts.scissor = vec4( vp->pos.x * 0.5 + 0.5, vp->pos.y * 0.5 + 0.5,
                               vp->size.x * 0.5, vp->size.y * 0.5 );
}
