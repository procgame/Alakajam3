#include "procgl.h"

void pg_model_init(struct pg_model* model, int num_attribs,
                   const struct pg_model_attrib* attribs)
{
    model->vertex_stride = 0;
    uint32_t vertex_offset = 0;
    int i;
    for(i = 0; i < num_attribs; ++i) {
        model->attribs[i] = attribs[i];
        model->attribs[i].offset = vertex_offset;
        vertex_offset += attribs[i].size;
        model->vertex_stride += attribs[i].size;
    }
    model->num_attribs = num_attribs;
    model->vertex_data = NULL;
    model->vertex_len = 0;
    model->vertex_cap = 0;
    HTABLE_INIT(model->vao_table, 4);
    ARR_INIT(model->faces);
    glGenBuffers(1, &model->vbo);
    glGenBuffers(1, &model->ebo);
}

void pg_model_deinit(struct pg_model* model)
{
    free(model->vertex_data);
    glDeleteBuffers(1, &model->vbo);
    glDeleteBuffers(1, &model->ebo);
    GLuint* vao;
    HTABLE_ITER vao_iter;
    HTABLE_ITER_BEGIN(model->vao_table, vao_iter);
    while(!HTABLE_ITER_END(model->vao_table, vao_iter)) {
        HTABLE_ITER_NEXT_PTR(model->vao_table, vao_iter, vao);
        glDeleteVertexArrays(1, vao);
    }
    HTABLE_DEINIT(model->vao_table);
}

void pg_model_buffer(struct pg_model* model)
{
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, model->vbo);
    glBufferData(GL_ARRAY_BUFFER, model->vertex_len * model->vertex_stride,
                 model->vertex_data, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model->ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, model->faces.len * sizeof(struct pg_model_face),
                 model->faces.data, GL_STATIC_DRAW);
}

void pg_model_reserve(struct pg_model* model, int n)
{
    if(n <= model->vertex_cap) return;
    model->vertex_data = realloc(model->vertex_data, n * model->vertex_stride);
    memset(model->vertex_data + (model->vertex_cap * model->vertex_stride), 0,
           (n - model->vertex_cap) * model->vertex_stride);
    model->vertex_cap = n;
}

void pg_model_push_vertex(struct pg_model* model, void* vertex)
{
    if(model->vertex_len == model->vertex_cap - 1)
        pg_model_reserve(model, model->vertex_cap + 1);
    memcpy(model->vertex_data + (model->vertex_len * model->vertex_stride),
           vertex, model->vertex_stride);
    ++model->vertex_len;
}

void pg_model_set_vertex(struct pg_model* model, int i, void* vertex)
{
    if(i >= model->vertex_len) return;
    memcpy(model->vertex_data + (i * model->vertex_stride),
           vertex, model->vertex_stride);
}

void pg_model_spec(struct pg_model* model, struct pg_shader* shader)
{
    GLuint* check_vao;
    HTABLE_GET(model->vao_table, shader->name, check_vao);
    if(check_vao) return;
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, model->vbo);
    int matched_attribs = 0;
    int i, j;
    for(i = 0; i < model->num_attribs; ++i) {
        for(j = 0; j < shader->num_attribs; ++j) {
            struct pg_model_attrib* m_attr = &model->attribs[i];
            struct pg_shader_attrib* sh_attr = &shader->attribs[j];
            if(strncmp(m_attr->name, sh_attr->name, 32) == 0) {
                printf("GL_FLOAT: %d, GL_FLOAT_VEC2: %d, GL_FLOAT_VEC3: %d\n",
                        GL_FLOAT, GL_FLOAT_VEC2, GL_FLOAT_VEC3);
                printf("model type: %d, shader type: %d\n", m_attr->type, sh_attr->type);
                ++matched_attribs;
                if(m_attr->as_integer) {
                    glVertexAttribIPointer(sh_attr->index,
                                           m_attr->elements,
                                           m_attr->type,
                                           model->vertex_stride,
                                           (void*)m_attr->offset);
                } else {
                    glVertexAttribPointer(sh_attr->index,
                                          m_attr->elements,
                                          m_attr->type,
                                          m_attr->normalized,
                                          model->vertex_stride,
                                          (void*)m_attr->offset);
                }
                glEnableVertexAttribArray(sh_attr->index);
            }
        }
    }
    glBindVertexArray(0);
    HTABLE_SET(model->vao_table, shader->name, vao);
}

void pg_model_begin(struct pg_model* model, struct pg_shader* shader)
{
    GLuint* check_vao = NULL;
    HTABLE_GET(model->vao_table, shader->name, check_vao);
    if(!check_vao) {
        glBindVertexArray(0);
        printf("Model not specified for use with shader %s\n", shader->name);
        return;
    }
    glBindVertexArray(*check_vao);
}

struct obj_vertex { vec3 pos; vec2 tex; };

static const struct pg_model_attrib obj_attribs[2] = {
    { "v_position", .type = GL_FLOAT, .elements = 3, .size = sizeof(vec3) },
    { "v_tex_coord", .type = GL_FLOAT, .elements = 2, .size = sizeof(vec2) },
};

void pg_model_load_obj(struct pg_model* model, const char* filename)
{
    pg_model_init(model, 2, obj_attribs);
    ARR_T(vec3) v_pos = {};
    ARR_T(vec2) v_tex = {};
    FILE* obj_file = fopen(filename, "r");
    if(!obj_file) {
        printf("procgame ERROR:\n"
               "    Failed to load model: %s\n", filename);
        return;
    }
    /*  First pass: Just count the number of each thing there is    */
    int n_pos = 0, n_tex = 0, n_face = 0;
    char line[64];
    while(fgets(line, 64, obj_file)) {
        if(strncmp(line, "v ", 2) == 0) ++n_pos;
        else if(strncmp(line, "vt ", 3) == 0) ++n_tex;
        else if(strncmp(line, "f ", 2) == 0) ++n_face;
    }
    if(n_pos == 0 || n_tex == 0 || n_face == 0) {
        printf("procgame ERROR:\n"
               "    Model file missing vertex, texture, or face data: %s\n",
               filename);
        fclose(obj_file);
        return;
    }
    printf("Model file %s\n"
           "    Vertex positions: %d\n"
           "    Vertex UVs: %d\n"
           "    Face definitions: %d\n", filename, n_pos, n_tex, n_face);
    /*  Second pass: Actually load model data   */
    fseek(obj_file, 0, SEEK_SET);
    pg_model_reserve(model, n_face);
    ARR_RESERVE_CLEAR(v_pos, n_pos);
    ARR_RESERVE_CLEAR(v_tex, n_tex);
    while(fgets(line, 64, obj_file)) {
        if(strncmp(line, "v ", 2) == 0) {
            vec3 pos;
            sscanf(line, "v %f %f %f", &pos.x, &pos.y, &pos.z);
            ARR_PUSH(v_pos, pos);
        } else if(strncmp(line, "vt ", 3) == 0) {
            vec2 tex;
            sscanf(line, "vt %f %f", &tex.x, &tex.y);
            ARR_PUSH(v_tex, tex);
        } else if(strncmp(line, "f ", 2) == 0) {
            int pos[3], tex[3];
            sscanf(line, "f %d/%d %d/%d %d/%d",
                    &pos[0], &tex[0], &pos[1], &tex[1], &pos[2], &tex[2]);
            struct obj_vertex new_vert[3] = {
                { .pos = v_pos.data[pos[0] - 1],
                  .tex = v_tex.data[tex[0] - 1] },
                { .pos = v_pos.data[pos[1] - 1],
                  .tex = v_tex.data[tex[1] - 1] },
                { .pos = v_pos.data[pos[2] - 1],
                  .tex = v_tex.data[tex[2] - 1] } };
            pg_model_push_vertex(model, &new_vert[0]);
            pg_model_push_vertex(model, &new_vert[1]);
            pg_model_push_vertex(model, &new_vert[2]);
        }
    }
    printf("Model now has %zu verts\n", model->vertex_len);
    fclose(obj_file);
    pg_model_buffer(model);
    return;
}
