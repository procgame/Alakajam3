
#define VEC4_TO_UINT(v) (uint32_t)( \
            (((uint32_t)(LM_SATURATE((v).x) * 255) & 0xFF) << 24) | \
            (((uint32_t)(LM_SATURATE((v).y) * 255) & 0xFF) << 16) | \
            (((uint32_t)(LM_SATURATE((v).z) * 255) & 0xFF) << 8) | \
            (((uint32_t)(LM_SATURATE((v).w) * 255) & 0xFF) << 0) )

struct pg_texture_opts {
    vec4 border;
    GLint wrap_x, wrap_y;
    GLint filter_min, filter_mag;
    GLint swizzle[4];
};

#define PG_TEXTURE_OPTS(...) \
    (&(struct pg_texture_opts){ \
        .border = {}, \
        .wrap_x = GL_REPEAT, .wrap_y = GL_REPEAT, \
        .filter_min = GL_LINEAR, .filter_mag = GL_LINEAR, \
        .swizzle = { GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA }, \
        __VA_ARGS__ })

struct pg_texture {
    /*  Base memory allocation. Texture data first, then atlas data.    */
    void* data;
    /*  Texture atlas info; This is a pointer to memory immediately
        following the pixel data, with one atlas per texture layer */
    struct pg_texture_layer {
        /*  Layer size in UV and pixel coords   */
        vec2 layer_uv, layer_pix;
        /*  Atlas frame size in UV and pixel coords */
        vec2 frame_uv, frame_pix;
        /*  Number of atlas frames per row  */
        int pitch;
    }* layer_info;
    /*  Maximum dimensions among the tex layers managed by this pg_texture.
        May not be the actual size of an individual layer, but guaranteed
        to be >= that size. */
    int w, h;
    /*  Size of individual texture in the full allocation   */
    size_t stride;
    /*  Number of textures in the pixel data    */
    int layers;
    /*  Number of color channels    */
    int channels;
    /*  Base data type of a color channel in the texture    */
    enum pg_data_type type;
    size_t pixel_stride;
    /*  GL texture info     */
    enum { PG_TEXTURE_IMAGE, PG_TEXTURE_DEPTH, PG_TEXTURE_DEPTH_STENCIL } usage;
    GLuint handle;
    struct pg_texture_opts opts;
};

typedef struct {
    vec4 frame;
    int layer;
} pg_tex_frame_t;

/*  Load a texture from a file, to a 4-channel 32bpp RGBA texture   */
void pg_texture_init_from_file(struct pg_texture* tex, const char* file,
                               struct pg_texture_opts* opts);
void pg_texture_from_files(struct pg_texture* tex, int num_files,
                           const char** files, struct pg_texture_opts* opts);
/*  Initialize an empty texture */
void pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int w, int h, int layers, struct pg_texture_opts* opts);
void pg_texture_init_depth(struct pg_texture* tex, int w, int h, int layers,
                           struct pg_texture_opts* opts);
void pg_texture_init_depth_stencil(struct pg_texture* tex, int w, int h, int layers,
                                   struct pg_texture_opts* opts);
void pg_texture_init_buffer(struct pg_texture* tex, enum pg_data_type type,
                            uint32_t len);
/*  Free a texture initialized with above functions */
void pg_texture_deinit(struct pg_texture* tex);

/*  Bind texture to texture unit GL_TEXTURE0+slot   */
void pg_texture_bind(struct pg_texture* tex, int slot);
/*  Upload texture data to the GPU  */
void pg_texture_buffer(struct pg_texture* tex);
/*  Set GL texture parameters (uploaded with next call to pg_texture_buffer) */
void pg_texture_options(struct pg_texture* tex, struct pg_texture_opts* opts);
void pg_texture_buffer_opts(struct pg_texture_opts* opts);

/*  Sets the dimensions of a texture atlas grid for a texture layer*/
void pg_texture_set_atlas(struct pg_texture* tex, int layer, int w, int h);
const struct pg_texture_layer* pg_texture_get_atlas(struct pg_texture* tex, int layer);
/*  Retrieves a vec4 texture transform for a given frame (x, y, w, h)   */
pg_tex_frame_t pg_texture_frame(struct pg_texture* tex, int layer, int frame);
/*  Flip a texture transform on given axes  */
pg_tex_frame_t pg_texture_frame_flip(pg_tex_frame_t in, int x, int y);
/*  Intuitively transform a texture transform: (x,y) + offset, (w,h) * scale */
pg_tex_frame_t pg_texture_frame_tx(pg_tex_frame_t in,
                                   vec2 const scale, vec2 const offset);

void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int layer, int x, int y, int w, int h);

/********************/
/*  Rendertexture   */
/********************/

struct pg_renderbuffer {
    GLuint fbo;
    struct pg_texture* outputs[8];
    int layer[8];
    GLenum attachments[8];
    GLenum drawbufs[8];
    int dirty, w, h;
};

struct pg_rendertarget {
    struct pg_renderbuffer* buf[2];
};

void pg_renderbuffer_init(struct pg_renderbuffer* buffer);
void pg_renderbuffer_deinit(struct pg_renderbuffer* buffer);
void pg_renderbuffer_attach(struct pg_renderbuffer* buffer,
                             struct pg_texture* tex, int layer,
                             int idx, GLenum attachment);
void pg_renderbuffer_build(struct pg_renderbuffer* buffer);
void pg_renderbuffer_dst(struct pg_renderbuffer* buffer);

void pg_rendertarget_init(struct pg_rendertarget* target,
                          struct pg_renderbuffer* b0, struct pg_renderbuffer* b1);
void pg_rendertarget_dst(struct pg_rendertarget* target);
void pg_rendertarget_swap(struct pg_rendertarget* target);
vec2 pg_rendertarget_get_resolution(struct pg_rendertarget* target);
void pg_rendertarget_clear(struct pg_rendertarget* target);

/********************/
/*  Buffer texture  */
/********************/

struct pg_buffertex {
    void* data;
    enum pg_data_type type;
    size_t stride;
    GLuint tex_handle;
    GLuint buf_handle;
    /*  Buffer capacity */
    uint32_t data_cap;
    /*  Index for the next write */
    uint32_t write_idx;
    uint32_t group_size;
    uint32_t group_stride;
};

void pg_buffertex_init(struct pg_buffertex* buf, enum pg_data_type type,
                       uint32_t group_size, uint32_t group_count);
void pg_buffertex_deinit(struct pg_buffertex* buf);
/*  Move write index to given value */
void pg_buffertex_seek(struct pg_buffertex* buf, int idx);
/*  Write formatted data (group_size per entry) */
void pg_buffertex_push(struct pg_buffertex* buf, void* data, int count);
/*  Upload data to GPU  */
void pg_buffertex_buffer(struct pg_buffertex* buf);
void pg_buffertex_sub(struct pg_buffertex* buf, uint32_t idx, uint32_t len);






