/*  Dynamic array macro API
    Macro                           Evaluates to...
    ARR_PUSH, ARR_INSERT,
    ARR_SWAPINSERT, ARR_CONCAT  ->  ARR_SUCCESS or ARR_FAIL
    ARR_TRUNCATE, ARR_SPLICE,
    ARR_SWAPSPLICE              ->  New length of array
    ARR_POP                     ->  Popped element
    ARR_FOREACH...              ->  For loop header
    ARR_INIT, ARR_DEINIT,
    ARR_SORT, ARR_SORT_R        ->  (Statements)  */

#ifndef ARR_INCLUDED
#define ARR_INCLUDED

#include <stdlib.h>
#include <string.h>

#define ARR_FAIL    0
#define ARR_SUCCEED 1

#define ARR_T(...) struct { size_t cap; size_t len; size_t static_cap; __VA_ARGS__ *data; }
typedef ARR_T(int)              arr_int_t;
typedef ARR_T(unsigned)         arr_uint_t;
typedef ARR_T(double)           arr_double_t;
typedef ARR_T(char)             arr_char_t;
typedef ARR_T(unsigned char)    arr_uchar_t;
typedef ARR_T(char*)            arr_str_t;

#define SARR_T(N, ...) struct { size_t cap; size_t len; size_t static_cap; __VA_ARGS__ *data, static_data[N]; }

#define ARR_FUNCTION_IFACE_DECL(F, P, A, T) \
F void   P##_init(A*); \
F void   P##_deinit(A*); \
F int    P##_reserve(A*, size_t); \
F int    P##_reserve_clear(A*, size_t); \
F int    P##_push(A*, T*); \
F T      P##_pop(A*); \
F int    P##_insert(A*, size_t, T*); \
F int    P##_swapinsert(A*, size_t, T*); \
F int    P##_concat(A*, A*); \
F size_t P##_truncate(A*, size_t); \
F size_t P##_truncate_clear(A*, size_t); \
F size_t P##_splice(A*, size_t, size_t); \
F size_t P##_swapsplice(A*, size_t, size_t); \
F void   P##_sort(A*, int (*)(const void*,const void*)); \

#define ARR_FUNCTION_IFACE_DEF(F, P, A, T) \
F void P##_init(A* arr) { ARR_INIT(*arr); } \
F void P##_deinit(A* arr) { ARR_DEINIT(*arr); } \
F int P##_reserve(A* arr, size_t count) \
    { return ARR_RESERVE(*arr, count); } \
F int P##_reserve_clear(A* arr, size_t count) \
    { return ARR_RESERVE_CLEAR(*arr, count); } \
F int P##_push(A* arr, T* val) \
    { return ARR_PUSH(*arr, *val); } \
F T P##_pop(A* arr) \
    { return ARR_POP(*arr); } \
F int P##_insert(A* arr, size_t idx, T* val) \
    { return ARR_INSERT(*arr, idx, *val); } \
F int P##_swapinsert(A* arr, size_t idx, T* val) \
    { return ARR_SWAPINSERT(*arr, idx, *val); } \
F int P##_concat(A* arr_a, A* arr_b) \
    { return ARR_CONCAT(*arr_a, *arr_b); } \
F size_t P##_truncate(A* arr, size_t len) \
    { return ARR_TRUNCATE(*arr, len); } \
F size_t P##_truncate_clear(A* arr, size_t len) \
    { return ARR_TRUNCATE_CLEAR(*arr, len); } \
F size_t P##_splice(A* arr, size_t idx, size_t count) \
    { return ARR_SPLICE(*arr, idx, count); } \
F size_t P##_swapsplice(A* arr, size_t idx, size_t count) \
    { return ARR_SWAPSPLICE(*arr, idx, count); } \
F void P##_sort(A* arr, int (*cmp_fn)(const void*,const void*)) \
    { ARR_SORT(*arr, cmp_fn); } \

/*  Bookkeeping */
#define ARR_INIT(arr) \
    do { (arr).len = 0; (arr).cap = 0; (arr).data = NULL; (arr).static_cap = 0; } while(0)
#define SARR_INIT(arr) \
    do { \
        (arr).data = (arr).static_data; \
        memset((arr).data, 0, sizeof((arr).static_data)); \
        (arr).len = 0; \
        (arr).cap = sizeof((arr).static_data) / sizeof(*(arr).data); \
        (arr).static_cap = (arr).cap; \
    } while(0)

#define ARR_DEINIT(arr) \
    do { \
        if((arr).data && !(arr).static_cap) free((arr).data); \
        (arr).len = 0; (arr).cap = 0; (arr).data = NULL; \
    } while(0)

#define ARR_RESERVE(arr, count) \
    ( (arr).static_cap ? ((arr).static_cap >= count ? ARR_SUCCEED : ARR_FAIL) : \
      (((arr).cap > (count)) ? ARR_SUCCEED :\
        ((arr).data = realloc((arr).data, (count) * sizeof(*(arr).data)), \
           (arr).data ? ((arr).cap = (count), ARR_SUCCEED) : ARR_FAIL)) )

#define ARR_RESERVE_CLEAR(arr, count) \
    ( (arr).static_cap ? ((arr).static_cap >= count ? ARR_SUCCEED : ARR_FAIL) : \
      ((arr).cap > (count)) ? ARR_SUCCEED : \
        ((arr).data = realloc((arr).data, (count) * sizeof(*(arr).data)), \
         (!(arr).data) ? ARR_FAIL : \
            ((arr).cap = (count), ARR_TRUNCATE_CLEAR((arr), (arr).len), \
             ARR_SUCCEED)) )

/*  Adding elements */
#define ARR_PUSH(arr, ...) \
    ( !ARR_RESERVE((arr), (arr).len + 1) ? ARR_FAIL \
        : ((arr).data[(arr).len++] = (__VA_ARGS__), ARR_SUCCEED) )

#define ARR_INSERT(arr, idx, ...) \
    ( !ARR_RESERVE((arr), (arr).len + 1) ? ARR_FAIL \
        : (memmove((arr).data + (idx) + 1, (arr).data + (idx), \
                   (((arr).len++) - (idx)) * sizeof(*(arr).data)), \
            ((arr).data[idx] = (__VA_ARGS__)), ARR_SUCCEED) )

#define ARR_SWAPINSERT(arr, idx, ...) \
    ( !ARR_RESERVE((arr), (arr).len + 1) ? ARR_FAIL\
        : ((arr).data[(arr).len++] = (arr).data[idx], \
           (arr).data[idx] = (__VA_ARGS__), ARR_SUCCEED) )

#define ARR_CONCAT(arr1, arr2) \
    ( !ARR_RESERVE((arr1), (arr1).len + (arr2).len) ? ARR_FAIL \
        : (memcpy((arr1).data + (arr1).len, \
                  (arr2).data, \
                  (arr2).len * sizeof(*(arr2).data)), \
            (arr1).len += (arr2).len, ARR_SUCCEED) )

/*  Removing elements   */
#define ARR_POP(arr) \
    ( (arr).data[--((arr).len)] )

#define ARR_TRUNCATE(arr, count) \
    ( ((arr).len > (count)) ? (arr).len = (count) : (arr).len )

#define ARR_TRUNCATE_CLEAR(arr, count) \
    ( ((arr).cap > (count)) \
        ? (memset((arr).data + (count), 0, sizeof(*(arr).data) * ((arr).cap - (count))), \
            (arr).len = (count)) \
        : (arr).len )

#define ARR_SPLICE(arr, idx, count) \
    ( memmove((arr).data + (idx),   (arr).data + (idx) + (count),\
             ((arr).len - (idx) - (count)) * sizeof(*(arr).data)), \
        (arr).len -= (count) )

#define ARR_SWAPSPLICE(arr, idx, count) \
    ( memmove((arr).data + (idx), (arr).data + ((arr).len - (count)), \
              (count) * sizeof(*(arr).data)), \
        (arr).len -= (count) ) 

/*  Traversing elements */
#define ARR_SORT(arr, fn) \
    qsort((arr).data, (arr).len, sizeof(*(arr).data), fn)

#define ARR_FOREACH(arr, iter, idx) \
    for((idx) = 0; \
        ((idx) < (arr).len) && ((iter) = (arr).data[idx], 1); \
        ++(idx))

#define ARR_FOREACH_PTR(arr, iter, idx) \
    for((idx) = 0; \
        ((idx) < (arr).len) && ((iter) = &((arr).data[idx]), 1); \
        ++(idx))

#define ARR_FOREACH_REV(arr, iter, idx) \
    for((idx) = (arr).len - 1; \
        ((idx) >= 0) && ((iter) = (arr).data[idx], 1); \
        --(idx))

#define ARR_FOREACH_PTR_REV(arr, iter, idx) \
    for((idx) = (arr).len - 1; \
        ((idx) >= 0) && ((iter) = &((arr).data[idx]), 1); \
        --(idx))
#define ARR_FOREACH_REV_PTR ARR_FOREACH_PTR_REV

#endif
