#include "procgl.h"

static int drawfunc_box(const struct pg_type* unis, const struct pg_shader* shader,
                        const mat4* mats, const GLint* idx)
{
    mat4 mvp = mat4_mul(mats[PG_PROJECTIONVIEW_MATRIX], unis[0].m);
    glUniformMatrix4fv(idx[0], 1, GL_FALSE, mvp.v);
    glUniform1i(idx[1], unis[1].i[0] * 4);
    glDrawArrays(GL_TRIANGLES, 0, 36 * unis[1].i[1]);
    return 2;
}

void pg_boxbatch_init(struct pg_boxbatch* batch, int size, struct pg_texture* tex)
{
    pg_buffertex_init(&batch->buf, PG_VEC4, 4, size);
    batch->tex = tex;
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}

void pg_boxbatch_deinit(struct pg_boxbatch* batch)
{
    pg_buffertex_deinit(&batch->buf);
}

/*  Build a renderpass to use the batched data  */
void pg_boxbatch_init_pass(struct pg_boxbatch* batch, struct pg_renderpass* pass,
                          struct pg_rendertarget* target, vec2 tex_size)
{
    pg_renderpass_init(pass, "boxbatch", PG_RENDERPASS_OPTS(
        PG_RENDERPASS_DEPTH_TEST | PG_RENDERPASS_DEPTH_WRITE | PG_RENDERPASS_CULL_FACES,
        .depth_func = GL_LEQUAL));
    pg_renderpass_target(pass, target);
    pg_renderpass_uniform(pass, "tex_size", PG_VEC2, &PG_TYPE_FLOAT(VEC_XY(tex_size)));
    pg_renderpass_drawformat(pass, drawfunc_box, 2,
                            "pg_matrix_mvp", "sprite_offset");
    pg_renderpass_buftexture(pass, 0, &batch->buf);
    pg_renderpass_texture(pass, 1, batch->tex, NULL);
}

#define PACK_VEC3(v) (uint32_t)( \
    (((uint32_t)(LM_SATURATE((v).x) * 255) & 0xFF) << 24) | \
    (((uint32_t)(LM_SATURATE((v).y) * 255) & 0xFF) << 16) | \
    (((uint32_t)(LM_SATURATE((v).z) * 255) & 0xFF) << 8) )

#define PACK_2X_VEC2(v0, v1) (uint32_t)( \
    (((uint32_t)(LM_SATURATE((v0).x) * 255) & 0xFF) << 24) | \
    (((uint32_t)(LM_SATURATE((v0).y) * 255) & 0xFF) << 16) | \
    (((uint32_t)(LM_SATURATE((v1).x) * 255) & 0xFF) << 8) | \
    (((uint32_t)(LM_SATURATE((v1).y) * 255) & 0xFF) << 0) )

#define PACK_QUAT(q) (uint64_t)( \
    (((uint64_t)((LM_SATURATE((q).x * 0.5 + 0.5)) * 65535) & 0xFFFF) << 48L) | \
    (((uint64_t)((LM_SATURATE((q).y * 0.5 + 0.5)) * 65535) & 0xFFFF) << 32L) | \
    (((uint64_t)((LM_SATURATE((q).z * 0.5 + 0.5)) * 65535) & 0xFFFF) << 16L) | \
    (((uint64_t)((LM_SATURATE((q).w * 0.5 + 0.5)) * 65535) & 0xFFFF) << 0) )

#define PACK_UBVEC4(v) (uint32_t)( \
    ((uint32_t)((v).x) << 24) | ((uint32_t)((v).y) << 16) | \
    ((uint32_t)((v).z) << 8) | ((uint32_t)((v).w) << 0) )

//#define PACK_UBVEC4(v)  (*(uint32_t*)(&v))

/*  Add a single box to the current batch   */
void pg_boxbatch_add_box(struct pg_boxbatch* batch, struct pg_ezbox* box)
{
    struct pg_ezbox_packed pack = {
        .pos = box->pos,
        .iso_scale = box->iso_scale,
        .aniso_scale_and_tex_layer = PACK_VEC3(box->aniso_scale) | (box->tex_layer & 0xFF),
        .endcap_scale = PACK_2X_VEC2(box->cap_scale_bottom, box->cap_scale_top),
        .orientation = PACK_QUAT(box->orientation),
        .tex_left = PACK_UBVEC4(box->tex_left),
        .tex_right = PACK_UBVEC4(box->tex_right),
        .tex_front = PACK_UBVEC4(box->tex_front),
        .tex_back = PACK_UBVEC4(box->tex_back),
        .tex_top = PACK_UBVEC4(box->tex_top),
        .tex_bottom = PACK_UBVEC4(box->tex_bottom),
        .color_mod = PACK_UBVEC4(box->color_mod),
        .color_add = PACK_UBVEC4(box->color_add) };
    pg_buffertex_push(&batch->buf, &pack, 1);
    batch->cur_idx += 1;
}

/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_boxbatch_draw(struct pg_boxbatch* batch, struct pg_renderpass* pass)
{
    if(batch->cur_idx == 0) return;
    struct pg_type unis[2] = {
        PG_TYPE_MATRIX(batch->cur_transform),
        PG_TYPE_INT(batch->cur_start, batch->cur_idx),
    };
    pg_renderpass_add_draw(pass, 2, unis);
}

/*  Begin a new batch group with a given model matrix   */
void pg_boxbatch_next(struct pg_boxbatch* batch, mat4* transform)
{
    batch->cur_start += batch->cur_idx;
    batch->cur_idx = 0;
    batch->cur_transform = *transform;
}

/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_boxbatch_buffer(struct pg_boxbatch* batch)
{
    pg_buffertex_sub(&batch->buf, 0, batch->cur_start + batch->cur_idx);
}

/*  Restart batching    */
void pg_boxbatch_reset(struct pg_boxbatch* batch)
{
    pg_buffertex_seek(&batch->buf, 0);
    batch->cur_start = 0;
    batch->cur_idx = 0;
    batch->cur_transform = mat4_identity();
}

/************************************/
/*  BOX MODEL                       */
/************************************/

static inline ubvec4 ubvec4_offset(ubvec4 in, ivec4 offset)
{
    ivec4 tmp = ivec4_vclamp(ivec4_add(ivec4(VEC_XYZW(in)), offset), 0, 255);
    return ubvec4(VEC_XYZW(tmp));
}

void pg_boxbatch_add_model(struct pg_boxbatch* batch,
                           struct pg_ezbox_model_draw* draw)
{
    struct pg_ezbox_model* model = draw->model;
    struct pg_ezbox_rig* rig = draw->rig;
    if(!model) return;
    ivec4 tex_offset = ivec4(VEC_XYXY(draw->tex_offset));
    vec4 color_mod = vec4_scale(vec4(VEC_XYZW(draw->color_mod)), 1.0f / 255.0f);
    int i;
    for(i = 0; i < model->num_parts; ++i) {
        struct pg_ezbox box = model->parts[i];
        int b_idx = model->bone_idx[i];
        if(b_idx >= 0 && draw->pose[0] && draw->pose[1]) {
            quat bone_rot = quat_lerp(rig->pose_rot[draw->pose[0]][b_idx],
                                      rig->pose_rot[draw->pose[1]][b_idx],
                                      draw->pose_lerp);
            vec3 bone_move = vec3_lerp(rig->pose_move[draw->pose[0]][b_idx],
                                       rig->pose_move[draw->pose[1]][b_idx],
                                       draw->pose_lerp);
            bone_rot = quat_norm(bone_rot);
            /*  Rotate box position around bone origin  */
            vec3 box_from_bone = vec3_sub(box.pos, rig->bone_origin[b_idx]);
            box_from_bone = quat_mul_vec3(bone_rot, box_from_bone);
            box.pos = vec3_add(rig->bone_origin[b_idx], box_from_bone);
            box.pos = vec3_add(box.pos, bone_move);
            box.orientation = quat_mul(bone_rot, box.orientation);
        }
        box.orientation = quat_mul(draw->orientation, box.orientation);
        box.iso_scale *= draw->scale;
        box.pos = quat_mul_vec3(draw->orientation, box.pos);
        box.pos = vec3_add(vec3_scale(box.pos, draw->scale), draw->pos);
        box.tex_left = ubvec4_offset(box.tex_left, tex_offset);
        box.tex_right = ubvec4_offset(box.tex_right, tex_offset);
        box.tex_front = ubvec4_offset(box.tex_front, tex_offset);
        box.tex_back = ubvec4_offset(box.tex_back, tex_offset);
        box.tex_top = ubvec4_offset(box.tex_top, tex_offset);
        box.tex_bottom = ubvec4_offset(box.tex_bottom, tex_offset);
        vec4 b_color_mod = vec4_saturate(vec4_mul(vec4(VEC_XYZW(box.color_mod)), color_mod));
        b_color_mod = vec4_scale(b_color_mod, 255);
        box.color_mod = ubvec4(VEC_XYZW(b_color_mod));
        box.color_add = ubvec4_offset(box.color_add, ivec4(VEC_XYZW(draw->color_add)));
        pg_boxbatch_add_box(batch, &box);
    }
}

#define CHECK_FSCANF(N) \
    if(vals_read != N) { printf("Rigging file %s invalid\n", filename); return; }
void pg_ezbox_rig_read_from_file(struct pg_ezbox_rig* rig, char* filename)
{
    *rig = (struct pg_ezbox_rig){};
    FILE* f = fopen(filename, "r");
    if(!f) return;
    int line = 0;
    int vals_read = 0;
    int num_bones = 0;
    int num_poses = 0;
    vals_read = fscanf(f, " rig: %d : %d", &num_bones, &num_poses);
    CHECK_FSCANF(2);
    rig->num_bones = num_bones;
    rig->num_poses = num_poses;
    int b, p;
    /*  First read the resting pose */
    for(b = 0; b < num_bones; ++b) {
        int bone_idx;
        char bone_name[32];
        vec3 pos, end;
        vals_read = fscanf(f, "  bone: %d : %31s", &bone_idx, bone_name);
        CHECK_FSCANF(2);
        vals_read = fscanf(f, "    pos: %f : %f : %f", &pos.x, &pos.y, &pos.z);
        CHECK_FSCANF(3);
        vals_read = fscanf(f, "    end: %f : %f : %f", &end.x, &end.y, &end.z);
        CHECK_FSCANF(3);
        printf("Read bone: %s\n", bone_name);
        strncpy(rig->bone_name[b], bone_name, 32);
        rig->bone_origin[b] = pos;
    }
    for(p = 0; p < num_poses; ++p) {
        char pose_name[32];
        vals_read = fscanf(f, " pose: %*d : %31s", pose_name);
        CHECK_FSCANF(1);
        printf("Reading pose: %s\n", pose_name);
        strncpy(rig->pose_name[p], pose_name, 32);
        for(b = 0; b < num_bones; ++b) {
            quat rot;
            vec3 pos;
            vals_read = fscanf(f, "  bone: %*d : %*s");
            CHECK_FSCANF(0);
            vals_read = fscanf(f, "    pos: %f : %f : %f", &pos.x, &pos.y, &pos.z);
            CHECK_FSCANF(3);
            vals_read = fscanf(f, "    quat: %f : %f : %f : %f", &rot.x, &rot.y, &rot.z, &rot.w);
            CHECK_FSCANF(4);
            rig->pose_rot[p][b] = rot;
            rig->pose_move[p][b] = pos;
        }
    }
    fclose(f);
}

void pg_ezbox_model_link_rig(struct pg_ezbox_model* model, struct pg_ezbox_rig* rig)
{
    int part_i, bone_i;
    for(part_i = 0; part_i < model->num_parts; ++part_i) {
        if(model->use_bone[part_i][0] == '\0') {
            model->bone_idx[part_i] = -1;
            continue;
        }
        for(bone_i = 0; bone_i < rig->num_bones; ++bone_i) {
            if(strncmp(model->use_bone[part_i], rig->bone_name[bone_i], 31) == 0) {
                break;
            }
        }
        if(bone_i == rig->num_bones) model->bone_idx[part_i] = -1;
        else model->bone_idx[part_i] = bone_i;
    }
}
