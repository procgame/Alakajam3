#include "procgl.h"

/************************************************/
/*  Internal UI structures                      */
/************************************************/

struct pg_ui_property {
    vec4 value;
    enum { PG_UI_NO_ANIM, PG_UI_KEYFRAME_ANIM, PG_UI_SIMPLE_ANIM } anim_type;
    union {
        struct pg_anim_property kf_anim;
        struct pg_simple_anim simple_anim;
    };
};

struct pg_ui_element {
    /*  Static fields   */
    enum pg_ui_draw_mode draw;
    int enable_clip;
    int img_layer;
    struct pg_text_formatter text_formatter;
    struct pg_text_form text_form;
    vec2 text_anchor;
    /*  Event callbacks */
    enum pg_ui_action_item action_item;
    pg_ui_callback_t callbacks[PG_UI_ELEM_CALLBACKS];
    /*  Drag and drop   */
    uint64_t drop_type, accept_drops;
    /*  Animate-able fields */
    struct pg_ui_property properties[PG_UI_ELEM_PROPERTIES];
    /*  Running information    */
    int mouse_over;
};

struct pg_ui_group {
    /*  Animate-able properties */
    struct pg_ui_property properties[6];
    /*  Clipping enabled    */
    int enable_clip;
    /*  Children hash table */
    pg_ui_table_t children;
    /*  Children stored in depth order, bottom to top   */
    ARR_T(pg_ui_t) children_arr;
    /*  Indicates if new children have been added since last sort   */
    int children_added;
    /*  Drag and drop   */
    uint64_t accept_drops;
    uint64_t queued_drop_type;
    pg_ui_t queued_drop;
    int tried_drop;
    vec2 drop_pos;
};

struct pg_ui_content {
    enum { PG_UI_FREE, PG_UI_UNINITIALIZED, PG_UI_GROUP, PG_UI_ELEMENT } type;
    char name[32];
    pg_ui_t parent;
    /*  User variables hash table   */
    pg_type_table_t vars;
    int disabled;
    /*  Mouse pos relative to this content  */
    vec2 mouse_pos;
    /*  Depth layer */
    int layer;
    /*  UI type */
    union {
        struct pg_ui_group grp;
        struct pg_ui_element elem;
    };
};

/************************************************/
/*  Memory management                           */
/************************************************/

static pg_ui_t ui_alloc(struct pg_ui_context* ctx, int n,
                                  struct pg_ui_content** ptr)
{
    int run = 1;
    int i = 0;
    int alloc = 0;
    for(i = 0; i < ctx->content_pool.cap; ++i) {
        if(ctx->content_pool.data[i].type != PG_UI_FREE) continue;
        run = 1;
        while(run < n && run + i < ctx->content_pool.cap
        && ctx->content_pool.data[run + i].type == PG_UI_FREE) ++run;
        if(run < n) i += run - 1;
        else break;
    }
    alloc = i;
    if(alloc + n >= ctx->content_pool.cap)
        ARR_RESERVE(ctx->content_pool, alloc + n);
    for(i = 0; i < n; ++i) {
        struct pg_ui_content* new_cont = &ctx->content_pool.data[alloc + i];
        *new_cont = (struct pg_ui_content){ .type = PG_UI_UNINITIALIZED };
        HTABLE_INIT(new_cont->vars, 4);
    }
    if(ptr) *ptr = ctx->content_pool.data + alloc;
    return (pg_ui_t)alloc;
}

static void ui_free(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    if(ui_ref < 0 || ui_ref >= ctx->content_pool.cap) return;
    struct pg_ui_content* ui_cont = &ctx->content_pool.data[ui_ref];
    if(ui_cont->type == PG_UI_GROUP) {
        int i;
        pg_ui_t child;
        ARR_FOREACH(ui_cont->grp.children_arr, child, i) {
            ui_free(ctx, child);
        }
        ARR_DEINIT(ui_cont->grp.children_arr);
        HTABLE_DEINIT(ui_cont->grp.children);
    } else if(ui_cont->type == PG_UI_ELEMENT) {
        pg_text_form_deinit(&ui_cont->elem.text_form);
    }
    HTABLE_DEINIT(ui_cont->vars);
    ctx->content_pool.data[ui_ref] = (struct pg_ui_content){};
}

static struct pg_ui_content* ui_dereference(struct pg_ui_context* ctx,
                                     pg_ui_t ui_ref)
{
    if(ui_ref < 0 || ui_ref >= ctx->content_pool.cap) return NULL;
    return &ctx->content_pool.data[ui_ref];
}

/************************************************/
/* UI Context functions                         */
/************************************************/

#define V2(V)   vec4(VEC_XY(V))

static void pg_ui_group_init(struct pg_ui_group* grp, struct pg_ui_properties* props)
{
    *grp = (struct pg_ui_group){
        .properties = {
            [PG_UI_POS] =           { .value = V2(props->pos) },
            [PG_UI_PIXEL_POS] =     { .value = V2(props->pixel_pos) },
            [PG_UI_SCALE] =         { .value = V2(props->scale) },
            [PG_UI_ROTATION] =      { .value = vec4(props->rotation) },
            [PG_UI_CLIP] =          { .value = props->clip },
            [PG_UI_PIXEL_CLIP] =    { .value = props->pixel_clip },
        },
        .enable_clip = props->enable_clip,
    };
    HTABLE_INIT(grp->children, 8);
    ARR_INIT(grp->children_arr);
}

static void pg_ui_elem_init(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                            struct pg_ui_properties* props)
{
    cont->elem = (struct pg_ui_element) {
        .properties = {
            [PG_UI_PIXEL_POS] =         { .value = V2(props->pixel_pos) },
            [PG_UI_POS] =               { .value = V2(props->pos) },
            [PG_UI_SCALE] =             { .value = V2(props->scale) },
            [PG_UI_PIXEL_IMG_POS] =     { .value = V2(props->pixel_img_pos) },
            [PG_UI_IMG_POS] =           { .value = V2(props->img_pos) },
            [PG_UI_PIXEL_IMG_SCALE] =   { .value = V2(props->pixel_img_scale) },
            [PG_UI_IMG_SCALE] =         { .value = V2(props->img_scale) },
            [PG_UI_IMG_COLOR_MOD] =     { .value = props->img_color_mod },
            [PG_UI_IMG_COLOR_ADD] =     { .value = props->img_color_add },
            [PG_UI_IMG_FRAME] =         { .value = props->img_frame },
            [PG_UI_PIXEL_TEXT_POS] =    { .value = V2(props->pixel_text_pos) },
            [PG_UI_TEXT_POS] =          { .value = V2(props->text_pos) },
            [PG_UI_TEXT_SCALE] =        { .value = V2(props->text_scale) },
            [PG_UI_TEXT_COLOR] =        { .value = props->text_color },
            [PG_UI_PIXEL_ACTION_AREA] = { .value = props->pixel_action_area },
            [PG_UI_ACTION_AREA] =       { .value = props->action_area },
            [PG_UI_ROTATION] =          { .value = vec4(props->rotation) },
            [PG_UI_IMG_ROTATION] =      { .value = vec4(props->img_rotation) },
            [PG_UI_TEXT_ROTATION] =     { .value = vec4(props->text_rotation) } },
        .action_item = props->action_item,
        .draw = props->draw,
        .img_layer = props->img_layer,
        .drop_type = props->drop_type,
        .accept_drops = props->accept_drops,
        .callbacks = {
            [PG_UI_CLICK] = props->cb_click,
            [PG_UI_HOLD] = props->cb_hold,
            [PG_UI_RELEASE] = props->cb_release,
            [PG_UI_SCROLL] = props->cb_scroll,
            [PG_UI_ENTER] = props->cb_enter,
            [PG_UI_LEAVE] = props->cb_leave,
            [PG_UI_UPDATE] = props->cb_update,
            [PG_UI_DRAG_DROP] = props->cb_drag_drop, },
    };
    cont->layer = props->layer;
    cont->type = PG_UI_ELEMENT;
    cont->disabled = !props->enabled;
    /*  Text stuff  */
    cont->elem.text_anchor = props->text_anchor;
    cont->elem.text_formatter = props->text_formatter ?
        *props->text_formatter : ctx->default_text_formatter;
    if(props->text) {
        int len;
        if(props->text_len <= 0) len = wcslen(props->text);
        else len = wcsnlen(props->text, props->text_len);
        pg_text_form_init_alloc(&cont->elem.text_form, len);
        pg_text_format(&cont->elem.text_form, &cont->elem.text_formatter,
                       props->text, len);
    } else if(props->text_c) {
        int len;
        if(props->text_len <= 0) len = strlen(props->text_c);
        else len = strnlen(props->text_c, props->text_len);
        pg_text_form_init_alloc(&cont->elem.text_form, len);
        pg_text_format_c(&cont->elem.text_form, &cont->elem.text_formatter,
                       props->text_c, len);
    } else {
        pg_text_form_init_alloc(&cont->elem.text_form, 0);
    }
}

#undef V2

void pg_ui_init(struct pg_ui_context* ctx, struct pg_rendertarget* target,
                struct pg_texture* tex, struct pg_text_formatter* formatter)
{
    /*  Drawing-related setup   */
    ctx->tex = tex;
    ctx->default_text_formatter = *formatter;
    /*  Batch pass  */
    pg_quadbatch_init(&ctx->sprite_batch, 4096, tex);
    pg_quadbatch_init_pass(&ctx->sprite_batch, &ctx->rendpass, target);
    pg_renderpass_texture(&ctx->rendpass, 1, tex, PG_TEXTURE_OPTS(
        .filter_min = GL_LINEAR, .filter_mag = GL_LINEAR,
        .wrap_x = GL_CLAMP_TO_EDGE, .wrap_y = GL_CLAMP_TO_EDGE ));
    pg_renderpass_uniform(&ctx->rendpass, "alpha_cutoff", PG_FLOAT,
        &PG_TYPE_FLOAT(-1));
    /*  Rendertarget    */
    pg_ui_target(ctx, target);
    /*  Memory-related setup    */
    ARR_INIT(ctx->content_pool);
    struct pg_ui_content* root_cont;
    ctx->root = ui_alloc(ctx, 1, &root_cont);
    pg_ui_group_init(&root_cont->grp, PG_UI_PROPERTIES());
    root_cont->grp.queued_drop = -1;
    strncpy(root_cont->name, "UI", 32);
    root_cont->parent = -1;
    root_cont->type = PG_UI_GROUP;
}

void pg_ui_deinit(struct pg_ui_context* ctx)
{
    pg_renderpass_deinit(&ctx->rendpass);
    pg_quadbatch_deinit(&ctx->sprite_batch);
    int i;
    for(i = 0; i < ctx->content_pool.cap; ++i) ui_free(ctx, i);
    ARR_DEINIT(ctx->content_pool);
}

void pg_ui_target(struct pg_ui_context* ctx, struct pg_rendertarget* target)
{
    pg_renderpass_target(&ctx->rendpass, target);
    vec2 target_res = pg_rendertarget_get_resolution(target);
    pg_ui_resolution(ctx, vec2(target_res.x, target_res.y));
}

void pg_ui_resolution(struct pg_ui_context* ctx, vec2 res)
{
    vec2 half = vec2_scale(res, 0.5);
    ctx->screen_res = half;
    ctx->rendpass.mats[PG_PROJECTIONVIEW_MATRIX] = mat4_ortho(-half.x, half.x, half.y, -half.y, -1, 1);
    ctx->screen_mat = mat4_ortho(-half.x, half.x, half.y, -half.y, -1, 1);
}

/************************************************/
/*  Animation                                   */
/************************************************/

void pg_ui_cancel_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                       enum pg_ui_property_id prop_id)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont) return;
    struct pg_ui_property* prop;
    if(cont->type == PG_UI_GROUP && prop_id <= PG_UI_ROTATION) {
        prop = &cont->grp.properties[prop_id];
    } else if(cont->type == PG_UI_ELEMENT) {
        prop = &cont->elem.properties[prop_id];
    } else return;
    prop->anim_type = PG_UI_NO_ANIM;
}

void pg_ui_simple_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                       enum pg_ui_property_id prop_id, struct pg_simple_anim* anim)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont) return;
    struct pg_ui_property* prop;
    if(cont->type == PG_UI_GROUP && prop_id <= PG_UI_ROTATION) {
        prop = &cont->grp.properties[prop_id];
    } else if(cont->type == PG_UI_ELEMENT) {
        prop = &cont->elem.properties[prop_id];
    } else return;
    if(!anim) {
        prop->anim_type = PG_UI_NO_ANIM;
        return;
    }
    prop->anim_type = PG_UI_SIMPLE_ANIM;
    prop->simple_anim = *anim;
}

void pg_ui_keyframe_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                         enum pg_ui_property_id prop_id, const struct pg_animation* anim,
                         int start_frame, int loop_start, int loop_end,
                         int loop_count, int loop_mode)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont) return;
    struct pg_ui_property* prop;
    if(cont->type == PG_UI_GROUP && prop_id <= PG_UI_ROTATION) {
        prop = &cont->grp.properties[prop_id];
    } else if(cont->type == PG_UI_ELEMENT) {
        prop = &cont->elem.properties[prop_id];
    } else return;
    if(!anim) {
        prop->anim_type = PG_UI_NO_ANIM;
        return;
    }
    prop->anim_type = PG_UI_KEYFRAME_ANIM;
    prop->kf_anim.base_value = prop->value;
    pg_animate(&prop->kf_anim, anim, start_frame, loop_start, loop_end, loop_count, loop_mode);
}

static vec4 pg_ui_prop_value(struct pg_ui_property* prop, float t)
{
    switch(prop->anim_type) {
        case PG_UI_KEYFRAME_ANIM: {
            return pg_anim_prop_value(&prop->kf_anim, t);
        } case PG_UI_SIMPLE_ANIM: {
            vec4 v = pg_simple_anim_value(&prop->simple_anim, t);
            if(!prop->simple_anim.absolute) v = vec4_add(prop->value, v);
            return v;
        } default: case PG_UI_NO_ANIM: {
            return prop->value;
        }
    }
}

static void pg_ui_prop_update(struct pg_ui_property* prop, float t)
{
    if(prop->anim_type == PG_UI_KEYFRAME_ANIM) {
        pg_anim_prop_update(&prop->kf_anim, t);
    }
}

/************************************************/
/*  Update                                      */
/************************************************/

/*  Stack data  */
struct ui_frame {
    vec2 mouse;
    int scroll;
    uint32_t input;
    int mouse_clipped;
    int mouse_hover_consumed;
    int mouse_input_consumed;
    int mouse_scroll_consumed;
    pg_ui_t current;
    pg_ui_t parent_ref;
    struct pg_ui_group* parent;
};

static void pg_ui_content_update(struct pg_ui_context* ctx,
                                 struct pg_ui_content* cont,
                                 struct ui_frame* frame);

static void group_update(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                         struct ui_frame* frame)
{
    struct pg_ui_group* grp = &cont->grp;
    struct ui_frame in_frame = *frame;
    /*  Update all the animations first */
    pg_ui_prop_update(&grp->properties[PG_UI_PIXEL_POS], ctx->time);
    pg_ui_prop_update(&grp->properties[PG_UI_POS], ctx->time);
    pg_ui_prop_update(&grp->properties[PG_UI_SCALE], ctx->time);
    pg_ui_prop_update(&grp->properties[PG_UI_ROTATION], ctx->time);
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    vec4 a_pos, a_pix_pos, a_scale;
    a_pos = pg_ui_prop_value(&grp->properties[PG_UI_POS], ctx->time);
    a_pix_pos = pg_ui_prop_value(&grp->properties[PG_UI_PIXEL_POS], ctx->time);
    a_scale = pg_ui_prop_value(&grp->properties[PG_UI_SCALE], ctx->time);
    float rot = pg_ui_prop_value(&grp->properties[PG_UI_ROTATION], ctx->time).x;
    vec2 pos = vec2_add( vec2(VEC_XY(a_pix_pos)),
                vec2_mul( vec2(VEC_XY(a_pos)), ctx->screen_res ));
    vec2 scale = vec2_recip(vec2(VEC_XY(a_scale)));
    /*  Transform mouse pos to group-local coordinates  */
    in_frame.mouse = vec2_mul( vec2_rotate( vec2_sub(in_frame.mouse, pos), rot), scale);
    cont->mouse_pos = in_frame.mouse;
    if(grp->enable_clip) {
        vec4 a_clip, a_pix_clip, clip;
        a_clip = pg_ui_prop_value(&grp->properties[PG_UI_CLIP], ctx->time);
        a_pix_clip = pg_ui_prop_value(&grp->properties[PG_UI_PIXEL_CLIP], ctx->time);
        clip = vec4_add( a_pix_clip,
               vec4_mul( a_clip, vec4(VEC_XYXY(ctx->screen_res)) ));
        vec2 mouse_to_clip = vec2_scale(vec2_abs(vec2_sub(vec2(VEC_XY(clip)), in_frame.mouse)), 2);
        if(mouse_to_clip.x > clip.z || mouse_to_clip.y > clip.w) {
            in_frame.mouse_clipped = 1;
        }
    }
    /*  Handle drag-and-drop, dropping through nested groups    */
    if(in_frame.parent && in_frame.parent->queued_drop >= 0) {
        vec2 drop_pos = vec2_sub(pos, in_frame.parent->drop_pos);
        drop_pos = vec2_mul(vec2_rotate(drop_pos, rot), scale);
        pg_ui_drag_drop(ctx, in_frame.current, in_frame.parent->queued_drop, drop_pos);
    }
    /*  Iterate through all the children and do the same    */
    in_frame.parent = grp;
    in_frame.parent_ref = in_frame.current;
    int i;
    struct pg_ui_content* iter_cont;
    pg_ui_t iter_ref;
    ARR_FOREACH_REV(grp->children_arr, iter_ref, i) {
        iter_cont = ui_dereference(ctx, iter_ref);
        if(!iter_cont) continue;
        in_frame.current = iter_ref;
        pg_ui_content_update(ctx, iter_cont, &in_frame);
    }
    /*  Propagate input consumption back upward */
    frame->mouse_input_consumed = in_frame.mouse_input_consumed;
    frame->mouse_hover_consumed = in_frame.mouse_hover_consumed;
    frame->mouse_scroll_consumed = in_frame.mouse_scroll_consumed;
    /*  Clear the queued drag-and-drop  */
    if(grp->queued_drop >= 0) {
        if(grp->tried_drop) grp->queued_drop = -1;
        else grp->tried_drop = 1;
    }
}

static void element_update(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                           struct ui_frame* frame)
{
    struct pg_ui_element* elem = &cont->elem;
    struct ui_frame in_frame = *frame;
    /*  Update all the animations first */
    int i;
    for(i = 0; i < PG_UI_ELEM_PROPERTIES; ++i) {
        pg_ui_prop_update(&elem->properties[i], ctx->time);
    }
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    vec4 a_pos, a_pix_pos, a_scale, area, pix_area;
    float rot = pg_ui_prop_value(&elem->properties[PG_UI_ROTATION], ctx->time).x;
    a_pos = pg_ui_prop_value(&elem->properties[PG_UI_POS], ctx->time);
    a_pix_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_POS], ctx->time);
    a_scale = pg_ui_prop_value(&elem->properties[PG_UI_SCALE], ctx->time);
    vec2 pos = vec2_add( vec2(VEC_XY(a_pix_pos)),
                vec2_mul( vec2(VEC_XY(a_pos)), ctx->screen_res ));
    vec2 scale = vec2_recip(vec2(VEC_XY(a_scale)));
    /*  Calculate the action area   */
    if(elem->action_item == PG_UI_ACTION_TEXT) {
        vec4 a_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_POS], ctx->time);
        vec4 a_pix_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_TEXT_POS], ctx->time);
        vec4 a_text_scale = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_SCALE], ctx->time);
        float text_rot = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_ROTATION], ctx->time).x;
        vec2 text_pos = vec2_add( vec2(VEC_XY(a_pix_text_pos)),
                         vec2_mul( vec2(VEC_XY(a_text_pos)), ctx->screen_res ));
        vec2 text_scale = vec2(VEC_XY(a_text_scale));
        vec2 text_area0 = vec2_add(vec2_mul(elem->text_form.area[0], text_scale), text_pos);
        vec2 text_area1 = vec2_add(vec2_mul(elem->text_form.area[1], text_scale), text_pos);
        area = vec4(VEC_XY(text_area0), VEC_XY(text_area1));
        rot -= text_rot;
    } else if(elem->action_item == PG_UI_ACTION_IMG) {
        vec4 a_img_pos = pg_ui_prop_value(&elem->properties[PG_UI_IMG_POS], ctx->time);
        vec4 a_pix_img_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_IMG_POS], ctx->time);
        vec4 a_img_scale = pg_ui_prop_value(&elem->properties[PG_UI_IMG_SCALE], ctx->time);
        vec4 a_pix_img_scale = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_IMG_SCALE], ctx->time);
        float img_rot = pg_ui_prop_value(&elem->properties[PG_UI_IMG_ROTATION], ctx->time).x;
        vec2 img_pos = vec2_floor(
                         vec2_add( vec2(VEC_XY(a_pix_img_pos)),
                          vec2_mul( vec2(VEC_XY(a_img_pos)), ctx->screen_res )));
        vec2 img_scale = vec2_add( vec2(VEC_XY(a_pix_img_scale)),
                           vec2_mul( vec2(VEC_XY(a_img_scale)), ctx->screen_res ));
        img_scale = vec2_scale(img_scale, 0.5);
        area = vec4_add(vec4(VEC_XYXY(img_pos)),
                        vec4(-img_scale.x, -img_scale.y, VEC_XY(img_scale)));
        rot -= img_rot;
    } else if(elem->action_item == PG_UI_ACTION_INDEPENDENT) {
        area = pg_ui_prop_value(&elem->properties[PG_UI_ACTION_AREA], ctx->time);
        pix_area = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_ACTION_AREA], ctx->time);
        area = vec4_add(vec4_mul(area, vec4(VEC_XYXY(ctx->screen_res))), pix_area);
    }
    /*  Handle drag-and-drop    */
    if(elem->callbacks[PG_UI_DRAG_DROP] && in_frame.parent->queued_drop >= 0
    && (in_frame.parent->queued_drop_type | elem->accept_drops)) {
        vec2 drop_pos = vec2_sub(pos, in_frame.parent->drop_pos);
        drop_pos = vec2_mul(vec2_rotate(drop_pos, rot), scale);
        if(vec2_cmp_gt(drop_pos, vec2(VEC_XY(area)))
        && vec2_cmp_lt(drop_pos, vec2(VEC_ZW(area)))) {
            struct pg_ui_event event =
                { .drag_drop.drop = in_frame.parent->queued_drop };
            elem->callbacks[PG_UI_DRAG_DROP](ctx, in_frame.current, &event);
        }
    }
    /*  Transform mouse pos to elem-local coords    */
    in_frame.mouse = vec2_mul(vec2_rotate(vec2_sub(in_frame.mouse, pos), rot), scale);
    cont->mouse_pos = in_frame.mouse;
    /*  Handle mouse input  */
    int mouse_over = 0;
    if(!in_frame.mouse_clipped && !in_frame.mouse_hover_consumed) {
        int consume_input = 0, consume_hover = 0, consume_scroll = 0;
        if(vec2_cmp_gt(in_frame.mouse, vec2(VEC_XY(area)))
        && vec2_cmp_lt(in_frame.mouse, vec2(VEC_ZW(area)))) mouse_over = 1;
        /*  Handle mouse enter/leave events */
        if(mouse_over && !elem->mouse_over && elem->callbacks[PG_UI_ENTER]) {
            struct pg_ui_event event = { .hover = { PG_UI_HOVER_ENTER } };
            consume_hover = cont->elem.callbacks[PG_UI_ENTER](ctx, frame->current, &event);
        } else if(!mouse_over && elem->mouse_over && elem->callbacks[PG_UI_LEAVE]) {
            struct pg_ui_event event = { .hover = { PG_UI_HOVER_LEAVE } };
            consume_hover = cont->elem.callbacks[PG_UI_LEAVE](ctx, frame->current, &event);
        }
        /*  Handle mouse click/hold events  */
        if(mouse_over && !in_frame.mouse_input_consumed) {
            if(in_frame.input == PG_CONTROL_HIT && elem->callbacks[PG_UI_CLICK]) {
                struct pg_ui_event event = { .mouse = {
                    PG_UI_MOUSE_CLICK, PG_UI_MOUSE_LEFT } };
                consume_input = elem->callbacks[PG_UI_CLICK](ctx, frame->current, &event);
            } else if(in_frame.input == PG_CONTROL_HELD && elem->callbacks[PG_UI_HOLD]) {
                struct pg_ui_event event = { .mouse = {
                    PG_UI_MOUSE_HOLD, PG_UI_MOUSE_LEFT } };
                consume_input = elem->callbacks[PG_UI_HOLD](ctx, frame->current, &event);
            } else if(in_frame.input == PG_CONTROL_RELEASED && elem->callbacks[PG_UI_RELEASE]) {
                struct pg_ui_event event = { .mouse = {
                    PG_UI_MOUSE_RELEASE, PG_UI_MOUSE_LEFT } };
                consume_input = elem->callbacks[PG_UI_RELEASE](ctx, frame->current, &event);
            }
        }
        if(mouse_over && in_frame.scroll
        && !in_frame.mouse_scroll_consumed && elem->callbacks[PG_UI_SCROLL]) {
            struct pg_ui_event event = { .mouse = {
                PG_UI_MOUSE_SCROLL, PG_UI_MOUSE_MIDDLE, in_frame.scroll } };
            consume_scroll = elem->callbacks[PG_UI_SCROLL](ctx, frame->current, &event);
        }
        /*  If this element's callbacks should consume the mouse input,
            modify the parent's input frame to reflect that */
        if(consume_input) frame->mouse_input_consumed = 1;
        if(consume_hover) frame->mouse_hover_consumed = 1;
        if(consume_scroll) frame->mouse_scroll_consumed = 1;
    }
    elem->mouse_over = mouse_over;
    /*  Generic callbacks   */
    if(elem->callbacks[PG_UI_UPDATE]) {
        struct pg_ui_event event = {};
        elem->callbacks[PG_UI_UPDATE](ctx, frame->current, &event);
    }
}

static void pg_ui_content_update(struct pg_ui_context* ctx,
                                 struct pg_ui_content* cont,
                                 struct ui_frame* frame)
{
    if(cont->disabled) return;
    if(cont->type == PG_UI_GROUP) group_update(ctx, cont, frame);
    else if(cont->type == PG_UI_ELEMENT) element_update(ctx, cont, frame);
}

void pg_ui_update(struct pg_ui_context* ctx, float time)
{
    ctx->mouse_pos = vec2_mul(pg_mouse_pos(), ctx->screen_res);
    ctx->mouse_ctrl = pg_input_state(PG_LEFT_MOUSE);
    if(pg_input_state(PG_MOUSEWHEEL_UP) == PG_CONTROL_HIT) ctx->mouse_scroll = 1;
    else if(pg_input_state(PG_MOUSEWHEEL_DOWN) == PG_CONTROL_HIT) ctx->mouse_scroll = -1;
    else ctx->mouse_scroll = 0;
    struct ui_frame root_frame = {
        .mouse = ctx->mouse_pos,
        .scroll = ctx->mouse_scroll,
        .input = ctx->mouse_ctrl,
        .current = ctx->root };
    pg_ui_content_update(ctx, ui_dereference(ctx, ctx->root), &root_frame);
    ctx->time = time;
}

/************************************************/
/*  Draw                                        */
/************************************************/

/*  Depth sorting   */
static int layer_comp(pg_ui_t a, pg_ui_t b, struct pg_ui_context* ctx)
{
    struct pg_ui_content* a_cont = &ctx->content_pool.data[a];
    struct pg_ui_content* b_cont = &ctx->content_pool.data[b];
    return (a_cont->layer < b_cont->layer);
}

KSORT_S_DEF(layers, pg_ui_t, struct pg_ui_context*, layer_comp)

static void group_sort_layers(struct pg_ui_context* ctx, struct pg_ui_group* grp)
{
    ks_introsort_s_layers(grp->children_arr.len, grp->children_arr.data, ctx);
    grp->children_added = 0;
}

/*  Stack data  */
struct draw_frame {
    mat4 tx;
    int clip;
};

static void pg_ui_content_draw(struct pg_ui_context* ctx,
                               struct pg_ui_content* cont,
                               struct draw_frame* frame, float time);

static void group_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                       struct draw_frame* frame, float time)
{
    struct pg_ui_group* grp = &cont->grp;
    struct draw_frame in_frame = *frame;
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    vec4 a_pos, a_pix_pos, a_scale, a_clip, a_pix_clip, clip;
    a_pos = pg_ui_prop_value(&grp->properties[PG_UI_POS], time);
    a_pix_pos = pg_ui_prop_value(&grp->properties[PG_UI_PIXEL_POS], time);
    a_scale = pg_ui_prop_value(&grp->properties[PG_UI_SCALE], time);
    float rot = pg_ui_prop_value(&grp->properties[PG_UI_ROTATION], time).x;
    vec2 pos = vec2_floor(
               vec2_add( vec2(VEC_XY(a_pix_pos)),
               vec2_mul( vec2(VEC_XY(a_pos)), ctx->screen_res )));
    in_frame.tx = mat4_mul( in_frame.tx,
                  mat4_rotate( vec3_Z(), rot,
                  mat4_scale_aniso( vec3(VEC_XY(a_scale), 1),
                  mat4_translation( vec3(pos.x, -pos.y, 0) ))));
    /*  Draw a stencil quad if clipping is enabled  */
    if(grp->enable_clip) {
        a_clip = pg_ui_prop_value(&grp->properties[PG_UI_CLIP], time);
        a_pix_clip = pg_ui_prop_value(&grp->properties[PG_UI_PIXEL_CLIP], time);
        clip = vec4_add( a_pix_clip,
               vec4_mul( a_clip, vec4(VEC_XYXY(ctx->screen_res)) ));
        pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass,
            PG_QUADBATCH_STENCIL( .test = 1, .write_mask = 0xFF,
                .func = GL_EQUAL, .ref = in_frame.clip, .func_mask = 0xFF,
                .sfail = GL_KEEP, .dpfail = GL_KEEP, .dppass = GL_INCR ));
        pg_quadbatch_next(&ctx->sprite_batch, &in_frame.tx);
        pg_quadbatch_add_quad(&ctx->sprite_batch, PG_EZQUAD(
            .pos = vec3(VEC_XY(clip)), .scale = vec2(VEC_ZW(clip)),
            .color_mod = 0xFFFFFFFF ));
        pg_quadbatch_draw(&ctx->sprite_batch, &ctx->rendpass);
        pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass,
            PG_QUADBATCH_STENCIL( .test = 1, .write_mask = 0x00,
                .func = GL_EQUAL, .ref = in_frame.clip + 1, .func_mask = 0xFF,
                .sfail = GL_KEEP, .dpfail = GL_KEEP, .dppass = GL_KEEP ));
        in_frame.clip += 1;
    }
    /*  Iterate through all the children and recurse    */
    if(grp->children_added) group_sort_layers(ctx, grp);
    int i;
    struct pg_ui_content* iter_cont;
    pg_ui_t iter_ref;
    ARR_FOREACH(grp->children_arr, iter_ref, i) {
        iter_cont = ui_dereference(ctx, iter_ref);
        if(!iter_cont) continue;
        pg_ui_content_draw(ctx, iter_cont, &in_frame, time);
    }
    /*  Now redraw the stencil quad to get rid of all the potential nested
        stencil quads   */
    if(grp->enable_clip) {
        pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass,
            PG_QUADBATCH_STENCIL( .test = 1, .write_mask = 0xFF,
                .func = GL_LESS, .ref = in_frame.clip - 1, .func_mask = 0xFF,
                .sfail = GL_KEEP, .dpfail = GL_KEEP, .dppass = GL_REPLACE ));
        pg_quadbatch_next(&ctx->sprite_batch, &in_frame.tx);
        pg_quadbatch_add_quad(&ctx->sprite_batch, PG_EZQUAD(
            .pos = vec3(VEC_XY(clip)), .scale = vec2(VEC_ZW(clip)),
            .color_mod = 0xFFFFFFFF ));
        pg_quadbatch_draw(&ctx->sprite_batch, &ctx->rendpass);
        pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass,
            PG_QUADBATCH_STENCIL( .test = 1, .write_mask = 0x00,
                .func = GL_EQUAL, .ref = in_frame.clip - 1, .func_mask = 0xFF,
                .sfail = GL_KEEP, .dpfail = GL_KEEP, .dppass = GL_KEEP ));
    }
}

static void element_draw(struct pg_ui_context* ctx, struct pg_ui_content* cont,
                         struct draw_frame* frame, float time)
{
    struct pg_ui_element* elem = &cont->elem;
    if(elem->draw == PG_UI_DRAW_NONE) return;
    /*  Calculate the basic animated properties (pos, scale, rot)   */
    vec4 a_pos, a_pix_pos, a_scale;
    a_pos = pg_ui_prop_value(&elem->properties[PG_UI_POS], time);
    a_pix_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_POS], time);
    a_scale = pg_ui_prop_value(&elem->properties[PG_UI_SCALE], time);
    float rot = pg_ui_prop_value(&elem->properties[PG_UI_ROTATION], time).x;
    vec2 pos = vec2_floor(
                vec2_add( vec2(VEC_XY(a_pix_pos)),
                 vec2_mul( vec2(VEC_XY(a_pos)), ctx->screen_res )));
    mat4 elem_tx = mat4_mul( frame->tx,
                    mat4_rotate( vec3_Z(), rot,
                     mat4_scale_aniso( vec3(a_scale.x, a_scale.y, 1),
                      mat4_translation( vec3(pos.x, -pos.y, 0) ))));
    pg_quadbatch_next(&ctx->sprite_batch, &elem_tx);
    /*  Kind of ugly here; text drawing is duplicated before and after
        image drawing, to allow the user to set the draw order  */
    if(elem->draw == PG_UI_TEXT_ONLY || elem->draw == PG_UI_TEXT_THEN_IMG) {
        vec4 text_color = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_COLOR], time);
        vec4 a_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_POS], time);
        vec4 a_pix_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_TEXT_POS], time);
        vec4 text_scale = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_SCALE], time);
        float text_rot = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_ROTATION], time).x;
        vec2 text_pos = vec2_floor(
                         vec2_add( vec2(VEC_XY(a_pix_text_pos)),
                          vec2_mul( vec2(VEC_XY(a_text_pos)), ctx->screen_res )));
        pg_quadbatch_add_text(&ctx->sprite_batch, PG_EZDRAW_TEXT(
            .form = &elem->text_form, .color = text_color,
            .pos = vec2(VEC_XY(text_pos)), .scale = vec2(VEC_XY(text_scale)),
            .rotation = text_rot, .anchor = elem->text_anchor ));
    }
    if(elem->draw == PG_UI_IMG_ONLY || elem->draw == PG_UI_IMG_THEN_TEXT
    || elem->draw == PG_UI_TEXT_THEN_IMG) {
        vec4 a_img_pos = pg_ui_prop_value(&elem->properties[PG_UI_IMG_POS], time);
        vec4 a_pix_img_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_IMG_POS], time);
        vec4 a_img_scale = pg_ui_prop_value(&elem->properties[PG_UI_IMG_SCALE], time);
        vec4 a_pix_img_scale = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_IMG_SCALE], time);
        vec4 img_color_mod = pg_ui_prop_value(&elem->properties[PG_UI_IMG_COLOR_MOD], time);
        vec4 img_color_add = pg_ui_prop_value(&elem->properties[PG_UI_IMG_COLOR_ADD], time);
        vec4 img_frame = pg_ui_prop_value(&elem->properties[PG_UI_IMG_FRAME], time);
        float img_rot = pg_ui_prop_value(&elem->properties[PG_UI_IMG_ROTATION], time).x;
        vec2 img_pos = vec2_floor(
                         vec2_add( vec2(VEC_XY(a_pix_img_pos)),
                          vec2_mul( vec2(VEC_XY(a_img_pos)), ctx->screen_res )));
        vec2 img_scale = vec2_add( vec2(VEC_XY(a_pix_img_scale)),
                           vec2_mul( vec2(VEC_XY(a_img_scale)), ctx->screen_res ));
        pg_quadbatch_add_sprite(&ctx->sprite_batch, PG_EZDRAW_2D(
            .frame = { img_frame, elem->img_layer },
            .pos = vec3(VEC_XY(img_pos)), .scale = img_scale, .rotation = img_rot,
            .color_mod = img_color_mod, .color_add = img_color_add ));
    }
    if(elem->draw == PG_UI_IMG_THEN_TEXT) {
        vec4 text_color = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_COLOR], time);
        vec4 a_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_POS], time);
        vec4 a_pix_text_pos = pg_ui_prop_value(&elem->properties[PG_UI_PIXEL_TEXT_POS], time);
        vec4 text_scale = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_SCALE], time);
        float text_rot = pg_ui_prop_value(&elem->properties[PG_UI_TEXT_ROTATION], time).x;
        vec2 text_pos = vec2_floor(
                         vec2_add( vec2(VEC_XY(a_pix_text_pos)),
                          vec2_mul( vec2(VEC_XY(a_text_pos)), ctx->screen_res )));
        pg_quadbatch_add_text(&ctx->sprite_batch, PG_EZDRAW_TEXT(
            .form = &elem->text_form, .color = text_color,
            .pos = vec2(VEC_XY(text_pos)), .scale = vec2(VEC_XY(text_scale)),
            .rotation = text_rot, .anchor = elem->text_anchor ));
    }
    pg_quadbatch_draw(&ctx->sprite_batch, &ctx->rendpass);
}

static void pg_ui_content_draw(struct pg_ui_context* ctx,
                               struct pg_ui_content* cont,
                               struct draw_frame* frame, float time)
{
    if(cont->disabled) return;
    if(cont->type == PG_UI_GROUP) group_draw(ctx, cont, frame, time);
    else if(cont->type == PG_UI_ELEMENT) element_draw(ctx, cont, frame, time);
}

void pg_ui_draw(struct pg_ui_context* ctx, float dt)
{
    pg_renderpass_clear(&ctx->rendpass);
    pg_quadbatch_reset(&ctx->sprite_batch);
    float time = ctx->time + dt;
    struct draw_frame root_frame = { .clip = 0, .tx = mat4_identity() };
    pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass, PG_QUADBATCH_STENCIL());
    pg_ui_content_draw(ctx, ui_dereference(ctx, ctx->root), &root_frame, time);
    pg_quadbatch_stencil(&ctx->sprite_batch, &ctx->rendpass, PG_QUADBATCH_STENCIL());
    pg_quadbatch_buffer(&ctx->sprite_batch);
}

/************************************************/
/*  Building UI hierarchy                       */
/************************************************/


pg_ui_t pg_ui_add_group(struct pg_ui_context* ctx, pg_ui_t parent,
                                  const char* name, struct pg_ui_properties* props)
{
    struct pg_ui_content* p_cont = ui_dereference(ctx, parent);
    if(!p_cont || p_cont->type != PG_UI_GROUP) {
        printf("procgame ERROR: Can't create UI group %s, invalid parent\n", name);
        return -1;
    }
    struct pg_ui_content* new_cont;
    pg_ui_t new_ref = ui_alloc(ctx, 1, &new_cont);
    p_cont = ui_dereference(ctx, parent);
    pg_ui_group_init(&new_cont->grp, props);
    new_cont->layer = props->layer;
    new_cont->type = PG_UI_GROUP;
    strncpy(new_cont->name, name, 32);
    new_cont->parent = parent;
    new_cont->grp.queued_drop = -1;
    new_cont->disabled = !props->enabled;
    HTABLE_SET(p_cont->grp.children, name, new_ref);
    ARR_PUSH(p_cont->grp.children_arr, new_ref);
    p_cont->grp.children_added += 1;
    return new_ref;
}

pg_ui_t pg_ui_add_elem(struct pg_ui_context* ctx, pg_ui_t parent,
                                 const char* name, struct pg_ui_properties* props)
{
    struct pg_ui_content* p_cont = ui_dereference(ctx, parent);
    if(!p_cont || p_cont->type != PG_UI_GROUP) {
        printf("procgame ERROR: Can't create UI element %s, invalid parent\n", name);
        return -1;
    }
    struct pg_ui_content* new_cont;
    pg_ui_t new_ref = ui_alloc(ctx, 1, &new_cont);
    p_cont = ui_dereference(ctx, parent);
    strncpy(new_cont->name, name, 32);
    new_cont->parent = parent;
    pg_ui_elem_init(ctx, new_cont, props);
    HTABLE_SET(p_cont->grp.children, name, new_ref);
    ARR_PUSH(p_cont->grp.children_arr, new_ref);
    p_cont->grp.children_added += 1;
    return new_ref;
}

void pg_ui_delete(struct pg_ui_context* ctx, pg_ui_t del)
{
    struct pg_ui_content* del_cont = ui_dereference(ctx, del);
    if(!del_cont) return;
    struct pg_ui_content* p_cont = ui_dereference(ctx, del_cont->parent);
    if(p_cont) {
        int i;
        pg_ui_t arr_child;
        ARR_FOREACH(p_cont->grp.children_arr, arr_child, i) {
            if(arr_child == del) {
                ARR_SPLICE(p_cont->grp.children_arr, i, 1);
                break;
            }
        }
        HTABLE_UNSET(p_cont->grp.children, del_cont->name);
    }
    ui_free(ctx, del);
}

/************************************************/
/* General UI function                          */
/************************************************/

float pg_ui_get_aspect(struct pg_ui_context* ctx)
{
    return ctx->screen_res.x / ctx->screen_res.y;
}

void pg_ui_enabled(struct pg_ui_context* ctx, pg_ui_t ui_ref, int enabled)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(cont) cont->disabled = !enabled;
}

pg_ui_t pg_ui_child(struct pg_ui_context* ctx, pg_ui_t parent,
                              const char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, parent);
    if(!cont || cont->type != PG_UI_GROUP) return -1;
    pg_ui_t* ref;
    HTABLE_GET(cont->grp.children, name, ref);
    return ref ? *ref : -1;
}

pg_ui_t pg_ui_child_path(struct pg_ui_context* ctx, pg_ui_t parent,
                                   const char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, parent);
    if(!cont || cont->type != PG_UI_GROUP) return -1;
    pg_ui_t* child;
    struct pg_ui_content* iter_cont = cont;
    int i = 0;
    const char* token = name;
    int token_len = 0;
    int token_start = 0;
    while(name[i] != '\0') {
        if(name[i] == '.') {
            token_len = i - token_start;
            if(token_len == 0) return -1;
            HTABLE_NGET(iter_cont->grp.children, name + token_start, token_len, child);
            if(!child) return -1;
            iter_cont = ui_dereference(ctx, *child);
            if(!iter_cont || iter_cont->type != PG_UI_GROUP) return -1;
            token_start = i + 1;
        }
        ++i;
    }
    token_len = i - token_start;
    HTABLE_NGET(iter_cont->grp.children, name + token_start, token_len, child);
    if(!child) return -1;
    return *child;
}

pg_ui_t pg_ui_parent(struct pg_ui_context* ctx, pg_ui_t child)
{

    struct pg_ui_content* cont = ui_dereference(ctx, child);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return -1;
    return cont->parent;
}

struct pg_type* pg_ui_variable(struct pg_ui_context* ctx,
                               pg_ui_t ui_ref, char* name)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return NULL;
    struct pg_type* ret;
    HTABLE_GET(cont->vars, name, ret);
    if(!ret) HTABLE_SET_GET(cont->vars, name, ret, PG_TYPE_INT());
    return ret;
}

void pg_ui_set_text_formatter(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                              struct pg_text_formatter* formatter)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.text_formatter = *formatter;
}

void pg_ui_set_text(struct pg_ui_context* ctx, pg_ui_t ui_ref, const wchar_t* text, int n)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    int len = wcsnlen(text, n);
    pg_text_form_alloc_reserve(&cont->elem.text_form, len);
    pg_text_format(&cont->elem.text_form, &cont->elem.text_formatter, text, len);
}

void pg_ui_set_text_c(struct pg_ui_context* ctx, pg_ui_t ui_ref, const char* text, int n)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    int len = strnlen(text, n);
    pg_text_form_alloc_reserve(&cont->elem.text_form, len);
    pg_text_format_c(&cont->elem.text_form, &cont->elem.text_formatter, text, len);
}

void pg_ui_draw_mode(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                enum pg_ui_draw_mode draw)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.draw = draw;
}

const char* pg_ui_get_name(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return "<null>";
    return cont->name;
}

vec4* pg_ui_get_property(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                         enum pg_ui_property_id prop_id)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont) return NULL;
    struct pg_ui_property* prop = NULL;
    if(cont->type == PG_UI_GROUP && prop_id <= PG_UI_ROTATION) {
        prop = &cont->grp.properties[prop_id];
    } else if(cont->type == PG_UI_ELEMENT) {
        prop = &cont->elem.properties[prop_id];
    }
    if(!prop) return NULL;
    return &prop->value;
}

vec2 pg_ui_mouse_pos(struct pg_ui_context* ctx, pg_ui_t ui_ref)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui_ref);
    if(!cont || cont->type <= PG_UI_UNINITIALIZED) return vec2(0,0);
    return cont->mouse_pos;
}

void pg_ui_drag_drop(struct pg_ui_context* ctx, pg_ui_t dst,
                     pg_ui_t dropped, vec2 drop_pos)
{
    struct pg_ui_content* dst_cont = ui_dereference(ctx, dst);
    struct pg_ui_content* drop_cont = ui_dereference(ctx, dropped);
    if(!dst_cont || dst_cont->type != PG_UI_GROUP || !drop_cont 
    || !(dst_cont->grp.accept_drops | drop_cont->elem.drop_type)) return;
    dst_cont->grp.drop_pos = drop_pos;
    dst_cont->grp.tried_drop = 0;
    dst_cont->grp.queued_drop = dropped;
    dst_cont->grp.queued_drop_type = drop_cont->elem.drop_type;
}

void pg_ui_set_callback(struct pg_ui_context* ctx, pg_ui_t ui,
                        enum pg_ui_callback_id id, pg_ui_callback_t cb)
{
    struct pg_ui_content* cont = ui_dereference(ctx, ui);
    if(!cont || cont->type != PG_UI_ELEMENT) return;
    cont->elem.callbacks[id] = cb;
}
