#version 330

const vec4 verts[4] =
    vec4[](vec4(-0.5, 0.5,  0.00001, 0.99999),
           vec4(0.5, 0.5,   0.99999, 0.99999),
           vec4(-0.5, -0.5, 0.00001, 0.00001),
           vec4(0.5, -0.5,  0.99999, 0.00001));

uniform vec2 pg_resolution;
uniform mat4 pg_matrix_projview;
/*  [x1, y1, x2, y2, layer] */
uniform float frame[5];
/*  [x, y, z, w, h, rotate]    */
uniform float sprite[6];

out vec3 f_tex_coord;

void main()
{
    vec4 tex_frame = vec4(frame[0], frame[1], frame[2], frame[3]);
    vec4 sprite_transform = vec4(sprite[0], -sprite[1], sprite[3], sprite[4]);
    float rotation = sprite[5];
    float s = sin(rotation);
    float c = cos(rotation);
    mat2 rotate = mat2(c, -s, s, c);
    vec2 vert = rotate * (verts[gl_VertexID].xy * sprite_transform.zw)
                 + sprite_transform.xy;
    f_tex_coord = vec3(
        mix(tex_frame.x, tex_frame.z, verts[gl_VertexID].z),
        mix(tex_frame.y, tex_frame.w, verts[gl_VertexID].w), frame[4]);
    gl_Position = pg_matrix_projview * vec4(vert, sprite[2], 1);
}

