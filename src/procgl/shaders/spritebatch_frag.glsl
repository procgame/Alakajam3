#version 330

uniform sampler2DArray pg_texture_1;
uniform float alpha_cutoff;

in vec3 f_tex_coord;
in vec4 f_color_mod;
in vec4 f_color_add;

layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 tex_color = texture(pg_texture_1, f_tex_coord);
    tex_color = tex_color * f_color_mod + f_color_add;
    if(tex_color.a <= alpha_cutoff) discard;
    frag_color = tex_color;
}


