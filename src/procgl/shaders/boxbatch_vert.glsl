#version 330

#define BOX_SIZE    0.5
#define BOX_HEIGHT  1

const vec3 verts[36] = vec3[](
    /*  Right   */
    vec3( BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE, -BOX_SIZE,         0),
    vec3( BOX_SIZE,  BOX_SIZE,         0),
    vec3( BOX_SIZE, -BOX_SIZE,         0),
    vec3( BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    /*  Left    */
    vec3(-BOX_SIZE, -BOX_SIZE,         0),
    vec3(-BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE,  BOX_SIZE,         0),
    vec3(-BOX_SIZE, -BOX_SIZE,         0),
    vec3(-BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    /*  Front   */
    vec3( BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE,  BOX_SIZE,         0),
    vec3(-BOX_SIZE,  BOX_SIZE,         0),
    vec3( BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE,  BOX_SIZE,         0),
    vec3(-BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    /*  Back    */
    vec3( BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE, -BOX_SIZE,         0),
    vec3( BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE, -BOX_SIZE,         0),
    vec3( BOX_SIZE, -BOX_SIZE,         0),
    /*  Top     */
    vec3(-BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3(-BOX_SIZE,  BOX_SIZE,  BOX_HEIGHT),
    vec3( BOX_SIZE, -BOX_SIZE,  BOX_HEIGHT),
    /*  Bottom  */
    vec3( BOX_SIZE,  BOX_SIZE,         0),
    vec3(-BOX_SIZE, -BOX_SIZE,         0),
    vec3(-BOX_SIZE,  BOX_SIZE,         0),
    vec3( BOX_SIZE,  BOX_SIZE,         0),
    vec3( BOX_SIZE, -BOX_SIZE,         0),
    vec3(-BOX_SIZE, -BOX_SIZE,         0) );

#define TEX_0   -1
#define TEX_1    1
const vec2 uv_verts[36] = vec2[](
    /*  Right   */
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_0), vec2(TEX_1, TEX_0),
    vec2(TEX_0, TEX_0), vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_1),
    /*  Left    */
    vec2(TEX_0, TEX_0), vec2(TEX_1, TEX_1), vec2(TEX_1, TEX_0),
    vec2(TEX_0, TEX_0), vec2(TEX_0, TEX_1), vec2(TEX_1, TEX_1),
    /*  Front   */
    vec2(TEX_1, TEX_1), vec2(TEX_1, TEX_0), vec2(TEX_0, TEX_0),
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_0), vec2(TEX_0, TEX_1),
    /*  Back    */
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_1), vec2(TEX_0, TEX_0),
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_0), vec2(TEX_1, TEX_0),
    /*  Top     */
    vec2(TEX_0, TEX_1), vec2(TEX_0, TEX_0), vec2(TEX_1, TEX_0),
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_1), vec2(TEX_1, TEX_0),
    /*  Bottom  */
    vec2(TEX_1, TEX_1), vec2(TEX_0, TEX_0), vec2(TEX_0, TEX_1),
    vec2(TEX_1, TEX_1), vec2(TEX_1, TEX_0), vec2(TEX_0, TEX_0) );

    /*  RIGHT AND LEFT SCALE TEX BASED ON CAP Y SCALE   */
    /*  FRONT AND BACK SCALE TEX BASED ON CAP X SCALE   */
    /*  TOP AND BOTTOM SCALE TEX BASED ON CAP X AND Y SCALE */
const vec2 uv_scale[6] = vec2[](
    vec2(0, 1), vec2(0, 1),
    vec2(1, 0), vec2(1, 0),
    vec2(1, 1), vec2(1, 1) );

uniform mat4 pg_matrix_mvp;

#define sprites pg_texture_0
uniform samplerBuffer sprites;
uniform int sprite_offset;
uniform vec2 tex_size;

out vec3 f_tex_coord;
out vec4 f_color_mod;
out vec4 f_color_add;

vec4 unpack_uint(float u_)
{
    uint u = floatBitsToUint(u_);
    return vec4(
        float((u & uint(0xFF000000)) >> 24),
        float((u & uint(0x00FF0000)) >> 16),
        float((u & uint(0x0000FF00)) >> 8),
        float((u & uint(0x000000FF)) >> 0)) / 255;
}

vec4 unpack_uint_raw(float u_)
{
    uint u = floatBitsToUint(u_);
    return vec4(
        float((u & uint(0xFF000000)) >> 24),
        float((u & uint(0x00FF0000)) >> 16),
        float((u & uint(0x0000FF00)) >> 8),
        float((u & uint(0x000000FF)) >> 0));
}

vec4 unpack_ulong(float a, float b)
{
    uint a_ = floatBitsToUint(a);
    uint b_ = floatBitsToUint(b);
    return vec4(
        float((b_ & uint(0xFFFF0000)) >> 16),
        float((b_ & uint(0x0000FFFF)) >> 0),
        float((a_ & uint(0xFFFF0000)) >> 16),
        float((a_ & uint(0x0000FFFF)) >> 0)) / 65535;
}

vec3 quat_apply(vec4 q, vec3 v)
{
    return v + 2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v);
}

/*
    struct {
        float3 pos; float scale;

        uint packed_scale_xyz; uint packed_end_scale;
        uint packed_quat; uint tex_layer;

        uint packed_tex_left; uint packed_tex_right;
        uint packed_tex_front; uint packed_tex_back;

        uint packed_tex_top; uint packed_tex_bottom;
        uint color_mod; uint color_add;
    }
*/

void main()
{
    int buf_offset = gl_VertexID / 36 * 4;
    int vert_idx = gl_VertexID % 36;
    /*  Unpack the data from the box-buffer    */
    vec4 batch[4] = vec4[](
        vec4(texelFetch(sprites, sprite_offset + 0 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 1 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 2 + buf_offset)),
        vec4(texelFetch(sprites, sprite_offset + 3 + buf_offset)) );
    vec3 pos = batch[0].xyz;
    float iso_scale = batch[0].w;
    vec4 aniso_scale = unpack_uint(batch[1].x);
    vec4 endcap_scale = unpack_uint(batch[1].y);
    vec4 rotation = unpack_ulong(batch[1].z, batch[1].w) * 2 - 1;
    float tex_layer = float(floatBitsToUint(batch[1].x) & uint(0xFF));
    float tex_sides[6] = float[]( batch[2].x, batch[2].y, batch[2].z,
                                  batch[2].w, batch[3].x, batch[3].y );
    f_color_mod = unpack_uint(batch[3].z);
    f_color_add = unpack_uint(batch[3].w);
    /*  Apply the transforms to the vertex  */
    vec3 vert = verts[vert_idx];
    bool is_top = bool(vert.z);
    vec3 cap_scale = vec3(mix(endcap_scale.xy, endcap_scale.zw, is_top), 1);
    vert.xyz *= cap_scale * aniso_scale.xyz * iso_scale;
    vert = pos + quat_apply(rotation, vert);
    gl_Position = pg_matrix_mvp * vec4(vert.xyz, 1);
    /*  Calculate the texture coordinates   */
    vec2 uv = uv_verts[vert_idx];
    int side = vert_idx / 6;
    int axis = side / 2;
    if(axis == 0) uv.x *= cap_scale.y;
    else if(axis == 1) uv.x *= cap_scale.x;
    else if(axis == 2) uv.xy *= cap_scale.xy;
    uv.xy = uv.xy * 0.5 + 0.5;
    vec4 tex_frame = unpack_uint_raw(tex_sides[side]);
    vec2 tex_coord = vec2(mix(tex_frame.x, tex_frame.z, uv.x),
                          mix(tex_frame.y, tex_frame.w, uv.y)) / tex_size;
    f_tex_coord = vec3(tex_coord.xy, tex_layer);
}



