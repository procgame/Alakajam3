#version 330

uniform sampler2DArray pg_texture_0;
uniform int pg_tex_layer_0;
uniform float pg_aspect_ratio;
/*  [frequency, phase, amplitude] */
uniform vec3 wave;
uniform vec2 axis;

in vec2 f_tex_coord;

out vec4 frag_color;

void main()
{
    vec2 new_coord = (f_tex_coord * 2 - 1) * vec2(pg_aspect_ratio, 1);
    vec2 screen_axis = normalize(axis);
    float p = dot(new_coord, screen_axis);
    float sin_offset = sin(p * (wave[0] * 3.14159) + wave[1]) * wave[2];
    vec2 sin_coord = vec2(-screen_axis.y / pg_aspect_ratio, screen_axis.x);
    sin_coord = f_tex_coord + sin_coord * sin_offset;
    frag_color = texture(pg_texture_0, vec3(sin_coord.xy, pg_tex_layer_0));
}
