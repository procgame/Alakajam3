#version 330

const vec4 verts[6] = vec4[](
           vec4(-0.5, -0.5, 0.00001, 0.00001),
           vec4(-0.5, 0.5,  0.00001, 0.99999),
           vec4(0.5, -0.5,  0.99999, 0.00001),
           vec4(0.5, -0.5,  0.99999, 0.00001),
           vec4(-0.5, 0.5,  0.00001, 0.99999),
           vec4(0.5, 0.5,   0.99999, 0.99999));

uniform mat4 pg_matrix_projview;
/*  Number of glyphs per row in the texture */
uniform uint font_pitch;
/*  Size of each glyph in UV (x,y) and actual units (z,w)   */
uniform vec4 glyph_size;
/*  64-char string (packed to uints)    */
uniform uint chars[16];
/*  [ Pos x,y | Scale z,w ][ Spacing x,y | Rotation z | Anchor w ]
    [ color mod ][ color add ]   */
uniform vec4 text_style[4];

#define UINT_CHAR(c, i) (((c) >> (i * 8)) & uint(0xFF))

out vec3 f_tex;

void main()
{
    /*  Re-structure the style data */
    vec2 pos = vec2(text_style[0].x, -text_style[0].y);
    vec2 scale = text_style[0].zw;
    vec2 spacing = text_style[1].xy;
    float rotation = text_style[1].z;
    float anchor = text_style[1].w - 0.5;

    /*  Find the char to use    */
    int char_idx = int(gl_VertexID / 6);
    int uint_idx = char_idx / 4;
    int char_idx_in_uint = char_idx % 4;
    uint ch = UINT_CHAR(chars[uint_idx], char_idx_in_uint);

    /*  Make a rotation matrix  */
    float s = sin(rotation);
    float c = cos(rotation);
    mat2 rotate = mat2(c, -s, s, c);

    /*  Scale and positioning transformations   */
    int vert_idx = int(gl_VertexID % 6);
    vec2 glyph = glyph_size.zw * scale;
    vec2 glyph_stride = glyph * spacing;
    vec2 glyph_offset = glyph_stride * vec2(char_idx)
                        - glyph_stride * vec2(anchor);
    vec2 vert = rotate * (verts[vert_idx].xy * glyph + glyph_offset) + pos;
    gl_Position = pg_matrix_projview * vec4(vert.xy, 0, 1);

    /*  Decide the texture coords in the font based on char value (ASCII)   */
    uint frame = ch - uint(32);
    vec2 tex_offset = vec2(frame % font_pitch * glyph_size.x,
                           frame / font_pitch * glyph_size.y);
    f_tex = vec3(verts[vert_idx].zw * glyph_size.xy + tex_offset, 0);
}

