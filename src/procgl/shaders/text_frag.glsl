#version 330

uniform sampler2DArray pg_texture_0;
uniform vec4 text_style[4];

in vec3 f_tex;

layout(location = 0) out vec4 frag_color;

void main()
{
    vec4 color_mod = text_style[2];
    vec4 color_add = text_style[3];
    vec4 tex_color = texture(pg_texture_0, f_tex);
    frag_color = tex_color * color_mod + color_add;
}
