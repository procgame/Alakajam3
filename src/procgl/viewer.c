#include "ext/linmath.h"
#include "viewer.h"

void pg_viewer_perspective(struct pg_viewer* view, vec3 pos, quat orientation,
                           vec2 size, vec2 near_far)
{
    view->pos = vec3_scale(pos, -1);
    view->orientation = orientation;
    view->size = size;
    view->near_far = near_far;
    view->view_matrix = mat4_translate(view->pos, mat4_from_quat(orientation));
    view->proj_matrix = mat4_perspective(1.5f, size.x / size.y, near_far.x, near_far.y);
    view->projview_matrix = mat4_mul(view->proj_matrix, view->view_matrix);
    view->projview_inverse = mat4_inverse(view->projview_matrix);
}

void pg_viewer_ortho(struct pg_viewer* view, vec2 pos, vec2 scale,
                     float rotation, vec2 size)
{
    view->pos = vec3(vec_cast2(pos));
    view->orientation = quat_rotation(vec3(0, 0, 1), rotation);
    view->scale = scale;
    view->size = size;
    view->near_far = vec2(-1, 1);
    view->view_matrix = mat4_translate(view->pos, mat4_from_quat(view->orientation));
    view->proj_matrix = mat4_ortho(-size.x, size.x, size.y, -size.y, -1, 1);
    view->projview_matrix = mat4_mul(view->proj_matrix, view->view_matrix);
    view->projview_inverse = mat4_inverse(view->projview_matrix);
}

vec2 pg_viewer_project(struct pg_viewer* view, vec3 pos)
{
    vec4 pos_ = vec4(vec_cast3(pos), 1);
    vec4 out = mat4_mul_vec4(view->projview_matrix, pos_);
    out.x /= out.w;
    out.y /= out.w;
    return vec2(vec_cast2(out));
}

vec3 pg_viewer_unproject(struct pg_viewer* view, vec3 pos)
{
    vec4 pos_ = vec4(vec_cast3(pos), 1);
    vec4 out = mat4_mul_vec4(view->projview_inverse, pos_);
    return vec3(vec_cast3(out));
}

vec2 pg_viewport_coord(struct pg_viewport* vp, vec2 in)
{
    vec2 p = vec2_sub(in, vp->pos);
    p = vec2_div(p, vp->size);
    p = vec2_sub(vec2_scale(p, 2), vec2(1,1));
    return p;
}

vec2 pg_viewport_res(struct pg_viewport* vp, vec2 screen)
{
    return vec2_mul(screen, vec2_scale(vp->size, 0.5));
}
