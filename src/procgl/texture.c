#include "procgl.h"
#include "ext/lodepng.h"

static const struct tex_format {
    int channels;
    enum pg_data_type pgtype;
    GLenum iformat, pixformat, type;
    size_t size;
} gl_tex_formats[] = {
    [PG_UBYTE] = { 1, PG_UBYTE, GL_R8, GL_RED, GL_UNSIGNED_BYTE, sizeof(uint8_t) },
    [PG_UBVEC2] = { 2, PG_UBYTE, GL_RG8, GL_RG, GL_UNSIGNED_BYTE, sizeof(uint8_t) * 2 },
    [PG_UBVEC3] = { 3, PG_UBYTE, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE, sizeof(uint8_t) * 3 },
    [PG_UBVEC4] = { 4, PG_UBYTE, GL_RGBA8, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8, sizeof(uint8_t) * 4 },
    [PG_UINT] = { 1, PG_UINT, GL_R32UI, GL_RED, GL_UNSIGNED_INT, sizeof(uint32_t) },
    [PG_UVEC2] = { 2, PG_UINT, GL_RG32UI, GL_RG, GL_UNSIGNED_INT, sizeof(uint32_t) * 2 },
    [PG_UVEC3] = { 3, PG_UINT, GL_RGB32UI, GL_RGB, GL_UNSIGNED_INT, sizeof(uint32_t) * 3 },
    [PG_UVEC4] = { 4, PG_UINT, GL_RGBA32UI, GL_RGBA, GL_UNSIGNED_INT, sizeof(uint32_t) * 4 },
    [PG_INT] = { 1, PG_INT, GL_R32I, GL_RED, GL_INT, sizeof(int32_t) },
    [PG_IVEC2] = { 2, PG_INT, GL_RG32I, GL_RG, GL_INT, sizeof(int32_t) * 2 },
    [PG_IVEC3] = { 3, PG_INT, GL_RGB32I, GL_RGB, GL_INT, sizeof(int32_t) * 3 },
    [PG_IVEC4] = { 4, PG_INT, GL_RGBA32I, GL_RGBA, GL_INT, sizeof(int32_t) * 4 },
    [PG_FLOAT] = { 1, PG_FLOAT, GL_R32F, GL_RED, GL_FLOAT, sizeof(float) },
    [PG_VEC2] = { 2, PG_FLOAT, GL_RG32F, GL_RG, GL_FLOAT, sizeof(float) * 2 },
    [PG_VEC3] = { 3, PG_FLOAT, GL_RGB32F, GL_RGB, GL_FLOAT, sizeof(float) * 3 },
    [PG_VEC4] = { 4, PG_FLOAT, GL_RGBA32F, GL_RGBA, GL_FLOAT, sizeof(float) * 4  }
};

void pg_texture_init_from_file(struct pg_texture* tex, const char* file,
                               struct pg_texture_opts* opts)
{
    pg_texture_from_files(tex, 1, &file, opts);
}

void pg_texture_from_files(struct pg_texture* tex, int num_files,
                           const char** files, struct pg_texture_opts* opts)
{
    uint8_t* tex_data[num_files];
    uvec2 tex_size[num_files];
    uvec2 max_size = uvec2();
    unsigned error = 0;
    /*  First load all the texture data individually    */
    int i;
    for(i = 0; i < num_files; ++i) {
        if(files[i][0] == ':') {
            sscanf(files[i], ":%i,%i", &tex_size[i].x, &tex_size[i].y);
            tex_data[i] = NULL;
        } else {
            error = lodepng_decode32_file(&tex_data[i],
                        &tex_size[i].x, &tex_size[i].y, files[i]);
            if(error) {
                tex_data[i] = NULL;
                tex_size[i] = uvec2(0, 0);
                printf("procgame ERROR\n"
                       "    Failed to load texture (%s)\n"
                       "    LodePNG error %u: %s\n",
                       files[i], error, lodepng_error_text(error));
                continue;
            }
        }
        if(tex_size[i].x > max_size.x || tex_size[i].y > max_size.y) {
            max_size = tex_size[i];
        }
    }
    /*  Now allocate a big single allocation to hold all of them    */
    size_t pixel_stride = sizeof(uint8_t) * 4;
    size_t tex_stride = max_size.x * max_size.y * pixel_stride;
    tex->data = calloc(1, (tex_stride * num_files) +
                        sizeof(struct pg_texture_layer) * num_files);
    tex->layer_info = tex->data + (tex_stride * num_files);
    /*  Copy the individual texture data into the big allocation and record
        the proportion of texture space used for each one (some textures may
        be smaller than the full size, which must be taken into account
        when calculating frames in a texture atlas  */
    for(i = 0; i < num_files; ++i) {
        if(uvec2_is_zero(tex_size[i])) continue;
        struct pg_texture_layer* dst_layer = tex->layer_info + i;
        dst_layer->layer_uv = vec2( (float)tex_size[i].x / (float)max_size.x,
                                    (float)tex_size[i].y / (float)max_size.y );
        dst_layer->layer_pix = vec2(vec_cast2(tex_size[i]));
        dst_layer->frame_uv = vec2(1,1);
        dst_layer->frame_pix = vec2(vec_cast2(tex_size[i]));
        dst_layer->pitch = 1;
        if(!tex_data[i]) continue;
        uint8_t* dst_data = tex->data + i * tex_stride;
        int x, y;
        for(x = 0; x < tex_size[i].x; ++x) for(y = 0; y < tex_size[i].y; ++y) {
            ubvec4* src_pix = (ubvec4*)(tex_data[i] + (x * 4) + (y * tex_size[i].x * 4));
            ubvec4* dst_pix = (ubvec4*)(dst_data + (x * 4) + (y * max_size.x * 4));
            *dst_pix = *src_pix;
        }
        free(tex_data[i]);
    }
    /*  Store all the bookkeeping stuff */
    tex->type = PG_UBYTE;
    tex->stride = tex_stride;
    tex->pixel_stride = pixel_stride;
    tex->usage = PG_TEXTURE_IMAGE;
    tex->channels = 4;
    tex->layers = num_files;
    tex->w = max_size.x;
    tex->h = max_size.y;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init(struct pg_texture* tex, enum pg_data_type type,
                     int w, int h, int layers, struct pg_texture_opts* opts)
{
    if(type < PG_UBYTE || type >= PG_MATRIX) {
        *tex = (struct pg_texture){};
        return;
    }
    const struct tex_format* fmt = &gl_tex_formats[type];
    tex->type = fmt->pgtype;
    tex->usage = PG_TEXTURE_IMAGE;
    tex->channels = fmt->channels;
    tex->stride = fmt->size * w * h;
    tex->pixel_stride = fmt->size;
    tex->data = calloc(1, (tex->stride * layers) +
                       (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init_depth(struct pg_texture* tex, int w, int h, int layers,
                           struct pg_texture_opts* opts)
{
    tex->type = PG_FLOAT;
    tex->usage = PG_TEXTURE_DEPTH;
    tex->channels = 1;
    tex->stride = sizeof(float) * w * h;
    tex->data = calloc(1, (tex->stride * layers) +
                        (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_init_depth_stencil(struct pg_texture* tex, int w, int h, int layers,
                                   struct pg_texture_opts* opts)
{
    tex->type = PG_FLOAT;
    tex->usage = PG_TEXTURE_DEPTH_STENCIL;
    tex->channels = 1;
    tex->stride = sizeof(float) * w * h;
    tex->data = calloc(1, (tex->stride * layers) +
                        (sizeof(struct pg_texture_layer) * layers));
    tex->layer_info = (struct pg_texture_layer*)(tex->data + (tex->stride * layers));
    tex->layers = layers;
    tex->w = w;
    tex->h = h;
    if(!opts) tex->opts = *PG_TEXTURE_OPTS();
    else tex->opts = *opts;
    glGenTextures(1, &tex->handle);
    pg_texture_buffer(tex);
}

void pg_texture_deinit(struct pg_texture* tex)
{
    if(!tex->data) return;
    glDeleteTextures(1, &tex->handle);
    free(tex->data);
}

void pg_texture_options(struct pg_texture* tex, struct pg_texture_opts* opts)
{
    tex->opts = *opts;
}

void pg_texture_bind(struct pg_texture* tex, int slot)
{
    if(tex->type == PG_NULL) return;
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
}

void pg_texture_buffer_opts(struct pg_texture_opts* opts)
{
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, opts->wrap_x);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, opts->wrap_y);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, opts->filter_min);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, opts->filter_mag);
    glTexParameteriv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_SWIZZLE_RGBA, opts->swizzle);
    glTexParameterfv(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BORDER_COLOR, opts->border.v);
}

void pg_texture_buffer(struct pg_texture* tex)
{
    if(tex->type == PG_NULL) return;
    glActiveTexture(GL_TEXTURE0);
    if(tex->usage == PG_TEXTURE_IMAGE) {
        enum pg_data_type fulltype = tex->type + tex->channels - 1;
        const struct tex_format* fmt = &gl_tex_formats[fulltype];
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, fmt->iformat, tex->w, tex->h, tex->layers, 0,
                     fmt->pixformat, fmt->type, tex->data);
        if(tex->opts.filter_min != GL_LINEAR
        && tex->opts.filter_min != GL_NEAREST) {
            glGenerateMipmap(tex->handle);
        }
    } else if(tex->usage == PG_TEXTURE_DEPTH) {
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH_COMPONENT32F, tex->w, tex->h,
                     tex->layers, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    } else if(tex->usage == PG_TEXTURE_DEPTH_STENCIL) {
        glBindTexture(GL_TEXTURE_2D_ARRAY, tex->handle);
        pg_texture_buffer_opts(&tex->opts);
        glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_DEPTH24_STENCIL8, tex->w, tex->h,
                     tex->layers, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
    }
}

void pg_texture_set_atlas(struct pg_texture* tex, int layer, int frame_w, int frame_h)
{
    if(layer >= tex->layers) return;
    struct pg_texture_layer* layer_info = tex->layer_info + layer;
    layer_info->frame_pix = vec2(frame_w, frame_h);
    layer_info->frame_uv = vec2_div(layer_info->frame_pix, layer_info->layer_pix);
    layer_info->pitch = layer_info->layer_pix.x / layer_info->frame_pix.x;
}

const struct pg_texture_layer* pg_texture_get_atlas(struct pg_texture* tex, int layer)
{
    if(layer >= tex->layers) return NULL;
    return &tex->layer_info[layer];
}

pg_tex_frame_t pg_texture_frame(struct pg_texture* tex, int layer, int frame)
{
    if(layer >= tex->layers) return (pg_tex_frame_t){ vec4(0,0,1,1), 0 };
    struct pg_texture_layer* layer_info = &tex->layer_info[layer];
    vec2 f0 = vec2( (float)(frame % layer_info->pitch),
                    (float)(frame / layer_info->pitch) );
    f0 = vec2_mul(f0, layer_info->frame_uv);
    vec2 f1 = vec2_add(f0, layer_info->frame_uv);
    f0 = vec2_mul(f0, layer_info->layer_uv);
    f1 = vec2_mul(f1, layer_info->layer_uv);

    pg_tex_frame_t ret = { .layer = layer,
        .frame = vec4( VEC_XY(f0), VEC_XY(f1) ) };
    return ret;
}

pg_tex_frame_t pg_texture_frame_flip(pg_tex_frame_t in, int x, int y)
{
    pg_tex_frame_t ret = in;
    if(x) {
        ret.frame.x = in.frame.z;
        ret.frame.z = in.frame.x;
    }
    if(y) {
        ret.frame.y = in.frame.w;
        ret.frame.w = in.frame.y;
    }
    return ret;
}

pg_tex_frame_t pg_texture_frame_tx(pg_tex_frame_t in,
                                   vec2 const scale, vec2 const offset)
{
    pg_tex_frame_t ret = in;
    vec2 diff = vec2_sub(vec2(in.frame.z, in.frame.w), vec2(in.frame.x, in.frame.y));
    diff = vec2_mul(diff, scale);
    ret.frame.z += diff.x + offset.x;
    ret.frame.w += diff.y + offset.y;
    ret.frame.x += offset.x;
    ret.frame.y += offset.y;
    return ret;
}

void pg_texture_write_pixels(struct pg_texture* tex, void* data,
                             int layer, int x, int y, int w, int h)
{
    struct pg_texture_layer* dst_layer = tex->layer_info + layer;
    void* dst_data = tex->data + layer * tex->stride;
    size_t pix_sz = tex->pixel_stride;
    size_t line_sz = tex->pixel_stride * tex->w;
    if(x > dst_layer->layer_pix.x || y > dst_layer->layer_pix.y) return;
    int x_dst_start = LM_MAX(0, x);
    int x_src_start = -LM_MIN(0, x);
    int x_len = LM_MIN(dst_layer->layer_pix.x - x_dst_start, w);
    int y_dst_start = LM_MAX(0, y);
    int y_src_start = -LM_MIN(0, y);
    int y_len = LM_MIN(dst_layer->layer_pix.y - y_dst_start, h);
    if(x_len <= 0 || y_len <= 0) return;
    int i;
    for(i = 0; i < y_len; ++i) {
        void* dst_line = dst_data + (x_dst_start * pix_sz) + ((y_dst_start + i) * line_sz);
        void* src_line = data + (x_src_start * pix_sz) + ((y_src_start + i) * pix_sz * w);
        memcpy(dst_line, src_line, x_len * pix_sz);
    }
}

/********************/
/*  Rendertexture   */
/********************/

void pg_renderbuffer_init(struct pg_renderbuffer* buffer)
{
    *buffer = (struct pg_renderbuffer){};
    glGenFramebuffers(1, &buffer->fbo);
}

void pg_renderbuffer_deinit(struct pg_renderbuffer* buffer)
{
    glDeleteFramebuffers(1, &buffer->fbo);
}

void pg_renderbuffer_attach(struct pg_renderbuffer* buffer,
                             struct pg_texture* tex, int layer,
                             int idx, GLenum attachment)
{
    buffer->outputs[idx] = tex;
    buffer->layer[idx] = layer;
    buffer->attachments[idx] = tex ? attachment : GL_NONE;
    buffer->dirty = 1;
}

void pg_renderbuffer_build(struct pg_renderbuffer* buffer)
{
    glBindFramebuffer(GL_FRAMEBUFFER, buffer->fbo);
    int w = -1, h = -1;
    buffer->dirty = 0;
    int i;
    for(i = 0; i < 8; ++i) {
        if(!buffer->outputs[i]) continue;
        if(w < 0) w = buffer->outputs[i]->w;
        else w = LM_MIN(w, buffer->outputs[i]->w);
        if(h < 0) h = buffer->outputs[i]->h;
        else h = LM_MIN(h, buffer->outputs[i]->h);
        glFramebufferTextureLayer(GL_FRAMEBUFFER, buffer->attachments[i],
                                  buffer->outputs[i]->handle, 0, buffer->layer[i]);
        if(buffer->attachments[i] != GL_DEPTH_ATTACHMENT
        && buffer->attachments[i] != GL_DEPTH_STENCIL_ATTACHMENT) {
            buffer->drawbufs[i] = buffer->attachments[i];
        } else {
            buffer->drawbufs[i] = GL_NONE;
        }
    }
    buffer->w = w;
    buffer->h = h;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void pg_renderbuffer_dst(struct pg_renderbuffer* buffer)
{
    if(buffer->dirty) {
        pg_renderbuffer_build(buffer);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, buffer->fbo);
    glDrawBuffers(8, buffer->drawbufs);
    glViewport(0, 0, buffer->w, buffer->h);
}

void pg_rendertarget_init(struct pg_rendertarget* target,
                          struct pg_renderbuffer* b0, struct pg_renderbuffer* b1)
{
    target->buf[0] = b0;
    target->buf[1] = b1;
    if(b1 && (b1->w != b0->w || b1->h != b0->h)) {
        printf("Procgl WARNING: pg_rendertarget buffer size mismatch\n");
    }
}

void pg_rendertarget_dst(struct pg_rendertarget* target)
{
    pg_renderbuffer_dst(target->buf[0]);
}

void pg_rendertarget_swap(struct pg_rendertarget* target)
{
    if(!target->buf[1]) return;
    struct pg_renderbuffer* tmp = target->buf[0];
    target->buf[0] = target->buf[1];
    target->buf[1] = tmp;
}

vec2 pg_rendertarget_get_resolution(struct pg_rendertarget* target)
{
    if(!target || !target->buf[0]) {
        int w, h;
        pg_screen_size(&w, &h);
        return vec2(w, h);
    }
    return vec2(target->buf[0]->w, target->buf[0]->h);
}

void pg_rendertarget_clear(struct pg_rendertarget* target)
{
    pg_rendertarget_dst(target);
    glDepthMask(GL_TRUE);
    glStencilMask(0xFF);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

/********************/
/*  Buffer texture  */
/********************/

void pg_buffertex_init(struct pg_buffertex* buf, enum pg_data_type type,
                       uint32_t group_size, uint32_t group_count)
{
    const struct tex_format* fmt = &gl_tex_formats[type];
    buf->type = type;
    buf->stride = fmt->size;
    buf->data_cap = buf->stride * group_size * group_count;
    buf->data = calloc(1, (buf->stride * buf->data_cap));
    buf->write_idx = 0;
    buf->group_size = group_size;
    buf->group_stride = group_size * buf->stride;
    glGenTextures(1, &buf->tex_handle);
    glGenBuffers(1, &buf->buf_handle);
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferData(GL_TEXTURE_BUFFER, buf->data_cap, buf->data, GL_DYNAMIC_DRAW);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}

void pg_buffertex_deinit(struct pg_buffertex* buf)
{
    glDeleteTextures(1, &buf->tex_handle);
    glDeleteBuffers(1, &buf->buf_handle);
    free(buf->data);
    *buf = (struct pg_buffertex){};
}

/*  Move write index to given value */
void pg_buffertex_seek(struct pg_buffertex* buf, int idx)
{
    buf->write_idx = idx * buf->group_stride;
}

/*  Write formatted data (group_size per entry) */
void pg_buffertex_push(struct pg_buffertex* buf, void* data, int count)
{
    uint32_t len = buf->group_stride * count;
    if(buf->write_idx + len >= buf->data_cap) return;
    memcpy(buf->data + buf->write_idx, data, len);
    buf->write_idx += len;
}

/*  Upload data to GPU  */
void pg_buffertex_buffer(struct pg_buffertex* buf)
{
    const struct tex_format* fmt = &gl_tex_formats[buf->type];
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferData(GL_TEXTURE_BUFFER, buf->data_cap, buf->data, GL_DYNAMIC_DRAW);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}

void pg_buffertex_sub(struct pg_buffertex* buf, uint32_t idx, uint32_t len)
{
    const struct tex_format* fmt = &gl_tex_formats[buf->type];
    glBindBuffer(GL_TEXTURE_BUFFER, buf->buf_handle);
    glBufferSubData(GL_TEXTURE_BUFFER,
                    idx * buf->group_stride,
                    len * buf->group_stride,
                    buf->data + idx * buf->group_stride);
    glBindTexture(GL_TEXTURE_BUFFER, buf->tex_handle);
    glTexBuffer(GL_TEXTURE_BUFFER, fmt->iformat, buf->buf_handle);
}






