struct pg_font_glyph {
    vec2 uv[2];
    vec2 offset;
    vec2 scale;
    float xadvance;
};

struct pg_font_glyph_mapping {
    wchar_t ch;
    int idx;
};

struct pg_font {
    struct pg_texture* tex;
    int tex_layer;
    int size;
    float full_size;
    float ascent;
    float descent;
    float line_gap;
    float line_move;
    /*  TTF info    */
    char ttf_filename[256];
    unsigned char* ttf;
    stbtt_fontinfo ttf_info;
    /*  Glyph packing context   */
    stbtt_pack_context pack_ctx;
    /*  Hash table mapping codepoints -> glyph array indices    */
    ARR_T(struct pg_font_glyph) chars;
    ARR_T(struct pg_font_glyph_mapping) char_buckets[256];
};

void pg_font_init(struct pg_font* font, const char* filename,
                  struct pg_texture* tex, int tex_layer, int size);
void pg_font_deinit(struct pg_font* font);

int pg_font_load_chars(struct pg_font* font, const wchar_t* ch, int n);
const struct pg_font_glyph* pg_font_get_glyph(struct pg_font* font, wchar_t ch);

struct pg_text_form {
    vec2 area[2];
    struct pg_text_form_glyph {
        vec2 pos, scale, uv0, uv1;
        uint32_t color; uint8_t ctrl_flags; uint8_t tex_layer;
    } *glyphs;
    int n_glyphs, max_glyphs;
    int own_alloc;
};

struct pg_font_group {
    struct pg_font* font[4];
    int n_fonts;
};

struct pg_text_formatter {
    struct pg_font_group fonts;
    int default_font;
    vec2 region;
    float htab;
    vec2 scale;
    vec2 spacing;
    uint32_t color_palette[8];
    float last_line_justify_threshold;
    enum pg_text_alignment {
        PG_TEXT_LEFT, PG_TEXT_CENTER, PG_TEXT_RIGHT, PG_TEXT_JUSTIFY
    } align;
    enum pg_text_wrapping {
        PG_TEXT_NO_WRAP, PG_TEXT_WRAP_LETTERS, PG_TEXT_WRAP_WORDS
    } wrap;
};

#define PG_FONTS(N, ...) (struct pg_font_group){ \
    .n_fonts = (N), .font = { __VA_ARGS__ } }

#define PG_TEXT_FORMATTER(...) (struct pg_text_formatter){ \
    .region = {{0,0}}, .htab = 0, .scale = {{1,1}}, .spacing = {{1,1}}, \
    .align = PG_TEXT_LEFT, .wrap = PG_TEXT_NO_WRAP, \
    .last_line_justify_threshold = 0.75, \
    .color_palette = { 0xFFFFFFFF, 0x7F7F7FFF, 0x000000FF, \
                       0xFF0000FF, 0x00FF00FF, 0x0000FFFF, \
                       0x000000FF, 0x000000FF }, \
    __VA_ARGS__ }

#define PG_TEXT_FORMATTER_MOD(fmt, ...) (struct pg_text_formatter){ \
    .fonts = (fmt).fonts, .default_font = (fmt).default_font, \
    .region = (fmt).region, .htab = (fmt).htab, .scale = (fmt).scale, \
    .spacing = (fmt).spacing, .align = (fmt).align, .wrap = (fmt).wrap, \
    .last_line_justify_threshold = (fmt).last_line_justify_threshold, \
    .color_palette = { (fmt).color_palette[0], (fmt).color_palette[1], \
        (fmt).color_palette[2], (fmt).color_palette[3], (fmt).color_palette[4], \
        (fmt).color_palette[5], (fmt).color_palette[6], (fmt).color_palette[7] }, \
    __VA_ARGS__ }

struct pg_font* pg_font_group_get(struct pg_font_group* grp, int i);
void pg_text_form_init_alloc(struct pg_text_form* form, int n);
void pg_text_form_init_ptr(struct pg_text_form* form, struct pg_text_form_glyph* glyphs, int n);
void pg_text_form_deinit(struct pg_text_form* form);
void pg_text_form_alloc_reserve(struct pg_text_form* form, int n);
void pg_text_format(struct pg_text_form* form, struct pg_text_formatter* fmt,
                    const wchar_t* str, int len);
void pg_text_format_c(struct pg_text_form* form, struct pg_text_formatter* fmt,
                      const char* str, int len);
