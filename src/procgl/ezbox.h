/************************************/
/*  BOX BATCH                       */
/************************************/

/*
    struct {
        float3 pos; float scale;

        uint packed_scale_xyz_texlayer_w; uint packed_end_scale;
        ulong packed_quaternion;

        uint packed_tex_left; uint packed_tex_right;
        uint packed_tex_front; uint packed_tex_back;

        uint packed_tex_top; uint packed_tex_bottom;
        uint color_mod; uint color_add;
    }
*/

struct pg_ezbox_packed {
    vec3 pos; float iso_scale;

    uint32_t aniso_scale_and_tex_layer; uint32_t endcap_scale;
    uint64_t orientation;

    uint32_t tex_left, tex_right, tex_front, tex_back;
    
    uint32_t tex_top, tex_bottom;
    uint32_t color_mod, color_add;
};

struct pg_ezbox {
    vec3 pos; float iso_scale;
    vec3 aniso_scale; vec2 cap_scale_bottom, cap_scale_top;
    quat orientation; unsigned tex_layer;
    ubvec4 tex_left, tex_right, tex_front, tex_back, tex_top, tex_bottom;
    ubvec4 color_mod, color_add;
};

struct pg_boxbatch {
    struct pg_buffertex buf;
    struct pg_texture* tex;
    int font_layer;
    mat4 cur_transform;
    uint32_t cur_start;
    uint32_t cur_idx;
};

#define PG_EZBOX(...) \
    ((struct pg_ezbox){ \
        .iso_scale = 1, .aniso_scale = {{ 1, 1, 1 }}, \
        .cap_scale_bottom = {{ 1, 1 }}, .cap_scale_top = {{ 1, 1 }}, \
        .orientation = {{ 0, 0, 0, 1 }}, \
        .tex_left =  {{ 0, 0, 64, 64 }}, .tex_right =  {{ 0, 0, 64, 64 }}, \
        .tex_front = {{ 0, 0, 64, 64 }}, .tex_back =   {{ 0, 0, 64, 64 }}, \
        .tex_top =   {{ 0, 0, 64, 64 }}, .tex_bottom = {{ 0, 0, 64, 64 }}, \
        .color_mod = {{ 0xFF, 0xFF, 0xFF, 0xFF }}, \
        .color_add = {{ 0x00, 0x00, 0x00, 0x00 }}, __VA_ARGS__ })

void pg_boxbatch_init(struct pg_boxbatch* batch, int size, struct pg_texture* tex);
/*  Build a renderpass to use the batched data  */
void pg_boxbatch_init_pass(struct pg_boxbatch* batch, struct pg_renderpass* pass,
                          struct pg_rendertarget* target, vec2 tex_size);
void pg_boxbatch_deinit(struct pg_boxbatch* batch);
/*  Add sprites or text to a current batch group    */
void pg_boxbatch_add_box(struct pg_boxbatch* batch, struct pg_ezbox* box);
/*  Emit a draw operation to a renderpass for the current batch group   */
void pg_boxbatch_draw(struct pg_boxbatch* batch, struct pg_renderpass* pass);
/*  Begin a new batch group with a given model matrix   */
void pg_boxbatch_next(struct pg_boxbatch* batch, mat4* transform);
/*  Upload all the batch groups to the GPU so it can be read at draw time   */
void pg_boxbatch_buffer(struct pg_boxbatch* batch);
/*  Restart batching    */
void pg_boxbatch_reset(struct pg_boxbatch* batch);


/************************************/
/*  EZBOX MODELS AND ANIMATION      */
/************************************/

/*  Basic explanation:
    pg_ezbox_model  -   Boxes and bone associations
    pg_ezbox_rig    -   Bones and bone transforms for each pose
    pg_ezbox_pose   -   Calculated transforms for each box in a model   */

#define PG_BOXMODEL_MAX_PARTS   32
#define PG_BOXMODEL_MAX_BONES   32
#define PG_BOXMODEL_MAX_POSES   32
struct pg_ezbox_model {
    struct pg_ezbox parts[PG_BOXMODEL_MAX_PARTS];
    int num_parts;
    char use_bone[PG_BOXMODEL_MAX_PARTS][32];
    int bone_idx[PG_BOXMODEL_MAX_PARTS];
};

struct pg_ezbox_pose {
    quat box_rot[PG_BOXMODEL_MAX_PARTS];
    vec3 box_move[PG_BOXMODEL_MAX_PARTS];
};

struct pg_ezbox_rig {
    char bone_name[PG_BOXMODEL_MAX_BONES][32];
    vec3 bone_origin[PG_BOXMODEL_MAX_BONES];
    int num_bones;
    char pose_name[PG_BOXMODEL_MAX_POSES][32];
    quat pose_rot[PG_BOXMODEL_MAX_POSES][PG_BOXMODEL_MAX_BONES];
    vec3 pose_move[PG_BOXMODEL_MAX_POSES][PG_BOXMODEL_MAX_BONES];
    int num_poses;
};

struct pg_ezbox_model_draw {
    struct pg_ezbox_model* model;
    quat orientation;
    vec3 pos;
    float scale;
    ubvec4 color_mod, color_add;
    ubvec2 tex_offset;
    struct pg_ezbox_rig* rig;
    int pose[2];
    float pose_lerp;
};

#define PG_EZDRAW_BOX_MODEL(MODEL, ...) (&(struct pg_ezbox_model_draw){ \
    .model = (MODEL), .orientation = {{ 0, 0, 0, 1 }}, \
    .pos = {{ 0, 0, 0 }}, .scale = 1, .tex_offset = {{ 0, 0 }}, \
    .color_mod = {{ 255, 255, 255, 255}}, \
    .color_add = {{ 0, 0, 0, 0 }}, __VA_ARGS__ })

void pg_boxbatch_add_model(struct pg_boxbatch* batch,
                           struct pg_ezbox_model_draw* draw);

void pg_ezbox_rig_read_from_file(struct pg_ezbox_rig* rig, char* filename);
void pg_ezbox_model_link_rig(struct pg_ezbox_model* model, struct pg_ezbox_rig* rig);
