struct pg_ui_context;
struct pg_ui_content;
struct pg_ui_event;

typedef int pg_ui_t;
typedef ARR_T(pg_ui_t) pg_ui_arr_t;
typedef HTABLE_T(pg_ui_t) pg_ui_table_t;

typedef int (*pg_ui_callback_t)(struct pg_ui_context*, pg_ui_t elem,
                                struct pg_ui_event* event);

enum pg_ui_property_id {
    PG_UI_POS, PG_UI_PIXEL_POS,
    PG_UI_SCALE,
    PG_UI_ROTATION,
    PG_UI_CLIP, PG_UI_PIXEL_CLIP,
    PG_UI_IMG_POS, PG_UI_PIXEL_IMG_POS,
    PG_UI_IMG_ROTATION,
    PG_UI_IMG_SCALE, PG_UI_PIXEL_IMG_SCALE,
    PG_UI_IMG_COLOR_MOD,
    PG_UI_IMG_COLOR_ADD,
    PG_UI_IMG_FRAME,
    PG_UI_TEXT_POS, PG_UI_PIXEL_TEXT_POS,
    PG_UI_TEXT_ROTATION,
    PG_UI_TEXT_SCALE,
    PG_UI_TEXT_COLOR,
    PG_UI_ACTION_AREA, PG_UI_PIXEL_ACTION_AREA,
    PG_UI_ELEM_PROPERTIES,
};

enum pg_ui_callback_id {
    PG_UI_CLICK,
    PG_UI_HOLD,
    PG_UI_RELEASE,
    PG_UI_SCROLL,
    PG_UI_ENTER,
    PG_UI_LEAVE,
    PG_UI_UPDATE,
    PG_UI_DRAG_DROP,
    PG_UI_ELEM_CALLBACKS,
};

enum pg_ui_action_item {
    PG_UI_ACTION_INDEPENDENT,
    PG_UI_ACTION_IMG,
    PG_UI_ACTION_TEXT
};

enum pg_ui_draw_mode {
    PG_UI_DRAW_NONE,
    PG_UI_TEXT_ONLY,
    PG_UI_IMG_ONLY,
    PG_UI_TEXT_THEN_IMG,
    PG_UI_IMG_THEN_TEXT,
};

struct pg_ui_properties {
    /*  General */
    int enabled;
    int enable_clip;
    vec4 pixel_clip;
    vec4 clip;
    vec2 pixel_pos;
    vec2 pos;
    vec2 scale;
    float rotation;
    vec4 pixel_action_area;
    vec4 action_area;
    float layer;
    /*  Image   */
    float img_rotation;
    vec2 pixel_img_pos;
    vec2 img_pos;
    vec2 pixel_img_scale;
    vec2 img_scale;
    vec4 img_color_mod;
    vec4 img_color_add;
    vec4 img_frame;
    /*  Text    */
    float text_rotation;
    vec2 text_rot_anchor;
    vec2 pixel_text_pos;
    vec2 text_pos;
    vec2 text_scale;
    vec4 text_color;
    /*  Animate-able fields above, static fields below  */
    enum pg_ui_draw_mode draw;
    enum pg_ui_action_item action_item;
    int img_layer;
    vec2 text_anchor;
    const wchar_t* text;
    const char* text_c;
    int text_len;
    struct pg_text_formatter* text_formatter;
    /*  Event handling  */
    uint64_t drop_type, accept_drops;
    pg_ui_callback_t cb_click, cb_hold, cb_release, cb_scroll,
                     cb_enter, cb_leave, cb_drag_drop, cb_update;
};

struct pg_ui_event {
    union {
        struct {
            enum {
                PG_UI_MOUSE_CLICK,
                PG_UI_MOUSE_HOLD,
                PG_UI_MOUSE_RELEASE,
                PG_UI_MOUSE_SCROLL,
            } action;
            enum {
                PG_UI_MOUSE_LEFT,
                PG_UI_MOUSE_MIDDLE,
                PG_UI_MOUSE_RIGHT,
            } button;
            int scroll_direction;
        } mouse;
        struct {
            enum {
                PG_UI_HOVER_ENTER,
                PG_UI_HOVER_STAY,
                PG_UI_HOVER_LEAVE,
            } action;
        } hover;
        struct {
            pg_ui_t drop;
        } drag_drop;
    };
};


#define PG_UI_PROPERTIES(...) \
    (&(struct pg_ui_properties){ .enabled = 1, .enable_clip = 0, \
        .clip = {{ 0,0,1,1 }}, \
        .action_item = PG_UI_ACTION_INDEPENDENT, .draw = PG_UI_DRAW_NONE, \
        .scale = {{ 1, 1 }}, .text_scale = {{ 1, 1 }}, \
        .img_color_mod = {{ 1, 1, 1, 1 }}, .text_color = {{ 1, 1, 1, 1 }}, \
        __VA_ARGS__ })

struct pg_ui_context {
    ARR_T(struct pg_ui_content) content_pool;
    pg_ui_t root;
    struct pg_quadbatch sprite_batch;
    struct pg_renderpass rendpass;
    struct pg_texture* tex;
    struct pg_text_formatter default_text_formatter;
    int clip;
    vec2 screen_res;
    mat4 screen_mat;
    vec4 mouse_img;
    vec2 mouse_pos;
    uint8_t mouse_ctrl;
    int mouse_scroll;
    float time;
};

/*  Context management functions   */
void pg_ui_init(struct pg_ui_context* ctx, struct pg_rendertarget* target,
                struct pg_texture* tex, struct pg_text_formatter* formatter);
void pg_ui_deinit(struct pg_ui_context* ui);
void pg_ui_target(struct pg_ui_context* ui, struct pg_rendertarget* target);
void pg_ui_resolution(struct pg_ui_context* ui, vec2 res);

/*  Doing the thing */
void pg_ui_update(struct pg_ui_context* ui, float time);
void pg_ui_draw(struct pg_ui_context* ui, float dt);

/*  Building UI hierarchy   */
pg_ui_t pg_ui_context_root(struct pg_ui_context* ctx);
pg_ui_t pg_ui_add_group(struct pg_ui_context* ctx, pg_ui_t parent,
                                  const char* name, struct pg_ui_properties* props);
pg_ui_t pg_ui_add_elem(struct pg_ui_context* ctx, pg_ui_t parent,
                                 const char* name, struct pg_ui_properties* props);
void pg_ui_delete(struct pg_ui_context* ctx, pg_ui_t del);

/*  Animations  */
void pg_ui_cancel_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                       enum pg_ui_property_id prop_id);
void pg_ui_simple_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                       enum pg_ui_property_id prop_id, struct pg_simple_anim* anim);
void pg_ui_keyframe_anim(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                         enum pg_ui_property_id prop, const struct pg_animation* anim,
                         int start_frame, int loop_start, int loop_end,
                         int loop_count, int loop_mode);

/*  General UI functions    */
float pg_ui_get_aspect(struct pg_ui_context* ctx);
void pg_ui_enabled(struct pg_ui_context* ctx, pg_ui_t ui_ref, int enabled);
pg_ui_t pg_ui_child(struct pg_ui_context* ctx, pg_ui_t parent,
                              const char* name);
pg_ui_t pg_ui_child_path(struct pg_ui_context* ctx, pg_ui_t parent,
                                   const char* name);
pg_ui_t pg_ui_parent(struct pg_ui_context* ctx, pg_ui_t child);
struct pg_type* pg_ui_variable(struct pg_ui_context* ctx,
                               pg_ui_t ui_ref, char* name);
void pg_ui_set_text_formatter(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                              struct pg_text_formatter* formatter);
void pg_ui_set_text_c(struct pg_ui_context* ctx, pg_ui_t ui_ref, const char* text, int n);
void pg_ui_set_text(struct pg_ui_context* ctx, pg_ui_t ui_ref, const wchar_t* text, int n);
void pg_ui_show(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                int show_text, int show_img);

const char* pg_ui_get_name(struct pg_ui_context* ctx, pg_ui_t ui_ref);
vec4* pg_ui_get_property(struct pg_ui_context* ctx, pg_ui_t ui_ref,
                         enum pg_ui_property_id prop_id);
vec2 pg_ui_mouse_pos(struct pg_ui_context* ctx, pg_ui_t ui_ref);

void pg_ui_drag_drop(struct pg_ui_context* ctx, pg_ui_t dst,
                     pg_ui_t dropped, vec2 drop_pos);
void pg_ui_set_callback(struct pg_ui_context* ctx, pg_ui_t dst,
                        enum pg_ui_callback_id id, pg_ui_callback_t cb);
