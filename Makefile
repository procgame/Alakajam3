##################################
# Makefile for procgame projects #
##################################

CC := gcc
INCLUDES := -Isrc
TARGET := procgame


# Linux
LIBS_LINUX := -Lsrc/libs/linux/GL -lGLEW -lSDL2 -lGL -lm


# Windows
LIBS_WIN64 := -Lsrc/libs/win64/GL -Lsrc/libs/win64/SDL2 \
 -lglew32 -lmingw32 -lSDL2main -lSDL2 -lgdi32 -lwinmm -limm32 \
 -lole32 -loleaut32 -lversion -lopengl32 -lws2_32 -limagehlp \


# Compile flags for the different build types
CFLAGS_DEBUG := $(CFLAGS_EXTRA) -Wall -Wno-unused-variable -Wno-unused-function -g -O0 -std=gnu11
CFLAGS_RELEASE := $(CFLAGS_EXTRA) -Wall -Wno-unused-variable -Wno-unused-function -g -O3 -flto -std=gnu11


# Build targets
debug_linux: INCLUDES += -Isrc/libs/linux
debug_linux: LIBS := $(LIBS_LINUX)
debug_linux: CFLAGS := $(CFLAGS_DEBUG) -rdynamic \
 -D_POSIX_C_SOURCE=200809L -D_GNU_SOURCE \
 -DGLEW_STATIC -DSHADER_BASE_DIR='"${CURDIR}/src/procgl/shaders/"'
debug_linux: procgame

release_linux: INCLUDES += -Isrc/libs/linux
release_linux: LIBS := $(LIBS_LINUX)
release_linux: CFLAGS := $(CFLAGS_RELEASE) -rdynamic \
 -DPOSIX_C_SOURCE=200809L -D_GNU_SOURCE \
 -DGLEW_STATIC -DPROCGL_STATIC_SHADERS
release_linux: clean dump_shaders procgame stripdebug

debug_windows: INCLUDES += -Isrc/libs/win64
debug_windows: LIBS := $(LIBS_WIN64)
debug_windows: CFLAGS := $(CFLAGS_DEBUG) -Wl,--export-all-symbols -mwindows \
 -DGLEW_STATIC -DSHADER_BASE_DIR='"${CURDIR}/src/procgl/shaders/"'
debug_windows: TARGET := $(TARGET).exe
debug_windows: procgame

release_windows: INCLUDES += -Isrc/libs/win64
release_windows: LIBS := $(LIBS_WIN64)
release_windows: CFLAGS := $(CFLAGS_RELEASE) -Wl,--export-all-symbols -mwindows \
 -DPROCGL_STATIC_SHADERS -DGLEW_STATIC
release_windows: TARGET := $(TARGET).exe
release_windows: clean dump_shaders procgame stripdebug


# Build rules
GAME_OBJS = $(patsubst src%,obj%,$(patsubst %.c,%.o,$(wildcard src/game/*.c)))
GAME_UI_OBJS = $(patsubst src%,obj%,$(patsubst %.c,%.o,$(wildcard src/game/ui/*.c)))
PROCGAME_OBJS = $(patsubst src%,obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/*.c)))
EXT_OBJS = $(patsubst src%,obj%,$(patsubst %.c,%.o,$(wildcard src/procgl/ext/*.c)))

GAME_HEADERS = $(wildcard src/game/*.h)
GAME_UI_HEADERS = $(wildcard src/game/ui/*.h)
PROCGAME_HEADERS = $(wildcard src/procgl/*.h)
EXT_HEADERS = $(wildcard src/procgl/ext/*.h)

procgame: obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS)
	$(CC) $(CFLAGS) -o $(TARGET) obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS) $(LIBS)

obj/main.o: src/main.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ src/main.c
obj/game/%.o: src/game/%.c $(GAME_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
obj/game/ui/%.o: src/game/ui/%.c $(GAME_HEADERS) $(GAME_UI_HEADERS) $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
obj/procgl/%.o: src/procgl/%.c $(PROCGAME_HEADERS) $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<
obj/procgl/ext/%.o: src/procgl/ext/%.c $(EXT_HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c -o $@ $<


# Special build rules
dump_shaders: src/procgl/shaders/*.glsl
	cd src/procgl/shaders && \
    echo "#ifndef PG_STATIC_SHADERS_INCLUDED" > shader_sources.glsl.h && \
    echo "#define PG_STATIC_SHADERS_INCLUDED" >> shader_sources.glsl.h && \
    xxd -i text_vert.glsl >> shader_sources.glsl.h && \
    xxd -i text_frag.glsl >> shader_sources.glsl.h && \
    xxd -i 2d_vert.glsl >> shader_sources.glsl.h && \
    xxd -i 2d_frag.glsl >> shader_sources.glsl.h && \
    xxd -i screen_vert.glsl >> shader_sources.glsl.h && \
    xxd -i post_screen_frag.glsl >> shader_sources.glsl.h && \
    xxd -i post_blur_frag.glsl >> shader_sources.glsl.h && \
    xxd -i post_sine_frag.glsl >> shader_sources.glsl.h && \
    xxd -i post_gamma_frag.glsl >> shader_sources.glsl.h && \
    sed -i -e 's/unsigned char/static char/g' shader_sources.glsl.h && \
    sed -i -e 's/unsigned int/static unsigned/g' shader_sources.glsl.h && \
    echo "#endif" >> shader_sources.glsl.h

OBJDIRS = obj obj/game obj/game/ui obj/procgl obj/procgl/ext
objdirs: $(OBJDIRS)
$(OBJDIRS):
	mkdir -p $@

clean:
	rm -f obj/main.o $(GAME_OBJS) $(GAME_UI_OBJS) $(PROCGAME_OBJS) $(EXT_OBJS)
	rm -f src/procgl/shaders/*.glsl.h

stripdebug: procgame
	cp $(TARGET) $(TARGET).dbg
	strip --strip-debug $(TARGET) -o $(TARGET)
